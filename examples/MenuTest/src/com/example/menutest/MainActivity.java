package com.example.menutest;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    java.text.DateFormat formatter;
    Date date = new Date();
    System.out.println(date.getTime());
    try {
      formatter = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
      String dateString = formatter.format(date);
      System.out.println("String:" + dateString);
      date = (Date) formatter.parse(dateString);
      System.out.println(date.getTime());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private final int MENU_LOGOUT = 1;

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    menu.add(0, MENU_LOGOUT, 0, R.string.logoutbutton);
    menu.add(0, 2, 0, "Next");
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
    case MENU_LOGOUT: {
      System.out.println("Logout");
      logout();
      return true;
    }
    case 2: {
      System.out.println("Next");
      startActivity(new Intent(MainActivity.this, SecondActivity.class));
      return true;
    }
    }
    return false;
  }

  private void logout() {
    finish();
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return super.onPrepareOptionsMenu(menu);
  }

}
