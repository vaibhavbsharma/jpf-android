/**
 * Preference screens are used to display a set of settings to the user. Preferences can be displayed
 * in an PreferenceActivity or in a PreferenceFragment shown by a normal Activity class. Preferences
 * can be constructed manually by building a hierarchy using the objects extending the Preferences 
 * class or they can be defined in XML. They are stored in Shared preferences under their own unique key.
 * 
 * Preferences Screens only influence the application in by the fact that they can change the shared prefs.
 * ( I suppose this differs if you have more complex preference screens?)
 * 
 * We don't want the user to physically press the preferences, so instead we return all values when the
 * shared prefs are queued. This reduces the state by not executing all possible pref changes each time the
 * screen is brought up. We can use the cg to set the state from 'n monkeyrunner script. (hopefully)
 * 
 * So for the screen we ignore all interaction with the gui. This means the window is empty. The only event
 * that can be fired is the OnPreferenceChange listener. So when this screen is active we fire it. 
 * 
 * We needed to inflate the Preference Hierarchy and retrieve each PreferenceObject's type, key and default value.
 * This information is use by the application.
 * 
 * The values for shared prefs are set when the hierarchy is inflated:
 * 
 * String --> default value (or recorded values)
 * int --> default value
 * list of strings --> non-determistically all values (or subset if there are too much)
 * boolean --> true and false non-determistically
 * 
 * If not values has been set for a shared pref, a default value is returned.
 * 
 *  
 * If the application is started up, the preference xml has not been loaded yet. In this case the app can call: 
 * PreferenceManager.setDefaultValues(this, R.xml.preference, false);
 * 
 * It will load your preferences from XML, and last parameter (readAgain) will guarantee that user preferences 
 * won't be overwritten if the preference screen is opened?
 * 
 * Since the Preference objects do not form part of the app coverage, we just stub the methods called by the applcation
 * to ensure the app can run using the models.
 * 
 * PreferenceManager is used as an universal controller for  Preferences used by PreferenceFragment and PreferenceActivity. 
 *  
 *  
 * 
 */
package android.preference;