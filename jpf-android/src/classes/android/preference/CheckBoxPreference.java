package android.preference;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.SharedPreferenceEvent;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;

public class CheckBoxPreference extends Preference {

  public CheckBoxPreference(Context context) {
    super(context);
  }

  @FilterField
  boolean checked = false;

  public void setChecked(boolean checked) {
    this.checked = checked;
  }

  public boolean isChecked() {
    return checked;
  }

  @Override
  public List<Event> getEvents() {
    List<Event> ret = new LinkedList<Event>();
    SharedPreferenceEvent e = null;
    if (this.mOnChangeListener != null) {
      e = new SharedPreferenceEvent(this.mKey);
      e.setListener("mOnChangeListener");
      ret.add(e);
    }
    ret.addAll(super.getEvents());
    return ret;
  }

  @Override
  public void processEvent(SharedPreferenceEvent event) {
    if (event.getListener() != null) {
      if (event.getListener().equals("mOnChangeListener") && this.mOnChangeListener != null) {
        this.mOnChangeListener.onPreferenceChange(this, !checked);
      } else {
        super.processEvent(event);
      }
    }
  }
}
