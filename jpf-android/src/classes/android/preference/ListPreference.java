package android.preference;

import gov.nasa.jpf.annotation.FilterField;
import android.content.Context;

public class ListPreference extends android.preference.Preference {
  @FilterField
  CharSequence[] entries;
  @FilterField
  CharSequence[] mEntryValues;
  private String mValue;

  public ListPreference(Context context) {
    super(context);
  }

  public java.lang.String getValue() {
    return mValue;
  }

  public CharSequence getEntry() {
    return entries[0];

  }

  public void setValue(String s) {
    mValue = s;
  }

  public void setValueIndex(int index) {
    if (mEntryValues != null) {
      setValue(mEntryValues[index].toString());
    }
  }

  public void setEntries(String[] strings) {
    this.entries = strings;
  }

  public void setEntries(CharSequence[] strings) {
    this.entries = strings;
  }

  public void setEntryValues(CharSequence[] strings) {
    this.mEntryValues = strings;
  }

  public CharSequence[] getEntryValues() {
    return this.mEntryValues;
  }

  public CharSequence[] getEntries() {
    return entries;

  }

  @Override
  public void setDefaultValue(Object mDefaultValue) {
    this.mDefaultValue = mDefaultValue;
    setValue((String) mDefaultValue);
  }

}