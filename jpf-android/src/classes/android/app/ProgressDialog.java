package android.app;

import java.text.NumberFormat;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class ProgressDialog extends AlertDialog {

  /**
   * Creates a ProgressDialog with a circular, spinning progress bar. This is the default.
   */
  public static final int STYLE_SPINNER = 0;

  /**
   * Creates a ProgressDialog with a horizontal progress bar.
   */
  public static final int STYLE_HORIZONTAL = 1;

  public ProgressDialog(Context context) {
    super(context);
  }

  public ProgressDialog(Context context, int theme) {
    super(context, theme);
  }

  public static ProgressDialog show(Context context, CharSequence title, CharSequence message) {
    return show(context, title, message, false);
  }

  public static ProgressDialog show(Context context, CharSequence title, CharSequence message,
                                    boolean indeterminate) {
    return show(context, title, message, indeterminate, false, null);
  }

  public static ProgressDialog show(Context context, CharSequence title, CharSequence message,
                                    boolean indeterminate, boolean cancelable) {
    return show(context, title, message, indeterminate, cancelable, null);
  }

  public static ProgressDialog show(Context context, CharSequence title, CharSequence message,
                                    boolean indeterminate, boolean cancelable, OnCancelListener cancelListener) {
    ProgressDialog dialog = new ProgressDialog(context);
    dialog.setTitle(title);
    dialog.setMessage(message);
    dialog.setCancelable(cancelable);
    dialog.setOnCancelListener(cancelListener);
    dialog.show();
    return dialog;
  }

  @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  protected void onStop() {
    super.onStop();
  }

  public void setProgress(int value) {
  }

  public void setSecondaryProgress(int secondaryProgress) {
  }

  public int getProgress() {
    return 100;

  }

  public int getSecondaryProgress() {
    return 100;

  }

  public int getMax() {
    return 100;
  }

  public void setMax(int max) {
  }

  public void incrementProgressBy(int diff) {
  }

  public void incrementSecondaryProgressBy(int diff) {
  }

  public void setProgressDrawable(Drawable d) {
  }

  public void setIndeterminateDrawable(Drawable d) {
  }

  public void setIndeterminate(boolean indeterminate) {
  }

  public boolean isIndeterminate() {
    return true;
  }

  @Override
  public void setMessage(CharSequence message) {
  }

  public void setProgressStyle(int style) {
  }

  /**
   * Change the format of the small text showing current and maximum units of progress. The default is
   * "%1d/%2d". Should not be called during the number is progressing.
   * 
   * @param format
   *          A string passed to {@link String#format String.format()}; use "%1d" for the current number and
   *          "%2d" for the maximum. If null, nothing will be shown.
   */
  public void setProgressNumberFormat(String format) {
  }

  /**
   * Change the format of the small text showing the percentage of progress. The default is
   * {@link NumberFormat#getPercentInstance() NumberFormat.getPercentageInstnace().} Should not be called
   * during the number is progressing.
   * 
   * @param format
   *          An instance of a {@link NumberFormat} to generate the percentage text. If null, nothing will be
   *          shown.
   */
  public void setProgressPercentFormat(NumberFormat format) {
  }

}