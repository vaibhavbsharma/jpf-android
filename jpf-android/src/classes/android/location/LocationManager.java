package android.location;

import gov.nasa.jpf.util.event.EventProcessor;
import gov.nasa.jpf.util.event.InvalidEventException;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.LocationEvent;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import android.app.PendingIntent;
import android.os.Bundle;
import android.os.Looper;

public class LocationManager implements EventProcessor {

  // private final Context mContext;
  private final List<GpsStatus.Listener> mGpsStatusListeners = new LinkedList<GpsStatus.Listener>();
  private final List<GpsStatus.NmeaListener> mNmeaListeners = new LinkedList<GpsStatus.NmeaListener>();
  private final GpsStatus mGpsStatus = new GpsStatus();
  private final LocationProvider provider = new DefaultLocationProvider();

  /**
   * Name of the network location provider.
   * <p>
   * This provider determines location based on availability of cell tower and WiFi access points. Results are
   * retrieved by means of a network lookup.
   */
  public static final String NETWORK_PROVIDER = "network";

  /**
   * Name of the GPS location provider.
   *
   * <p>
   * This provider determines location using satellites. Depending on conditions, this provider may take a
   * while to return a location fix. Requires the permission
   * {@link android.Manifest.permission#ACCESS_FINE_LOCATION}.
   *
   * <p>
   * The extras Bundle for the GPS location provider can contain the following key/value pairs:
   * <ul>
   * <li>satellites - the number of satellites used to derive the fix
   * </ul>
   */
  public static final String GPS_PROVIDER = "gps";

  /**
   * A special location provider for receiving locations without actually initiating a location fix.
   *
   * <p>
   * This provider can be used to passively receive location updates when other applications or services
   * request them without actually requesting the locations yourself. This provider will return locations
   * generated by other providers. You can query the {@link Location#getProvider()} method to determine the
   * origin of the location update. Requires the permission
   * {@link android.Manifest.permission#ACCESS_FINE_LOCATION}, although if the GPS is not enabled this
   * provider might only return coarse fixes.
   */
  public static final String PASSIVE_PROVIDER = "passive";

  /**
   * Name of the Fused location provider.
   *
   * <p>
   * This provider combines inputs for all possible location sources to provide the best possible Location
   * fix. It is implicitly used for all API's that involve the {@link LocationRequest} object.
   *
   * @hide
   */
  public static final String FUSED_PROVIDER = "fused";

  /**
   * Key used for the Bundle extra holding a boolean indicating whether a proximity alert is entering (true)
   * or exiting (false)..
   */
  public static final String KEY_PROXIMITY_ENTERING = "entering";

  /**
   * Key used for a Bundle extra holding an Integer status value when a status change is broadcast using a
   * PendingIntent.
   */
  public static final String KEY_STATUS_CHANGED = "status";

  /**
   * Key used for a Bundle extra holding an Boolean status value when a provider enabled/disabled event is
   * broadcast using a PendingIntent.
   */
  public static final String KEY_PROVIDER_ENABLED = "providerEnabled";

  /**
   * Key used for a Bundle extra holding a Location value when a location change is broadcast using a
   * PendingIntent.
   */
  public static final String KEY_LOCATION_CHANGED = "location";

  /**
   * Broadcast intent action indicating that the GPS has either been enabled or disabled. An intent extra
   * provides this state as a boolean, where {@code true} means enabled.
   * 
   * @see #EXTRA_GPS_ENABLED
   *
   * @hide
   */
  public static final String GPS_ENABLED_CHANGE_ACTION = "android.location.GPS_ENABLED_CHANGE";

  /**
   * Broadcast intent action when the configured location providers change. For use with
   * {@link #isProviderEnabled(String)}. If you're interacting with the
   * {@link android.provider.Settings.Secure#LOCATION_MODE} API, use {@link #MODE_CHANGED_ACTION} instead.
   */
  public static final String PROVIDERS_CHANGED_ACTION = "android.location.PROVIDERS_CHANGED";

  /**
   * Broadcast intent action when {@link android.provider.Settings.Secure#LOCATION_MODE} changes. For use with
   * the {@link android.provider.Settings.Secure#LOCATION_MODE} API. If you're interacting with
   * {@link #isProviderEnabled(String)}, use {@link #PROVIDERS_CHANGED_ACTION} instead.
   *
   * In the future, there may be mode changes that do not result in {@link #PROVIDERS_CHANGED_ACTION}
   * broadcasts.
   */
  public static final String MODE_CHANGED_ACTION = "android.location.MODE_CHANGED";

  /**
   * Broadcast intent action indicating that the GPS has either started or stopped receiving GPS fixes. An
   * intent extra provides this state as a boolean, where {@code true} means that the GPS is actively
   * receiving fixes.
   * 
   * @see #EXTRA_GPS_ENABLED
   *
   * @hide
   */
  public static final String GPS_FIX_CHANGE_ACTION = "android.location.GPS_FIX_CHANGE";

  /**
   * The lookup key for a boolean that indicates whether GPS is enabled or disabled. {@code true} means GPS is
   * enabled. Retrieve it with {@link android.content.Intent#getBooleanExtra(String,boolean)}.
   *
   * @hide
   */
  public static final String EXTRA_GPS_ENABLED = "enabled";

  /**
   * Broadcast intent action indicating that a high power location requests has either started or stopped
   * being active. The current state of active location requests should be read from AppOpsManager using
   * {@code OP_MONITOR_HIGH_POWER_LOCATION}.
   *
   * @hide
   */
  public static final String HIGH_POWER_REQUEST_CHANGE_ACTION = "android.location.HIGH_POWER_REQUEST_CHANGE";

  // Map from LocationListeners to their associated ListenerTransport objects
  private HashSet<LocationListener> mListeners = new HashSet<LocationListener>();

  public LocationManager() {
  }

  public List<String> getAllProviders() {
    List<String> ret = new LinkedList<String>();
    ret.add("defaultprovider");
    return ret;

  }

  public List<String> getProviders(boolean enabledOnly) {
    List<String> ret = new LinkedList<String>();
    ret.add("defaultprovider");
    return ret;
  }

  public LocationProvider getProvider(String name) {
    return provider;
  }

  public List<String> getProviders(Criteria criteria, boolean enabledOnly) {
    List<String> ret = new LinkedList<String>();
    ret.add("defaultprovider");
    return ret;
  }

  public String getBestProvider(Criteria criteria, boolean enabledOnly) {
    return "defaultprovider";
  }

  public void requestLocationUpdates(String provider, long minTime, float minDistance,
                                     LocationListener listener) {
    mListeners.add(listener);

  }

  public void requestLocationUpdates(String provider, long minTime, float minDistance,
                                     LocationListener listener, Looper looper) {
    mListeners.add(listener);

  }

  public void requestLocationUpdates(long minTime, float minDistance, Criteria criteria,
                                     LocationListener listener, Looper looper) {
    mListeners.add(listener);

  }

  public void requestLocationUpdates(String provider, long minTime, float minDistance, PendingIntent intent) {
    // mListeners.add(listener);

  }

  public void requestLocationUpdates(long minTime, float minDistance, Criteria criteria, PendingIntent intent) {
    // mListeners.add(listener);

  }

  public void requestSingleUpdate(String provider, LocationListener listener, Looper looper) {
    mListeners.add(listener);

  }

  public void requestSingleUpdate(Criteria criteria, LocationListener listener, Looper looper) {
    mListeners.add(listener);

  }

  public void requestSingleUpdate(String provider, PendingIntent intent) {

  }

  public void requestSingleUpdate(Criteria criteria, PendingIntent intent) {

  }

  public void requestLocationUpdates(LocationRequest request, LocationListener listener, Looper looper) {
    mListeners.add(listener);

  }

  public void requestLocationUpdates(LocationRequest request, PendingIntent intent) {

  }

  public void removeUpdates(LocationListener listener) {
    mListeners.remove(listener);

  }

  public void removeUpdates(PendingIntent intent) {

  }

  public void addProximityAlert(double latitude, double longitude, float radius, long expiration,
                                PendingIntent intent) {

  }

  public void removeGeofence(Geofence fence, PendingIntent intent) {
  }

  public void removeAllGeofences(PendingIntent intent) {

  }

  public void addGeofence(LocationRequest request, Geofence fence, PendingIntent intent) {
  }

  public void removeProximityAlert(PendingIntent intent) {
  }

  public boolean isProviderEnabled(String provider) {
    return true;
  }

  public Location getLastKnownLocation(String provider) {
    return new Location();
  }

  public Location getLastLocation() {
    return new Location();

  }

  public boolean sendExtraCommand(String provider, String command, Bundle extras) {
    return true;

  }

  public boolean sendNiResponse(int notifId, int userResponse) {
    return true;

  }

  public boolean addGpsStatusListener(GpsStatus.Listener listener) {

    if (mGpsStatusListeners.contains(listener)) {
      // listener is already registered
      return true;
    }
    mGpsStatusListeners.add(listener);

    return true;
  }

  /**
   * Removes a GPS status listener.
   *
   * @param listener
   *          GPS status listener object to remove
   */
  public void removeGpsStatusListener(GpsStatus.Listener listener) {
  }

  /**
   * Adds an NMEA listener.
   *
   * @param listener
   *          a {@link GpsStatus.NmeaListener} object to register
   *
   * @return true if the listener was successfully added
   *
   * @throws SecurityException
   *           if the ACCESS_FINE_LOCATION permission is not present
   */
  public boolean addNmeaListener(GpsStatus.NmeaListener listener) {

    if (mNmeaListeners.contains(listener)) {
      // listener is already registered
      return true;
    }
    mNmeaListeners.add(listener);

    return true;
  }

  /**
   * Removes an NMEA listener.
   *
   * @param listener
   *          a {@link GpsStatus.NmeaListener} object to remove
   */
  public void removeNmeaListener(GpsStatus.NmeaListener listener) {
  }

  /**
   * Retrieves information about the current status of the GPS engine. This should only be called from the
   * {@link GpsStatus.Listener#onGpsStatusChanged} callback to ensure that the data is copied atomically.
   *
   * The caller may either pass in a {@link GpsStatus} object to set with the latest status information, or
   * pass null to create a new {@link GpsStatus} object.
   *
   * @param status
   *          object containing GPS status details, or null.
   * @return status object containing updated GPS status.
   */
  public GpsStatus getGpsStatus(GpsStatus status) {
    if (status == null) {
      status = new GpsStatus();
    }
    status.setStatus(mGpsStatus);
    return status;
  }

  @Override
  public Event[] getEvents() {
    List<Event> ret = new LinkedList<Event>();
    System.out.println("Getting location Events");
    if (mListeners != null && mListeners.size() > 0) {
      LocationEvent e = new LocationEvent();
      e.setListener("onLocationChanged");
      ret.add(e);
      e = new LocationEvent();
      e.setListener("onStatusChanged");
      ret.add(e);
      e = new LocationEvent();
      e.setListener("onProviderEnabled");
      ret.add(e);
      e = new LocationEvent();
      e.setListener("onProviderDisabled");
      ret.add(e);
    }

    if (mGpsStatusListeners != null && mGpsStatusListeners.size() > 0) {
      LocationEvent e = new LocationEvent();
      e.setListener("onGpsStatusChanged");
      ret.add(e);
    }
    return ret.toArray(new Event[ret.size()]);
  }

  @Override
  public void processEvent(Event event) throws InvalidEventException {
    if (((LocationEvent) event).getListener().equals("onLocationChanged")) {
      for (LocationListener l : mListeners) {
        l.onLocationChanged(new Location());
      }
    } else if (((LocationEvent) event).getListener().equals("onStatusChanged")) {
      for (LocationListener l : mListeners) {
        l.onStatusChanged("defaultprovider", LocationProvider.AVAILABLE, new Bundle());
      }
    } else if (((LocationEvent) event).getListener().equals("onProviderEnabled")) {
      for (LocationListener l : mListeners) {
        l.onProviderEnabled("defaultprovider");
      }
    } else if (((LocationEvent) event).getListener().equals("onProviderDisabled")) {
      for (LocationListener l : mListeners) {
        l.onProviderDisabled("defaultprovider");
      }
    } else if (((LocationEvent) event).getListener().equals("onGpsStatusChanged")) {
      for (GpsStatus.Listener l : mGpsStatusListeners) {
        l.onGpsStatusChanged(GpsStatus.GPS_EVENT_STARTED);
        l.onGpsStatusChanged(GpsStatus.GPS_EVENT_FIRST_FIX);
        l.onGpsStatusChanged(GpsStatus.GPS_EVENT_SATELLITE_STATUS);
        l.onGpsStatusChanged(GpsStatus.GPS_EVENT_STOPPED);
      }
    }

  }
}