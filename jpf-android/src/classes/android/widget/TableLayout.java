package android.widget;

import android.content.Context;
import android.util.AttributeSet;

public class TableLayout extends LinearLayout {

  public TableLayout(Context context) {
    super(context);
  }

  public TableLayout(Context context, AttributeSet attrs) {
    super(context);
  }

}
