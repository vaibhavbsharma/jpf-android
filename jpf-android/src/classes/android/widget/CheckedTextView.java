package android.widget;

/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewDebug;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.android.internal.R;

/**
 * An extension to TextView that supports the {@link android.widget.Checkable} interface. This is useful when
 * used in a {@link android.widget.ListView ListView} where the it's
 * {@link android.widget.ListView#setChoiceMode(int) setChoiceMode} has been set to something other than
 * {@link android.widget.ListView#CHOICE_MODE_NONE CHOICE_MODE_NONE}.
 *
 * @attr ref android.R.styleable#CheckedTextView_checked
 * @attr ref android.R.styleable#CheckedTextView_checkMark
 */
public class CheckedTextView extends TextView implements Checkable {
  private boolean mChecked;
  private int mCheckMarkResource;
  private Drawable mCheckMarkDrawable;
  private int mBasePadding;
  private int mCheckMarkWidth;
  private boolean mNeedRequestlayout;

  private static final int[] CHECKED_STATE_SET = { R.attr.state_checked };

  public CheckedTextView(Context context) {
    this(context, null);
  }

  public CheckedTextView(Context context, AttributeSet attrs) {
    this(context, attrs, R.attr.checkedTextViewStyle);
  }

  public CheckedTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

  }

  @Override
  public void toggle() {
    setChecked(!mChecked);
  }

  @Override
  @ViewDebug.ExportedProperty
  public boolean isChecked() {
    return mChecked;
  }

  /**
   * <p>
   * Changes the checked state of this text view.
   * </p>
   *
   * @param checked
   *          true to check the text, false to uncheck it
   */
  @Override
  public void setChecked(boolean checked) {
    mChecked = checked;
  }

  /**
   * Set the checkmark to a given Drawable, identified by its resourece id. This will be drawn when
   * {@link #isChecked()} is true.
   *
   * @param resid
   *          The Drawable to use for the checkmark.
   *
   * @see #setCheckMarkDrawable(Drawable)
   * @see #getCheckMarkDrawable()
   *
   * @attr ref android.R.styleable#CheckedTextView_checkMark
   */
  public void setCheckMarkDrawable(int resid) {
  }

  /**
   * Set the checkmark to a given Drawable. This will be drawn when {@link #isChecked()} is true.
   *
   * @param d
   *          The Drawable to use for the checkmark.
   *
   * @see #setCheckMarkDrawable(int)
   * @see #getCheckMarkDrawable()
   *
   * @attr ref android.R.styleable#CheckedTextView_checkMark
   */
  public void setCheckMarkDrawable(Drawable d) {
  }

  /**
   * Gets the checkmark drawable
   *
   * @return The drawable use to represent the checkmark, if any.
   *
   * @see #setCheckMarkDrawable(Drawable)
   * @see #setCheckMarkDrawable(int)
   *
   * @attr ref android.R.styleable#CheckedTextView_checkMark
   */
  public Drawable getCheckMarkDrawable() {
    return Drawable.TOP;
  }

  protected void internalSetPadding(int left, int top, int right, int bottom) {
  }

  @Override
  public void onRtlPropertiesChanged(int layoutDirection) {
  }

  private void updatePadding() {
  }

  private void setBasePadding(boolean isLayoutRtl) {
  }

  @Override
  protected void onDraw(Canvas canvas) {
  }

  @Override
  protected int[] onCreateDrawableState(int extraSpace) {
    return new int[] { extraSpace };
  }

  @Override
  protected void drawableStateChanged() {
  }

  @Override
  public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
  }

  @Override
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo info) {
  }
}
