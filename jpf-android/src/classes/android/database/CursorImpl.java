package android.database;

import gov.nasa.jpf.vm.Abstraction;
import android.content.ContentResolver;
import android.net.Uri;
import android.os.Bundle;

public class CursorImpl implements android.database.Cursor {

  @Override
  public boolean moveToFirst() {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public int getColumnIndex(java.lang.String param0) {
    return Abstraction.TOP_INT;
  }

  @Override
  public java.lang.String getString(int param0) {
    return Abstraction.TOP_STRING;
  }

  @Override
  public long getLong(int param0) {
    return Abstraction.TOP_LONG;
  }

  @Override
  public boolean moveToNext() {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public int getCount() {
    return Abstraction.TOP_INT;
  }

  @Override
  public int getPosition() {
    return 0;
  }

  @Override
  public boolean move(int offset) {
    return false;
  }

  @Override
  public boolean moveToPosition(int position) {
    return true;
  }

  @Override
  public boolean moveToLast() {
    return false;
  }

  @Override
  public boolean moveToPrevious() {
    return false;
  }

  @Override
  public boolean isFirst() {
    return false;
  }

  @Override
  public boolean isLast() {
    return false;
  }

  @Override
  public boolean isBeforeFirst() {
    return false;
  }

  @Override
  public boolean isAfterLast() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public String getColumnName(int columnIndex) {
    return Abstraction.TOP_STRING;
  }

  @Override
  public String[] getColumnNames() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public int getColumnCount() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public byte[] getBlob(int columnIndex) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
    // TODO Auto-generated method stub

  }

  @Override
  public short getShort(int columnIndex) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getInt(int columnIndex) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public float getFloat(int columnIndex) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public double getDouble(int columnIndex) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getType(int columnIndex) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public boolean isNull(int columnIndex) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void deactivate() {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean requery() {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public void close() {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean isClosed() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void registerContentObserver(ContentObserver observer) {
    // TODO Auto-generated method stub

  }

  @Override
  public void unregisterContentObserver(ContentObserver observer) {
    // TODO Auto-generated method stub

  }

  @Override
  public void registerDataSetObserver(DataSetObserver observer) {
    // TODO Auto-generated method stub

  }

  @Override
  public void unregisterDataSetObserver(DataSetObserver observer) {
    // TODO Auto-generated method stub

  }

  @Override
  public void setNotificationUri(ContentResolver cr, Uri uri) {
    // TODO Auto-generated method stub

  }

  @Override
  public Uri getNotificationUri() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean getWantsAllOnMoveCalls() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public Bundle getExtras() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Bundle respond(Bundle extras) {
    // TODO Auto-generated method stub
    return null;
  }
}