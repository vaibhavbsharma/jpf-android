package android.view;

import java.util.Arrays;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ActionMode.Callback;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.LayoutAnimationController;

/**
 * An extension to View. It contains a list of child Views.
 * 
 * @author "Heila van der Merwe"
 * 
 */
public class ViewGroup extends View implements ViewParent {

  public static class LayoutParams {

    public static final LayoutParams TOP = new LayoutParams();
    /**
     * Special value for the height or width requested by a View. FILL_PARENT means that the view wants to be
     * as big as its parent, minus the parent's padding, if any. This value is deprecated starting in API
     * Level 8 and replaced by {@link #MATCH_PARENT}.
     */
    @Deprecated
    public static final int FILL_PARENT = -1;

    /**
     * Special value for the height or width requested by a View. MATCH_PARENT means that the view wants to be
     * as big as its parent, minus the parent's padding, if any. Introduced in API Level 8.
     */
    public static final int MATCH_PARENT = -1;

    /**
     * Special value for the height or width requested by a View. WRAP_CONTENT means that the view wants to be
     * just large enough to fit its own internal content, taking its own padding into account.
     */
    public static final int WRAP_CONTENT = -2;

    /**
     * Information about how wide the view wants to be. Can be one of the constants FILL_PARENT (replaced by
     * MATCH_PARENT , in API Level 8) or WRAP_CONTENT. or an exact size.
     */
    public int width;

    /**
     * Information about how tall the view wants to be. Can be one of the constants FILL_PARENT (replaced by
     * MATCH_PARENT , in API Level 8) or WRAP_CONTENT. or an exact size.
     */
    public int height;

    /**
     * Used to animate layouts.
     */
    public LayoutAnimationController.AnimationParameters layoutAnimationParameters;

    /**
     * Creates a new set of layout parameters. The values are extracted from the supplied attributes set and
     * context. The XML attributes mapped to this set of layout parameters are:
     *
     * <ul>
     * <li><code>layout_width</code>: the width, either an exact value, {@link #WRAP_CONTENT}, or
     * {@link #FILL_PARENT} (replaced by {@link #MATCH_PARENT} in API Level 8)</li>
     * <li><code>layout_height</code>: the height, either an exact value, {@link #WRAP_CONTENT}, or
     * {@link #FILL_PARENT} (replaced by {@link #MATCH_PARENT} in API Level 8)</li>
     * </ul>
     *
     * @param c
     *          the application environment
     * @param attrs
     *          the set of attributes from which to extract the layout parameters' values
     */
    public LayoutParams(Context c, AttributeSet attrs) {
    }

    /**
     * Creates a new set of layout parameters with the specified width and height.
     *
     * @param width
     *          the width, either {@link #WRAP_CONTENT}, {@link #FILL_PARENT} (replaced by
     *          {@link #MATCH_PARENT} in API Level 8), or a fixed size in pixels
     * @param height
     *          the height, either {@link #WRAP_CONTENT}, {@link #FILL_PARENT} (replaced by
     *          {@link #MATCH_PARENT} in API Level 8), or a fixed size in pixels
     */
    public LayoutParams(int width, int height) {
    }

    /**
     * Copy constructor. Clones the width and height values of the source.
     *
     * @param source
     *          The layout params to copy from.
     */
    public LayoutParams(LayoutParams source) {
    }

    /**
     * Used internally by MarginLayoutParams.
     * 
     * @hide
     */
    LayoutParams() {
    }

    /**
     * Extracts the layout parameters from the supplied attributes.
     *
     * @param a
     *          the style attributes to extract the parameters from
     * @param widthAttr
     *          the identifier of the width attribute
     * @param heightAttr
     *          the identifier of the height attribute
     */
    protected void setBaseAttributes(TypedArray a, int widthAttr, int heightAttr) {
    }

    /**
     * Resolve layout parameters depending on the layout direction. Subclasses that care about layoutDirection
     * changes should override this method. The default implementation does nothing.
     *
     * @param layoutDirection
     *          the direction of the layout
     *
     *          {@link View#LAYOUT_DIRECTION_LTR} {@link View#LAYOUT_DIRECTION_RTL}
     */
    public void resolveLayoutDirection(int layoutDirection) {
    }

    /**
     * Returns a String representation of this set of layout parameters.
     *
     * @param output
     *          the String to prepend to the internal representation
     * @return a String with the following format: output +
     *         "ViewGroup.LayoutParams={ width=WIDTH, height=HEIGHT }"
     *
     * @hide
     */
    public String debug(String output) {
      return "ViewGroup.LayoutParams={}";
    }

    /**
     * Use {@code canvas} to draw suitable debugging annotations for these LayoutParameters.
     *
     * @param view
     *          the view that contains these layout parameters
     * @param canvas
     *          the canvas on which to draw
     *
     * @hide
     */
    public void onDebugDraw(View view, Canvas canvas, Paint paint) {
    }

    /**
     * Converts the specified size to a readable String.
     *
     * @param size
     *          the size to convert
     * @return a String instance representing the supplied size
     *
     * @hide
     */
    protected static String sizeToString(int size) {
      return "match-parent";
    }
  }

  public static class MarginLayoutParams extends ViewGroup.LayoutParams {
    /**
     * The left margin in pixels of the child. Call {@link ViewGroup#setLayoutParams(LayoutParams)} after
     * reassigning a new value to this field.
     */
    public int leftMargin;

    /**
     * The top margin in pixels of the child. Call {@link ViewGroup#setLayoutParams(LayoutParams)} after
     * reassigning a new value to this field.
     */
    public int topMargin;

    /**
     * The right margin in pixels of the child. Call {@link ViewGroup#setLayoutParams(LayoutParams)} after
     * reassigning a new value to this field.
     */
    public int rightMargin;

    /**
     * The bottom margin in pixels of the child. Call {@link ViewGroup#setLayoutParams(LayoutParams)} after
     * reassigning a new value to this field.
     */
    public int bottomMargin;

    /**
     * The default start and end margin.
     * 
     * @hide
     */
    public static final int DEFAULT_MARGIN_RELATIVE = Integer.MIN_VALUE;

    /**
     * Bit 0: layout direction Bit 1: layout direction Bit 2: left margin undefined Bit 3: right margin
     * undefined Bit 4: is RTL compatibility mode Bit 5: need resolution
     *
     * Bit 6 to 7 not used
     *
     * @hide
     */
    byte mMarginFlags;

    /**
     * Creates a new set of layout parameters. The values are extracted from the supplied attributes set and
     * context.
     *
     * @param c
     *          the application environment
     * @param attrs
     *          the set of attributes from which to extract the layout parameters' values
     */
    public MarginLayoutParams(Context c, AttributeSet attrs) {
      super();
    }

    /**
     * {@inheritDoc}
     */
    public MarginLayoutParams(int width, int height) {
      super(width, height);
    }

    /**
     * Copy constructor. Clones the width, height and margin values of the source.
     *
     * @param source
     *          The layout params to copy from.
     */
    public MarginLayoutParams(MarginLayoutParams source) {

    }

    /**
     * {@inheritDoc}
     */
    public MarginLayoutParams(LayoutParams source) {
      super(source);

    }

    /**
     * Sets the margins, in pixels. A call to {@link android.view.View#requestLayout()} needs to be done so
     * that the new margins are taken into account. Left and right margins may be overriden by
     * {@link android.view.View#requestLayout()} depending on layout direction.
     *
     * @param left
     *          the left margin size
     * @param top
     *          the top margin size
     * @param right
     *          the right margin size
     * @param bottom
     *          the bottom margin size
     *
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginLeft
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginTop
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginRight
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginBottom
     */
    public void setMargins(int left, int top, int right, int bottom) {
    }

    /**
     * Sets the relative margins, in pixels. A call to {@link android.view.View#requestLayout()} needs to be
     * done so that the new relative margins are taken into account. Left and right margins may be overriden
     * by {@link android.view.View#requestLayout()} depending on layout direction.
     *
     * @param start
     *          the start margin size
     * @param top
     *          the top margin size
     * @param end
     *          the right margin size
     * @param bottom
     *          the bottom margin size
     *
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginStart
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginTop
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginEnd
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginBottom
     *
     * @hide
     */
    public void setMarginsRelative(int start, int top, int end, int bottom) {
    }

    /**
     * Sets the relative start margin.
     *
     * @param start
     *          the start margin size
     *
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginStart
     */
    public void setMarginStart(int start) {
    }

    /**
     * Returns the start margin in pixels.
     *
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginStart
     *
     * @return the start margin in pixels.
     */
    public int getMarginStart() {
      return 10;
    }

    /**
     * Sets the relative end margin.
     *
     * @param end
     *          the end margin size
     *
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginEnd
     */
    public void setMarginEnd(int end) {
    }

    /**
     * Returns the end margin in pixels.
     *
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginEnd
     *
     * @return the end margin in pixels.
     */
    public int getMarginEnd() {
      return 10;
    }

    /**
     * Check if margins are relative.
     *
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginStart
     * @attr ref android.R.styleable#ViewGroup_MarginLayout_layout_marginEnd
     *
     * @return true if either marginStart or marginEnd has been set.
     */
    public boolean isMarginRelative() {
      return true;
    }

    /**
     * Set the layout direction
     * 
     * @param layoutDirection
     *          the layout direction. Should be either {@link View#LAYOUT_DIRECTION_LTR} or
     *          {@link View#LAYOUT_DIRECTION_RTL}.
     */
    public void setLayoutDirection(int layoutDirection) {
    }

    /**
     * Retuns the layout direction. Can be either {@link View#LAYOUT_DIRECTION_LTR} or
     * {@link View#LAYOUT_DIRECTION_RTL}.
     *
     * @return the layout direction.
     */
    public int getLayoutDirection() {
      return View.LAYOUT_DIRECTION_RTL;
    }

    /**
     * This will be called by {@link android.view.View#requestLayout()}. Left and Right margins may be
     * overridden depending on layout direction.
     */
    @Override
    public void resolveLayoutDirection(int layoutDirection) {
    }

    private void doResolveMargins() {
    }

    /**
     * @hide
     */
    public boolean isLayoutRtl() {
      return true;
    }

    /**
     * @hide
     */
    @Override
    public void onDebugDraw(View view, Canvas canvas, Paint paint) {
    }
  }

  private static final int ARRAY_INITIAL_CAPACITY = 12;
  private static final int ARRAY_CAPACITY_INCREMENT = 12;
  private int mLastTouchDownIndex = -1;

  public ViewGroup(Context context) {
    super(context);
  }

  public ViewGroup(Context context, AttributeSet att) {
    this(context);
  }

  public ViewGroup(Context context, AttributeSet attrs, int defStyle) {
    this(context);
  }

  // The view contained within this ViewGroup that has or contains focus.
  private View mFocused;
  private int mChildrenCount = 0;
  // Child views of this ViewGroup
  private View[] mChildren = new View[ARRAY_INITIAL_CAPACITY];

  public View[] getChildren() {
    return mChildren;
  }

  @Override
  public boolean dispatchKeyEvent(android.view.KeyEvent event) {
    final View[] where = mChildren;
    final int len = mChildren.length;
    View child = null;
    for (int i = 0; i < len; i++) {
      child = where[i];
      if (child != null) {
        boolean done = child.dispatchKeyEvent(event);
        if (done) {
          return true;
        }
      }
    }
    return super.dispatchKeyEvent(event);
  }

  @Override
  protected View findViewTraversal(int id) {
    if (JPFWindow.DEBUG_WINDOW)
      Log.i("Viewgroup:" + ViewGroup.this.getClass().getName(), "mID=" + mID + " findViewById(id=" + id
          + " found=" + (id == mID) + ")");

    if (id == mID) {
      return this;
    }

    final View[] where = mChildren;
    final int len = mChildren.length;
    if (JPFWindow.DEBUG_WINDOW)
      Log.i("Viewgroup:" + ViewGroup.this.getClass().getName(),
          "Searching " + len + " children " + Arrays.toString(mChildren));
    View child = null;
    for (int i = 0; i < len; i++) {
      child = where[i];
      if (JPFWindow.DEBUG_WINDOW)
        Log.i("Viewgroup:" + ViewGroup.this.getClass().getName(), "Searching " + child);
      // if ((v.mPrivateFlags & IS_ROOT_NAMESPACE) == 0) {
      if (child != null) {
        View foundChild = child.findViewTraversal(id);
        if (foundChild != null) {
          return foundChild;
        }
      } else {
//        System.out.println(this.toString() + " Could not find " + id);
        break;
      }
      // }
    }

    return null;
  }

  /**
   * Adds a child view. If no layout parameters are already set on the child, the default parameters for this
   * ViewGroup are set on the child.
   * 
   * @param child
   *          the child view to add
   * 
   * @see #generateDefaultLayoutParams()
   */
  public void addView(View child) {
    addView(child, -1);
  }

  /**
   * Adds a child view with the specified layout parameters.
   * 
   * @param child
   *          the child view to add
   * @param index
   *          the position at which to add the child
   * @param params
   *          the layout parameters to set on the child
   */
  public void addView(View child, int index) {
    addViewInner(child, index, false);
  }

  /**
   * Adds a child view with the specified layout parameters.
   * 
   * @param child
   *          the child view to add
   * @param index
   *          the position at which to add the child
   * @param params
   *          the layout parameters to set on the child
   */
  public void addView(View child, LayoutParams params) {
    addView(child);
  }

  private void addViewInner(View child, int index, boolean preventRequestLayout) {

    if (child.mParent != null) {
      // System.out.println("Child: "+ child.name);
      // System.out.println("Child's parent " + child.mParent);
      throw new IllegalStateException("The specified child already has a parent. "
          + "You must call removeView() on the child's parent first.");
    }

    if (index < 0) {
      index = mChildrenCount;
    }

    addInArray(child, index);
    child.mParent = this;
    //
    // if (child.hasFocus()) {
    // requestChildFocus(child, child.findFocus());
    // }

    // onViewAdded(child);

  }

  public void removeViews() {
    mChildrenCount = 0;
    // Child views of this ViewGroup
    mChildren = new View[ARRAY_INITIAL_CAPACITY];
  }

  public void removeAllViews() {
    removeViews();
    requestLayout();
    // invalidate(true);
  }

  private void addInArray(View child, int index) {
    View[] children = mChildren;
    final int count = mChildrenCount;
    final int size = children.length;
    if (index == count) {
      if (size == count) {
        mChildren = new View[size + ARRAY_CAPACITY_INCREMENT];
        System.arraycopy(children, 0, mChildren, 0, size);
        children = mChildren;
      }
      children[mChildrenCount++] = child;
    } else if (index < count) {
      if (size == count) {
        mChildren = new View[size + ARRAY_CAPACITY_INCREMENT];
        System.arraycopy(children, 0, mChildren, 0, index);
        System.arraycopy(children, index, mChildren, index + 1, count - index);
        children = mChildren;
      } else {
        System.arraycopy(children, index, children, index + 1, count - index);
      }
      children[index] = child;
      mChildrenCount++;
      if (mLastTouchDownIndex >= index) {
        mLastTouchDownIndex++;
      }
    } else {
      throw new IndexOutOfBoundsException("index=" + index + " count=" + count);
    }
  }

  /**
   * Returns the position in the group of the specified child view.
   * 
   * @param child
   *          the view for which to get the position
   * @return a positive integer representing the position of the view in the group, or -1 if the view does not
   *         exist in the group
   */
  public int indexOfChild(View child) {
    final int count = mChildrenCount;
    final View[] children = mChildren;
    for (int i = 0; i < count; i++) {
      if (children[i] == child) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Returns the number of children in the group.
   * 
   * @return a positive integer representing the number of children in the group
   */
  public int getChildCount() {
    return mChildrenCount;
  }

  /**
   * Returns the view at the specified position in the group.
   * 
   * @param index
   *          the position at which to get the view from
   * @return the view at the specified position or null if the position does not exist within the group
   */
  public View getChildAt(int index) {
    if (index < 0 || index >= mChildrenCount) {
      return null;
    }
    return mChildren[index];
  }

  @Override
  public View findViewTraversal(String name) {
    System.out.println(this.name);

    if (name.equals(this.name)) {
      return this;
    }
    final View[] where = mChildren;
    final int len = mChildren.length;
    View v = null;
    for (int i = 0; i < len; i++) {
      v = where[i];
      if (v != null) {
        v = v.findViewByName(name);
        if (v != null) {
          return v;
        }
      }
    }

    return null;
  }

  @Override
  public View findViewByText(String text) {
    final View[] where = mChildren;
    final int len = mChildren.length;
    View v = null;
    for (int i = 0; i < len; i++) {
      v = where[i];
      if (v != null) {
        v = v.findViewByText(text);
        if (v != null) {
          return v;
        }
      }
    }

    return null;
  }

  public View findViewWithTag(Object o) {
    final View[] where = getChildren();
    final int len = getChildCount();
    View v = null;
    View result = null;
    for (int i = 0; i < len; i++) {
      v = where[i];
      if (v instanceof ViewGroup) {
        result = ((ViewGroup) v).findViewWithTag(o);
        if (result != null)
          return result;
      } else {
        Object tag = v.getTag();
        if (tag != null && tag.equals(o)) {
          return v;
        }
      }
    }

    return null;

  }

  @Override
  public LayoutParams getLayoutParams() {
    if (this.getmParent() != null)
      return this.getmParent().getLayoutParams();
    return this.getLayoutParams();
  }

  @Override
  public void requestTransparentRegion(View child) {
    // TODO Auto-generated method stub

  }

  @Override
  public void invalidateChild(View child, Rect r) {
    // TODO Auto-generated method stub

  }

  @Override
  public ViewParent invalidateChildInParent(int[] location, Rect r) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void requestChildFocus(View child, View focused) {
    // TODO Auto-generated method stub

  }

  @Override
  public void recomputeViewAttributes(View child) {
    // TODO Auto-generated method stub

  }

  @Override
  public void clearChildFocus(View child) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean getChildVisibleRect(View child, Rect r, Point offset) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public View focusSearch(View v, int direction) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void bringChildToFront(View child) {
    // TODO Auto-generated method stub

  }

  @Override
  public void focusableViewAvailable(View v) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean showContextMenuForChild(View originalView) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public ActionMode startActionModeForChild(View originalView, Callback callback) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void childDrawableStateChanged(View child) {
    // TODO Auto-generated method stub

  }

  @Override
  public void requestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean requestChildRectangleOnScreen(View child, Rect rectangle, boolean immediate) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean requestSendAccessibilityEvent(View child, AccessibilityEvent event) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void childHasTransientStateChanged(View child, boolean hasTransientState) {
    // TODO Auto-generated method stub

  }

  @Override
  public ViewParent getParentForAccessibility() {
    return mParent;
  }

  @Override
  public void notifySubtreeAccessibilityStateChanged(View child, View source, int changeType) {
    // TODO Auto-generated method stub

  }

}
