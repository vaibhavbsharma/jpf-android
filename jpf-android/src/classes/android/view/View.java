package android.view;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.UIEvent;
import gov.nasa.jpf.vm.Abstraction;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.LayoutDirection;
import android.util.Log;
import android.util.SparseArray;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;

/**
 * This is a model of the View class. Currently it only stores the listeners of the View and executes these
 * listeners when the onClick method is called.
 * 
 * @author "Heila van der Merwe"
 * 
 */
public class View {

  /**
   * Horizontal layout direction of this view is from Left to Right. Use with {@link #setLayoutDirection}.
   */
  public static final int LAYOUT_DIRECTION_LTR = LayoutDirection.LTR;

  /**
   * Horizontal layout direction of this view is from Right to Left. Use with {@link #setLayoutDirection}.
   */
  public static final int LAYOUT_DIRECTION_RTL = LayoutDirection.RTL;

  /**
   * Horizontal layout direction of this view is inherited from its parent. Use with
   * {@link #setLayoutDirection}.
   */
  public static final int LAYOUT_DIRECTION_INHERIT = LayoutDirection.INHERIT;

  /**
   * Horizontal layout direction of this view is from deduced from the default language script for the locale.
   * Use with {@link #setLayoutDirection}.
   */
  public static final int LAYOUT_DIRECTION_LOCALE = LayoutDirection.LOCALE;

  /**
   * This view is visible. Use with {@link #setVisibility} and <a href="#attr_android:visibility">
   * {@code android:visibility}.
   */
  public static final int VISIBLE = 0x00000000;

  /**
   * This view is invisible, but it still takes up space for layout purposes. Use with {@link #setVisibility}
   * and <a href="#attr_android:visibility"> {@code android:visibility}.
   */
  public static final int INVISIBLE = 0x00000004;

  /**
   * This view is invisible, and it doesn't take any space for layout purposes. Use with
   * {@link #setVisibility} and <a href="#attr_android:visibility"> {@code android:visibility}.
   */
  public static final int GONE = 0x00000008;

  /**
   * Mask for use with setFlags indicating bits used for visibility. {@hide
   * 
   * 
   * }
   */
  static final int VISIBILITY_MASK = 0x0000000C;

  private static final int[] VISIBILITY_FLAGS = { VISIBLE, INVISIBLE, GONE };

  /**
   * Used to mark a View that has no ID.
   */
  public static final int NO_ID = -1;

  private int nativeHash;

  /** ID defined in R file */
  int mID = -1;
  @FilterField
  protected String name = null;

  protected ViewGroup mParent = null;

  public ViewGroup getmParent() {
    return mParent;
  }

  public String getName() {
    return name;
  }

  public void setParent(ViewGroup mParent) {
    this.mParent = mParent;
  }

  public boolean enabled = true;

  public boolean visible = true;
  public boolean focussed = true;
  protected ContextMenu menu;

  public static class ListenerInfo {
    /**
     * Listener used to dispatch focus change events. This field should be made private, so it is hidden from
     * the SDK. {@hide}
     */
    protected OnFocusChangeListener mOnFocusChangeListener;

    /**
     * Listener used to dispatch click events. This field should be made private, so it is hidden from the
     * SDK. {@hide}
     */
    public OnClickListener mOnClickListener;

    /**
     * Listener used to dispatch long click events. This field should be made private, so it is hidden from
     * the SDK. {@hide}
     */
    protected OnLongClickListener mOnLongClickListener;

    private OnKeyListener mOnKeyListener;

    private OnTouchListener mOnTouchListener;

    private OnHoverListener mOnHoverListener;

    private OnGenericMotionListener mOnGenericMotionListener;

    private OnDragListener mOnDragListener;

    public OnCreateContextMenuListener mOnCreateContextMenuListener;

  }

  protected ListenerInfo mListenerInfo = new ListenerInfo();

  /**
   * The application environment this view lives in. This field should be made private, so it is hidden from
   * the SDK. {@hide}
   */
  protected Context mContext;

  private final Resources mResources;

  public View(Context context) {
    mContext = context;
    mResources = context != null ? context.getResources() : null;
  }

  /**
   * Constructor that is called when inflating a view from XML. This is called when a view is being
   * constructed from an XML file, supplying attributes that were specified in the XML file. This version uses
   * a default style of 0, so the only attribute values applied are those in the Context's Theme and the given
   * AttributeSet.
   * 
   * <p>
   * The method onFinishInflate() will be called after all children have been added.
   * 
   * @param context
   *          The Context the view is running in, through which it can access the current theme, resources,
   *          etc.
   * @param attrs
   *          The attributes of the XML tag that is inflating the view.
   * @see #View(Context, AttributeSet, int)
   */
  public View(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  /**
   * Perform inflation from XML and apply a class-specific base style. This constructor of View allows
   * subclasses to use their own base style when they are inflating. For example, a Button class's constructor
   * would call this version of the super class constructor and supply <code>R.attr.buttonStyle</code> for
   * <var>defStyle</var>; this allows the theme's button style to modify all of the base view attributes (in
   * particular its background) as well as the Button class's attributes.
   * 
   * @param context
   *          The Context the view is running in, through which it can access the current theme, resources,
   *          etc.
   * @param attrs
   *          The attributes of the XML tag that is inflating the view.
   * @param defStyle
   *          The default style to apply to this view. If 0, no style will be applied (beyond what is included
   *          in the theme). This may either be an attribute resource, whose value will be retrieved from the
   *          current theme, or an explicit style resource.
   * @see #View(Context, AttributeSet)
   */
  public View(Context context, AttributeSet attrs, int defStyle) {
    this(context);

  }

  public Context getContext() {
    return mContext;
  }

  /**
   * Look for a child view with the given id. If this view has the given id, return this view.
   * 
   * @param id
   *          The id to search for.
   * @return The view that has the given id in the hierarchy or null
   */
  public View findViewById(int id) {
    // if (id < 0) {
    // return null;
    // }
    return findViewTraversal(id);
  }

  public int getId() {
    return mID;
  }

  public void setId(int mID) {
    this.mID = mID;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean isVisible) {
    this.visible = isVisible;
  }

  public ListenerInfo getListenerInfo() {
    return mListenerInfo;
  }

  public void setListenerInfo(ListenerInfo mListenerInfo) {
    this.mListenerInfo = mListenerInfo;
  }

  /**
   * {@hide}
   * 
   * @param id
   *          the id of the view to be found
   * @return the view of the specified id, null if cannot be found
   */
  protected View findViewTraversal(int id) {
    if (JPFWindow.DEBUG_WINDOW)
      Log.i("View: " + View.this.getClass().getName(), "mID=" + mID + " findViewById(" + id + " found="
          + (id == mID) + ")");
    if (id == mID) {
      return this;
    }
    return null;
  }

  public boolean isEnabled() {
    return enabled;
  }

  /**
   * Set the enabled state of this view. The interpretation of the enabled state varies by subclass.
   * 
   * @param enabled
   *          True if this view is enabled, false otherwise.
   */
  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public int getWidth() {
    return 100;
  }

  /**
   * Return the height of your view.
   *
   * @return The height of your view, in pixels.
   */
  public int getHeight() {
    return 100;
  }

  /**
   * Implement this to do your drawing.
   * 
   * @param canvas
   *          the canvas on which the background will be drawn
   */
  protected void onDraw(Canvas canvas) {
  }

  public void draw(Canvas canvas) {
    onDraw(canvas);

  }

  /**
   * Returns this view's tag.
   *
   * @return the Object stored in this view as a tag
   *
   * @see #setTag(Object)
   * @see #getTag(int)
   */
  public Object getTag() {
    return mTag;
  }

  Object mTag;

  /**
   * Sets the tag associated with this view. A tag can be used to mark a view in its hierarchy and does not
   * have to be unique within the hierarchy. Tags can also be used to store data within a view without
   * resorting to another data structure.
   *
   * @param tag
   *          an Object to tag the view with
   *
   * @see #getTag()
   * @see #setTag(int, Object)
   */
  public void setTag(final Object tag) {
    mTag = tag;
  }

  /**
   * Store this view hierarchy's frozen state into the given container.
   * 
   * @param container
   *          The SparseArray in which to save the view's state.
   * 
   * @see #restoreHierarchyState(android.util.SparseArray)
   * @see #dispatchSaveInstanceState(android.util.SparseArray)
   * @see #onSaveInstanceState()
   */
  public void saveHierarchyState(SparseArray<Parcelable> container) {
    dispatchSaveInstanceState(container);
  }

  /**
   * Called by {@link #saveHierarchyState(android.util.SparseArray)} to store the state for this view and its
   * children. May be overridden to modify how freezing happens to a view's children; for example, some views
   * may want to not store state for their children.
   * 
   * @param container
   *          The SparseArray in which to save the view's state.
   * 
   * @see #dispatchRestoreInstanceState(android.util.SparseArray)
   * @see #saveHierarchyState(android.util.SparseArray)
   * @see #onSaveInstanceState()
   */
  protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
    // if (mID != NO_ID ) {
    // Parcelable state = onSaveInstanceState();
    // if ((mPrivateFlags & SAVE_STATE_CALLED) == 0) {
    // throw new
    // IllegalStateException("Derived class did not call super.onSaveInstanceState()");
    // }
    // if (state != null) {
    // // Log.i("View", "Freezing #" + Integer.toHexString(mID)
    // // + ": " + state);
    // container.put(mID, state);
    // }
    // }
  }

  /**
   * Hook allowing a view to generate a representation of its internal state that can later be used to create
   * a new instance with that same state. This state should only contain information that is not persistent or
   * can not be reconstructed later. For example, you will never store your current position on screen because
   * that will be computed again when a new instance of the view is placed in its view hierarchy.
   * <p>
   * Some examples of things you may store here: the current cursor position in a text view (but usually not
   * the text itself since that is stored in a content provider or other persistent storage), the currently
   * selected item in a list view.
   * 
   * @return Returns a Parcelable object containing the view's current dynamic state, or null if there is
   *         nothing interesting to save. The default implementation returns null.
   * @see #onRestoreInstanceState(android.os.Parcelable)
   * @see #saveHierarchyState(android.util.SparseArray)
   * @see #dispatchSaveInstanceState(android.util.SparseArray)
   * @see #setSaveEnabled(boolean)
   */
  protected Parcelable onSaveInstanceState() {
    // mPrivateFlags |= SAVE_STATE_CALLED;
    // return BaseSavedState.EMPTY_STATE;
    return null;
  }

  /**
   * Restore this view hierarchy's frozen state from the given container.
   * 
   * @param container
   *          The SparseArray which holds previously frozen states.
   * 
   * @see #saveHierarchyState(android.util.SparseArray)
   * @see #dispatchRestoreInstanceState(android.util.SparseArray)
   * @see #onRestoreInstanceState(android.os.Parcelable)
   */
  public void restoreHierarchyState(SparseArray<Parcelable> container) {
    dispatchRestoreInstanceState(container);
  }

  /**
   * Called by {@link #restoreHierarchyState(android.util.SparseArray)} to retrieve the state for this view
   * and its children. May be overridden to modify how restoring happens to a view's children; for example,
   * some views may want to not store state for their children.
   * 
   * @param container
   *          The SparseArray which holds previously saved state.
   * 
   * @see #dispatchSaveInstanceState(android.util.SparseArray)
   * @see #restoreHierarchyState(android.util.SparseArray)
   * @see #onRestoreInstanceState(android.os.Parcelable)
   */
  protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
    // if (mID != NO_ID) {
    // Parcelable state = container.get(mID);
    // if (state != null) {
    // // Log.i("View", "Restoreing #" + Integer.toHexString(mID)
    // // + ": " + state);
    // mPrivateFlags &= ~SAVE_STATE_CALLED;
    // onRestoreInstanceState(state);
    // if ((mPrivateFlags & SAVE_STATE_CALLED) == 0) {
    // throw new
    // IllegalStateException("Derived class did not call super.onRestoreInstanceState()");
    // }
    // }
    // }
  }

  /**
   * Hook allowing a view to re-apply a representation of its internal state that had previously been
   * generated by {@link #onSaveInstanceState}. This function will never be called with a null state.
   * 
   * @param state
   *          The frozen state that had previously been returned by {@link #onSaveInstanceState}.
   * 
   * @see #onSaveInstanceState()
   * @see #restoreHierarchyState(android.util.SparseArray)
   * @see #dispatchRestoreInstanceState(android.util.SparseArray)
   */
  protected void onRestoreInstanceState(Parcelable state) {
    // mPrivateFlags |= SAVE_STATE_CALLED;
    // if (state != BaseSavedState.EMPTY_STATE && state != null) {
    // throw new
    // IllegalArgumentException("Wrong state class, expecting View State but " +
    // "received "
    // + state.getClass().toString() + " instead. This usually happens "
    // +
    // "when two views of different type have the same id in the same hierarchy. "
    // + "This view's id is " + ViewDebug.resolveId(mContext, getId()) +
    // ". Make sure "
    // + "other views do not use the same id.");
    // }
  }

  /**
   * Register a callback to be invoked when a key is pressed in this view.
   * 
   * @param l
   *          the key listener to attach to this view
   */
  public void setOnKeyListener(OnKeyListener l) {
    getListenerInfo().mOnKeyListener = l;
  }

  /**
   * Register a callback to be invoked when a touch event is sent to this view.
   * 
   * @param l
   *          the touch listener to attach to this view
   */
  public void setOnTouchListener(OnTouchListener l) {
    getListenerInfo().mOnTouchListener = l;
  }

  /**
   * Register a callback to be invoked when this view is clicked. If this view is not clickable, it becomes
   * clickable.
   * 
   * @param l
   *          The callback that will run
   * 
   * @see #setClickable(boolean)
   */
  public void setOnClickListener(OnClickListener l) {
    getListenerInfo().mOnClickListener = l;
  }

  public OnClickListener getOnClickListener() {
    ListenerInfo li = getListenerInfo();

    return mListenerInfo.mOnClickListener;
  }

  /**
   * Return whether this view has an attached OnClickListener. Returns true if there is a listener, false if
   * there is none.
   */
  public boolean hasOnClickListeners() {
    ListenerInfo li = getListenerInfo();
    return (li != null && li.mOnClickListener != null);
  }

  /**
   * Register a callback to be invoked when this view is clicked and held. If this view is not long clickable,
   * it becomes long clickable.
   * 
   * @param l
   *          The callback that will run
   * 
   * @see #setLongClickable(boolean)
   */
  public void setOnLongClickListener(OnLongClickListener l) {
    if (!isLongClickable()) {
      setLongClickable(true);
    }
    getListenerInfo().mOnLongClickListener = l;
  }

  /**
   * Call this view's OnClickListener, if it is defined. Performs all normal actions associated with clicking:
   * reporting accessibility event, playing a sound, etc.
   * 
   * @return True there was an assigned OnClickListener that was called, false otherwise is returned.
   */
  public void onClick() {
    OnClickListener li = getOnClickListener();
    if (li != null) {
      li.onClick(this);
    }

  }

  /**
   * Directly call any attached OnClickListener. Unlike {@link #performClick()}, this only calls the listener,
   * and does not do any associated clicking actions like reporting an accessibility event.
   * 
   * @return True there was an assigned OnClickListener that was called, false otherwise is returned.
   */
  public boolean callOnClick() {
    ListenerInfo li = getListenerInfo();
    if (li != null && li.mOnClickListener != null) {
      li.mOnClickListener.onClick(this);
      return true;
    } else if (li != null && li.mOnClickListener == null) {
      System.out.println("No listener attached");
      return true;
    }

    return false;
  }

  /**
   * Pass the touch screen motion event down to the target view, or this view if it is the target.
   * 
   * @param event
   *          The motion event to be dispatched.
   * @return True if the event was handled by the view, false otherwise.
   */
  public boolean dispatchTouchEvent(MotionEvent event) {
    ListenerInfo li = mListenerInfo;
    if (li != null && li.mOnTouchListener != null && isEnabled() && li.mOnTouchListener.onTouch(this, event)) {
      return true;
    }

    if (onTouchEvent(event)) {
      return true;
    }

    return false;
  }

  /**
   * Implement this method to handle touch screen motion events.
   * 
   * @param event
   *          The motion event.
   * @return True if the event was handled, false otherwise.
   */
  public boolean onTouchEvent(MotionEvent event) {
    onClick();
    return true;
  }

  public boolean performClick() {
    ListenerInfo li = mListenerInfo;
    if (li != null && li.mOnClickListener != null) {
      li.mOnClickListener.onClick(this);
      return true;
    }

    return false;
  }

  /**
   * Call this view's OnLongClickListener, if it is defined. Invokes the context menu if the
   * OnLongClickListener did not consume the event.
   * 
   * @return True if one of the above receivers consumed the event, false otherwise.
   */
  public boolean performLongClick() {
    boolean handled = false;
    ListenerInfo li = mListenerInfo;
    if (li != null && li.mOnLongClickListener != null) {
      handled = li.mOnLongClickListener.onLongClick(View.this);
    }
    return handled;
  }

  public int getVisibility() {
    return 0;// TODO mViewFlags & VISIBILITY_MASK;
  }

  /**
   * Set the enabled state of this view.
   * 
   * @param visibility
   *          One of {@link #VISIBLE}, {@link #INVISIBLE}, or {@link #GONE}.
   * @attr ref android.R.styleable#View_visibility
   */
  @RemotableViewMethod
  public void setVisibility(int visibility) {
    // TODO setFlags(visibility, VISIBILITY_MASK);
  }

  /**
   * Interface definition for a callback to be invoked when a view is clicked.
   */
  public interface OnClickListener {
    /**
     * Called when a view has been clicked.
     * 
     * @param v
     *          The view that was clicked.
     */
    void onClick(View v);
  }

  /**
   * Interface definition for a callback to be invoked when a key event is dispatched to this view. The
   * callback will be invoked before the key event is given to the view.
   */
  public interface OnKeyListener {
    /**
     * Called when a key is dispatched to a view. This allows listeners to get a chance to respond before the
     * target view.
     * 
     * @param v
     *          The view the key has been dispatched to.
     * @param keyCode
     *          The code for the physical key that was pressed
     * @param event
     *          The KeyEvent object containing full information about the event.
     * @return True if the listener has consumed the event, false otherwise.
     */
    boolean onKey(View v, int keyCode, KeyEvent event);
  }

  /**
   * Interface definition for a callback to be invoked when a touch event is dispatched to this view. The
   * callback will be invoked before the touch event is given to the view.
   */
  public interface OnTouchListener {
    /**
     * Called when a touch event is dispatched to a view. This allows listeners to get a chance to respond
     * before the target view.
     * 
     * @param v
     *          The view the touch event has been dispatched to.
     * @param event
     *          The MotionEvent object containing full information about the event.
     * @return True if the listener has consumed the event, false otherwise.
     */
    boolean onTouch(View v, MotionEvent event);
  }

  /**
   * Interface definition for a callback to be invoked when a hover event is dispatched to this view. The
   * callback will be invoked before the hover event is given to the view.
   */
  public interface OnHoverListener {
    /**
     * Called when a hover event is dispatched to a view. This allows listeners to get a chance to respond
     * before the target view.
     * 
     * @param v
     *          The view the hover event has been dispatched to.
     * @param event
     *          The MotionEvent object containing full information about the event.
     * @return True if the listener has consumed the event, false otherwise.
     */
    boolean onHover(View v, MotionEvent event);
  }

  /**
   * Interface definition for a callback to be invoked when a generic motion event is dispatched to this view.
   * The callback will be invoked before the generic motion event is given to the view.
   */
  public interface OnGenericMotionListener {
    /**
     * Called when a generic motion event is dispatched to a view. This allows listeners to get a chance to
     * respond before the target view.
     * 
     * @param v
     *          The view the generic motion event has been dispatched to.
     * @param event
     *          The MotionEvent object containing full information about the event.
     * @return True if the listener has consumed the event, false otherwise.
     */
    boolean onGenericMotion(View v, MotionEvent event);
  }

  /**
   * Interface definition for a callback to be invoked when a view has been clicked and held.
   */
  public interface OnLongClickListener {
    /**
     * Called when a view has been clicked and held.
     * 
     * @param v
     *          The view that was clicked and held.
     * 
     * @return true if the callback consumed the long click, false otherwise.
     */
    boolean onLongClick(View v);
  }

  /**
   * Interface definition for a callback to be invoked when a drag is being dispatched to this view. The
   * callback will be invoked before the hosting view's own onDrag(event) method. If the listener wants to
   * fall back to the hosting view's onDrag(event) behavior, it should return 'false' from this callback.
   * 
   * <div class="special reference"> <h3>Developer Guides</h3>
   * <p>
   * For a guide to implementing drag and drop features, read the <a href="{@docRoot}
   * guide/topics/ui/drag-drop.html">Drag and Drop</a> developer guide.
   * </p>
   * </div>
   */
  public interface OnDragListener {
    /**
     * Called when a drag event is dispatched to a view. This allows listeners to get a chance to override
     * base View behavior.
     * 
     * @param v
     *          The View that received the drag event.
     * @param event
     *          The {@link android.view.DragEvent} object for the drag event.
     * @return {@code true} if the drag event was handled successfully, or {@code false} if the drag event was
     *         not handled. Note that {@code false} will trigger the View to call its
     *         {@link #onDragEvent(DragEvent) onDragEvent()} handler.
     */
    boolean onDrag(View v, DragEvent event);
  }

  /**
   * Interface definition for a callback to be invoked when the focus state of a view changed.
   */
  public interface OnFocusChangeListener {
    /**
     * Called when the focus state of a view has changed.
     * 
     * @param v
     *          The view whose state has changed.
     * @param hasFocus
     *          The new focus state of v.
     */
    void onFocusChange(View v, boolean hasFocus);
  }

  /**
   * Interface definition for a callback to be invoked when the status bar changes visibility. This reports
   * <strong>global</strong> changes to the system UI state, not just what the application is requesting.
   * 
   * @see View#setOnSystemUiVisibilityChangeListener(android.view.View.OnSystemUiVisibilityChangeListener)
   */
  public interface OnSystemUiVisibilityChangeListener {
    /**
     * Called when the status bar changes visibility because of a call to
     * {@link View#setSystemUiVisibility(int)}.
     * 
     * @param visibility
     *          Bitwise-or of flags {@link #SYSTEM_UI_FLAG_LOW_PROFILE} or
     *          {@link #SYSTEM_UI_FLAG_HIDE_NAVIGATION}. This tells you the <strong>global</strong> state of
     *          the UI visibility flags, not what your app is currently applying.
     */
    public void onSystemUiVisibilityChange(int visibility);
  }

  /**
   * Interface definition for a callback to be invoked when this view is attached or detached from its window.
   */
  public interface OnAttachStateChangeListener {
    /**
     * Called when the view is attached to a window.
     * 
     * @param v
     *          The view that was attached
     */
    public void onViewAttachedToWindow(View v);

    /**
     * Called when the view is detached from a window.
     * 
     * @param v
     *          The view that was detached
     */
    public void onViewDetachedFromWindow(View v);
  }

  public void setName(String name) {
    this.name = name;
  }

  /**
   * Interface definition for a callback to be invoked when the context menu for this view is being built.
   */
  public interface OnCreateContextMenuListener {
    /**
     * Called when the context menu for this view is being built. It is not safe to hold onto the menu after
     * this method returns.
     *
     * @param menu
     *          The context menu that is being built
     * @param v
     *          The view for which the context menu is being built
     * @param menuInfo
     *          Extra information about the item for which the context menu should be shown. This information
     *          will vary depending on the class of v.
     */
    void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo);
  }

  /**
   * Register a callback to be invoked when the context menu for this view is being built. If this view is not
   * long clickable, it becomes long clickable.
   *
   * @param l
   *          The callback that will run
   *
   */
  public void setOnCreateContextMenuListener(OnCreateContextMenuListener l) {
    // if (!isLongClickable()) {
    // setLongClickable(true);
    // }
    getListenerInfo().mOnCreateContextMenuListener = l;
    performCreateContextMenu();

  }

  @Override
  public String toString() {
    return "View [ type=" + getClass().getSimpleName() + " mID=" + mID + ", name=" + name + "]";
  }

  public View findViewByName(String name) {
    if (name == null || name.length() < 1) {
      return null;
    }
    return findViewTraversal(name);
  }

  /**
   * {@hide}
   * 
   * @param id
   *          the id of the view to be found
   * @return the view of the specified id, null if cannot be found
   */
  protected View findViewTraversal(String name) {
    if (this.name != null) {
      System.out.println(this.name);

      if (this.name.equals(name)) {
        return this;
      }
    }
    return null;
  }

  public void setNativeHashCode(int hashcode) {
    this.nativeHash = hashcode;
  }

  public int getNativeHash() {
    return this.nativeHash;
  }

  /**
   * Returns the resources associated with this view.
   *
   * @return Resources object.
   */
  public Resources getResources() {
    return mResources;
  }

  protected View findViewByText(String text) {
    return null;
  }

  public Object getKeyDispatcherState() {
    // TODO Auto-generated method stub
    return null;
  }

  public void setOnCreateContextMenuListener(Dialog dialog) {

  }

  public void showContextMenu() {
  }

  public List<Event> collectEvents() {

    List<Event> events = new LinkedList<Event>();
    if (this.enabled && this.visible) {
      if (this instanceof ViewGroup) {

        View[] children = ((ViewGroup) this).getChildren();

        for (View child : children) {
          if (child != null) {
            events.addAll(child.collectEvents());
          }
        }
      }

      if (getListenerInfo().mOnClickListener != null) {
        UIEvent uiEvent = new UIEvent("$" + this.getName(), "onClick");
        events.add(uiEvent);
      }
      if (getListenerInfo().mOnLongClickListener != null) {
        UIEvent uiEvent = new UIEvent("$" + this.getName(), "onLongClick");
        events.add(uiEvent);
      }
      if (getListenerInfo().mOnFocusChangeListener != null) {
        UIEvent uiEvent = new UIEvent("$" + this.getName(), "onFocusChange");
        events.add(uiEvent);
      }

      if (getListenerInfo().mOnKeyListener != null) {
        UIEvent uiEvent = new UIEvent("$" + this.getName(), "onKey");
        events.add(uiEvent);
      }

      if (getListenerInfo().mOnTouchListener != null) {
        UIEvent uiEvent = new UIEvent("$" + this.getName(), "onTouch");
        events.add(uiEvent);
      }

      if (menu != null) {
        // menu was already created so lets return items
        Collection<MenuItem> c = menu.getItems();
        int i = 0;
        for (Iterator<MenuItem> iterator = c.iterator(); iterator.hasNext();) {
          MenuItem menuItem = iterator.next();

          UIEvent uiEvent = new UIEvent("$" + this.getName(), "onContextMenuItemSelected", new Object[] { i,
              menuItem.getTitle() });
          events.add(uiEvent);
          i++;
        }
      }
    }
    return events;
  }

  public boolean processEvent(Event event) {
    if (event instanceof UIEvent) {
      String action = ((UIEvent) event).getAction();
      if (action.equals("onClick")) {
        return callOnClick();
      } else if (action.equals("onLongClick")) {
        return performLongClick();
      } else if (action.equals("onContextMenuItemSelected")) {
        performOnContextMenuItemSelected((int) ((UIEvent) event).getArguments()[0]);
      } else if (action.equals("onFocusChange")) {
        getListenerInfo().mOnFocusChangeListener.onFocusChange(this, !hasFocus());
      } else if (action.equals("onKey")) {
        getListenerInfo().mOnKeyListener.onKey(this, KeyEvent.KEYCODE_ENTER, new KeyEvent(
            KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
      } else if (action.equals("onTouch")) {
        getListenerInfo().mOnTouchListener.onTouch(this,
            MotionEvent.obtain(2, 3, MotionEvent.ACTION_MOVE, 100, 100, MotionEvent.TOOL_TYPE_FINGER));
      }
      return true;
    }
    return false;
  }

  public void performCreateContextMenu() {
    menu = new ContextMenuImpl(getContext());
    AdapterContextMenuInfo menuinfo = new AdapterContextMenuInfo(this, 0, 0);
    ((MenuImpl) menu).setInfo(menuinfo);

    ListenerInfo li = mListenerInfo;
    if (li != null && li.mOnCreateContextMenuListener != null) {
      li.mOnCreateContextMenuListener.onCreateContextMenu(menu, this, menuinfo);
    }

  }

  public void performOnContextMenuItemSelected(int item) {
    MenuItem[] it = menu.getItems().toArray(new MenuItem[menu.size()]);
    it[item].setMenuInfo(((MenuImpl) menu).getInfo());
    ((Activity) getContext()).onContextItemSelected(it[item]);
  }

  public ViewGroup.LayoutParams getLayoutParams() {
    return LayoutParams.TOP;
  }

  public void setLayoutParams(ViewGroup.LayoutParams param0) {
  }

  public void setScrollBarStyle(int param0) {
  }

  public void setFocusable(boolean param0) {
  }

  public void setFocusableInTouchMode(boolean param0) {
  }

  public static View inflate(Context context, int resource, ViewGroup root) {
    LayoutInflater factory = LayoutInflater.from(context);
    return factory.inflate(resource, root);
  }

  /** Stubs **/

  public void invalidate(android.graphics.Rect param0) {
  }

  public void invalidate(int param0, int param1, int param2, int param3) {
  }

  public void invalidate() {
  }

  // public java.lang.String toString(){
  // return Abstraction.TOP_STRING;
  // }

  public int getVerticalFadingEdgeLength() {
    return Abstraction.TOP_INT;
  }

  public void setFadingEdgeLength(int param0) {
  }

  public int getHorizontalFadingEdgeLength() {
    return Abstraction.TOP_INT;
  }

  public int getVerticalScrollbarWidth() {
    return Abstraction.TOP_INT;
  }

  protected int getHorizontalScrollbarHeight() {
    return Abstraction.TOP_INT;
  }

  public void setVerticalScrollbarPosition(int param0) {
  }

  public int getVerticalScrollbarPosition() {
    return Abstraction.TOP_INT;
  }

  public void setOnFocusChangeListener(android.view.View.OnFocusChangeListener param0) {
    getListenerInfo().mOnFocusChangeListener = param0;
  }

  // public void addOnLayoutChangeListener(android.view.View.OnLayoutChangeListener param0){
  // }
  //
  // public void removeOnLayoutChangeListener(android.view.View.OnLayoutChangeListener param0){
  // }

  public void addOnAttachStateChangeListener(android.view.View.OnAttachStateChangeListener param0) {
  }

  public void removeOnAttachStateChangeListener(android.view.View.OnAttachStateChangeListener param0) {
  }

  public android.view.View.OnFocusChangeListener getOnFocusChangeListener() {
    return getListenerInfo().mOnFocusChangeListener;
  }

  // public void setOnClickListener(android.view.View.OnClickListener param0){
  // }
  // public boolean hasOnClickListeners(){
  // return Abstraction.TOP_BOOL;
  // }
  // //
  // public void setOnLongClickListener(android.view.View.OnLongClickListener param0){
  // }
  //
  // public void setOnCreateContextMenuListener(android.view.View.OnCreateContextMenuListener param0){
  // }
  //
  // public boolean performClick(){
  // return Abstraction.TOP_BOOL;
  // }
  //
  // public boolean callOnClick(){
  // return Abstraction.TOP_BOOL;
  // }
  //
  // public boolean performLongClick(){
  // return Abstraction.TOP_BOOL;
  // }
  //
  // public boolean showContextMenu(){
  // return Abstraction.TOP_BOOL;
  // }

  // public android.view.ActionMode startActionMode(android.view.ActionMode.Callback param0){
  // return android.view.ActionMode.TOP;
  // }

  // public void setOnKeyListener(android.view.View.OnKeyListener param0){
  // }
  //
  // public void setOnTouchListener(android.view.View.OnTouchListener param0){
  // }

  public void setOnGenericMotionListener(android.view.View.OnGenericMotionListener param0) {
  }

  public void setOnHoverListener(android.view.View.OnHoverListener param0) {
  }

  public void setOnDragListener(android.view.View.OnDragListener param0) {
  }

  public boolean requestRectangleOnScreen(android.graphics.Rect param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean requestRectangleOnScreen(android.graphics.Rect param0, boolean param1) {
    return Abstraction.TOP_BOOL;
  }

  public void clearFocus() {
    focussed = false;
  }

  public boolean hasFocus() {
    return focussed;
  }

  public boolean hasFocusable() {
    return Abstraction.TOP_BOOL;
  }

  protected void onFocusChanged(boolean param0, int param1, android.graphics.Rect param2) {
  }

  public void sendAccessibilityEvent(int param0) {
  }

  public void announceForAccessibility(java.lang.CharSequence param0) {
  }

  public void sendAccessibilityEventUnchecked(android.view.accessibility.AccessibilityEvent param0) {
  }

  public boolean dispatchPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public void onPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  // public android.view.accessibility.AccessibilityNodeInfo createAccessibilityNodeInfo(){
  // return android.view.accessibility.AccessibilityNodeInfo.IDENTITY_MATRIX;
  // }

  public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo param0) {
  }

  // public void setAccessibilityDelegate(android.view.View.AccessibilityDelegate param0){
  // }
  //
  // public android.view.accessibility.AccessibilityNodeProvider getAccessibilityNodeProvider(){
  // return android.view.accessibility.AccessibilityNodeProvider.TOP;
  // }

  public java.lang.CharSequence getContentDescription() {
    return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  }

  public void setContentDescription(java.lang.CharSequence param0) {
  }

  public int getLabelFor() {
    return Abstraction.TOP_INT;
  }

  public void setLabelFor(int param0) {
  }

  public boolean isFocused() {
    return Abstraction.TOP_BOOL;
  }

  public android.view.View findFocus() {
    return this;
  }

  public boolean isScrollContainer() {
    return Abstraction.TOP_BOOL;
  }

  public void setScrollContainer(boolean param0) {
  }

  public int getDrawingCacheQuality() {
    return Abstraction.TOP_INT;
  }

  public void setDrawingCacheQuality(int param0) {
  }

  public boolean getKeepScreenOn() {
    return Abstraction.TOP_BOOL;
  }

  public void setKeepScreenOn(boolean param0) {
  }

  public int getNextFocusLeftId() {
    return Abstraction.TOP_INT;
  }

  public void setNextFocusLeftId(int param0) {
  }

  public int getNextFocusRightId() {
    return Abstraction.TOP_INT;
  }

  public void setNextFocusRightId(int param0) {
  }

  public int getNextFocusUpId() {
    return Abstraction.TOP_INT;
  }

  public void setNextFocusUpId(int param0) {
  }

  public int getNextFocusDownId() {
    return Abstraction.TOP_INT;
  }

  public void setNextFocusDownId(int param0) {
  }

  public int getNextFocusForwardId() {
    return Abstraction.TOP_INT;
  }

  public void setNextFocusForwardId(int param0) {
  }

  public boolean isShown() {
    return Abstraction.TOP_BOOL;
  }

  protected boolean fitSystemWindows(android.graphics.Rect param0) {
    return Abstraction.TOP_BOOL;
  }

  // public android.view.WindowInsets onApplyWindowInsets(android.view.WindowInsets param0){
  // return android.view.WindowInsets.TOP;
  // }
  //
  // public void setOnApplyWindowInsetsListener(android.view.View.OnApplyWindowInsetsListener param0){
  // }

  // public android.view.WindowInsets dispatchApplyWindowInsets(android.view.WindowInsets param0){
  // return android.view.WindowInsets.TOP;
  // }
  //
  // public android.view.WindowInsets computeSystemWindowInsets(android.view.WindowInsets param0,
  // android.graphics.Rect param1){
  // return android.view.WindowInsets.TOP;
  // }

  public void setFitsSystemWindows(boolean param0) {
  }

  public boolean getFitsSystemWindows() {
    return Abstraction.TOP_BOOL;
  }

  public void requestFitSystemWindows() {
  }

  public void requestApplyInsets() {
  }

  // public int getVisibility(){
  // return Abstraction.TOP_INT;
  // }
  //
  // public void setVisibility(int param0){
  // }
  //
  // public boolean isEnabled(){
  // return Abstraction.TOP_BOOL;
  // }
  //
  // public void setEnabled(boolean param0){
  // }
  //
  // public void setFocusable(boolean param0){
  // }
  //
  // public void setFocusableInTouchMode(boolean param0){
  // }

  public void setSoundEffectsEnabled(boolean param0) {
  }

  public boolean isSoundEffectsEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setHapticFeedbackEnabled(boolean param0) {
  }

  public boolean isHapticFeedbackEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setLayoutDirection(int param0) {
  }

  public int getLayoutDirection() {
    return Abstraction.TOP_INT;
  }

  public boolean hasTransientState() {
    return Abstraction.TOP_BOOL;
  }

  public void setHasTransientState(boolean param0) {
  }

  public boolean isAttachedToWindow() {
    return Abstraction.TOP_BOOL;
  }

  public boolean isLaidOut() {
    return Abstraction.TOP_BOOL;
  }

  public void setWillNotDraw(boolean param0) {
  }

  public boolean willNotDraw() {
    return Abstraction.TOP_BOOL;
  }

  public void setWillNotCacheDrawing(boolean param0) {
  }

  public boolean willNotCacheDrawing() {
    return Abstraction.TOP_BOOL;
  }

  public boolean isClickable() {
    return Abstraction.TOP_BOOL;
  }

  public void setClickable(boolean param0) {
  }

  @FilterField
  boolean longClickable = false;

  public boolean isLongClickable() {
    return longClickable;
  }

  public void setLongClickable(boolean param0) {
    longClickable = param0;
  }

  public void setPressed(boolean param0) {
  }

  protected void dispatchSetPressed(boolean param0) {
  }

  public boolean isPressed() {
    return Abstraction.TOP_BOOL;
  }

  public boolean isSaveEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setSaveEnabled(boolean param0) {
  }

  public boolean getFilterTouchesWhenObscured() {
    return Abstraction.TOP_BOOL;
  }

  public void setFilterTouchesWhenObscured(boolean param0) {
  }

  public boolean isSaveFromParentEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setSaveFromParentEnabled(boolean param0) {
  }

  public final boolean isFocusable() {
    return Abstraction.TOP_BOOL;
  }

  public final boolean isFocusableInTouchMode() {
    return Abstraction.TOP_BOOL;
  }

  // public android.view.View focusSearch(int param0){
  // return android.view.View.IDENTITY_MATRIX;
  // }

  public boolean dispatchUnhandledMove(android.view.View param0, int param1) {
    return Abstraction.TOP_BOOL;
  }

  public java.util.ArrayList getFocusables(int param0) {
    return ((java.util.ArrayList) Abstraction.randomObject("java.util.ArrayList"));
  }

  public void addFocusables(java.util.ArrayList param0, int param1) {
  }

  public void addFocusables(java.util.ArrayList param0, int param1, int param2) {
  }

  public void findViewsWithText(java.util.ArrayList param0, java.lang.CharSequence param1, int param2) {
  }

  public java.util.ArrayList getTouchables() {
    return ((java.util.ArrayList) Abstraction.randomObject("java.util.ArrayList"));
  }

  public void addTouchables(java.util.ArrayList param0) {
  }

  public boolean isAccessibilityFocused() {
    return Abstraction.TOP_BOOL;
  }

  public final boolean requestFocus() {
    return Abstraction.TOP_BOOL;
  }

  public final boolean requestFocus(int param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean requestFocus(int param0, android.graphics.Rect param1) {
    return Abstraction.TOP_BOOL;
  }

  public final boolean requestFocusFromTouch() {
    return Abstraction.TOP_BOOL;
  }

  public int getImportantForAccessibility() {
    return Abstraction.TOP_INT;
  }

  public void setAccessibilityLiveRegion(int param0) {
  }

  public int getAccessibilityLiveRegion() {
    return Abstraction.TOP_INT;
  }

  public void setImportantForAccessibility(int param0) {
  }

  public boolean isImportantForAccessibility() {
    return Abstraction.TOP_BOOL;
  }

  // public android.view.ViewParent getParentForAccessibility(){
  // return android.view.ViewGroup.IDENTITY_MATRIX;
  // }

  public void addChildrenForAccessibility(java.util.ArrayList param0) {
  }

  public boolean performAccessibilityAction(int param0, android.os.Bundle param1) {
    return Abstraction.TOP_BOOL;
  }

  public void onStartTemporaryDetach() {
  }

  public void onFinishTemporaryDetach() {
  }

  // public android.view.KeyEvent.DispatcherState getKeyDispatcherState(){
  // return
  // ((android.view.KeyEvent.DispatcherState)Abstraction.randomObject("android.view.KeyEvent.DispatcherState"));
  // }

  public boolean dispatchKeyEventPreIme(android.view.KeyEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean dispatchKeyEvent(android.view.KeyEvent event) {
    ListenerInfo li = mListenerInfo;
    if (li != null && li.mOnKeyListener != null && isEnabled()
        && li.mOnKeyListener.onKey(this, event.getKeyCode(), event)) {
      return true;
    } else {
      return onKeyDown(event.getKeyCode(), event);
    }
  }

  public boolean dispatchKeyShortcutEvent(android.view.KeyEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  // public boolean dispatchTouchEvent(android.view.MotionEvent param0){
  // return Abstraction.TOP_BOOL;
  // }

  public boolean onFilterTouchEventForSecurity(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean dispatchTrackballEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean dispatchGenericMotionEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  protected boolean dispatchHoverEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  protected boolean dispatchGenericPointerEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  protected boolean dispatchGenericFocusedEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public void dispatchWindowFocusChanged(boolean param0) {
  }

  public void onWindowFocusChanged(boolean param0) {
  }

  public boolean hasWindowFocus() {
    return Abstraction.TOP_BOOL;
  }

  protected void dispatchVisibilityChanged(android.view.View param0, int param1) {
  }

  protected void onVisibilityChanged(android.view.View param0, int param1) {
  }

  public void dispatchDisplayHint(int param0) {
  }

  protected void onDisplayHint(int param0) {
  }

  public void dispatchWindowVisibilityChanged(int param0) {
  }

  protected void onWindowVisibilityChanged(int param0) {
  }

  public int getWindowVisibility() {
    return Abstraction.TOP_INT;
  }

  public void getWindowVisibleDisplayFrame(android.graphics.Rect param0) {
  }

  public void dispatchConfigurationChanged(android.content.res.Configuration param0) {
  }

  protected void onConfigurationChanged(android.content.res.Configuration param0) {
  }

  public boolean isInTouchMode() {
    return Abstraction.TOP_BOOL;
  }

  // public final android.content.Context getContext(){
  // return android.content.Context.IDENTITY_MATRIX;
  // }

  public boolean onKeyPreIme(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  public boolean onKeyDown(int param0, android.view.KeyEvent param1) {
    return false;
  }

  public boolean onKeyLongPress(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  public boolean onKeyUp(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  public boolean onKeyMultiple(int param0, int param1, android.view.KeyEvent param2) {
    return Abstraction.TOP_BOOL;
  }

  public boolean onKeyShortcut(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  public boolean onCheckIsTextEditor() {
    return Abstraction.TOP_BOOL;
  }

  // public android.view.inputmethod.InputConnection
  // onCreateInputConnection(android.view.inputmethod.EditorInfo param0){
  // return android.view.inputmethod.InputConnectionWrapper.IDENTITY_MATRIX;
  // }

  public boolean checkInputConnectionProxy(android.view.View param0) {
    return Abstraction.TOP_BOOL;
  }

  public void createContextMenu(android.view.ContextMenu param0) {
  }

  protected android.view.ContextMenu.ContextMenuInfo getContextMenuInfo() {
    return ((android.view.ContextMenu.ContextMenuInfo) Abstraction
        .randomObject("android.view.ContextMenu.ContextMenuInfo"));
  }

  protected void onCreateContextMenu(android.view.ContextMenu param0) {
  }

  public boolean onTrackballEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean onGenericMotionEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean onHoverEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean isHovered() {
    return Abstraction.TOP_BOOL;
  }

  public void setHovered(boolean param0) {
  }

  public void onHoverChanged(boolean param0) {
  }

  // public boolean onTouchEvent(android.view.MotionEvent param0){
  // return Abstraction.TOP_BOOL;
  // }

  public void cancelLongPress() {
  }

  public void setTouchDelegate(android.view.TouchDelegate param0) {
  }

  // public android.view.TouchDelegate getTouchDelegate(){
  // return android.view.TouchDelegate.IDENTITY_MATRIX;
  // }

  public final void requestUnbufferedDispatch(android.view.MotionEvent param0) {
  }

  public void bringToFront() {
  }

  protected void onScrollChanged(int param0, int param1, int param2, int param3) {
  }

  protected void onSizeChanged(int param0, int param1, int param2, int param3) {
  }

  protected void dispatchDraw(android.graphics.Canvas param0) {
  }

  public final android.view.ViewParent getParent() {
    return this.mParent;
  }

  public void setScrollX(int param0) {
  }

  public void setScrollY(int param0) {
  }

  public final int getScrollX() {
    return Abstraction.TOP_INT;
  }

  public final int getScrollY() {
    return Abstraction.TOP_INT;
  }

  // public final int getWidth(){
  // return Abstraction.TOP_INT;
  // }
  //
  // public final int getHeight(){
  // return Abstraction.TOP_INT;
  // }

  public void getDrawingRect(android.graphics.Rect param0) {
  }

  public final int getMeasuredWidth() {
    return Abstraction.TOP_INT;
  }

  public final int getMeasuredWidthAndState() {
    return Abstraction.TOP_INT;
  }

  public final int getMeasuredHeight() {
    return Abstraction.TOP_INT;
  }

  public final int getMeasuredHeightAndState() {
    return Abstraction.TOP_INT;
  }

  public final int getMeasuredState() {
    return Abstraction.TOP_INT;
  }

  public android.graphics.Matrix getMatrix() {
    return android.graphics.Matrix.IDENTITY_MATRIX;
  }

  public float getCameraDistance() {
    return Abstraction.TOP_FLOAT;
  }

  public void setCameraDistance(float param0) {
  }

  public float getRotation() {
    return Abstraction.TOP_FLOAT;
  }

  public void setRotation(float param0) {
  }

  public float getRotationY() {
    return Abstraction.TOP_FLOAT;
  }

  public void setRotationY(float param0) {
  }

  public float getRotationX() {
    return Abstraction.TOP_FLOAT;
  }

  public void setRotationX(float param0) {
  }

  public float getScaleX() {
    return Abstraction.TOP_FLOAT;
  }

  public void setScaleX(float param0) {
  }

  public float getScaleY() {
    return Abstraction.TOP_FLOAT;
  }

  public void setScaleY(float param0) {
  }

  public float getPivotX() {
    return Abstraction.TOP_FLOAT;
  }

  public void setPivotX(float param0) {
  }

  public float getPivotY() {
    return Abstraction.TOP_FLOAT;
  }

  public void setPivotY(float param0) {
  }

  public float getAlpha() {
    return Abstraction.TOP_FLOAT;
  }

  public boolean hasOverlappingRendering() {
    return Abstraction.TOP_BOOL;
  }

  public void setAlpha(float param0) {
  }

  public final int getTop() {
    return Abstraction.TOP_INT;
  }

  public final void setTop(int param0) {
  }

  public final int getBottom() {
    return Abstraction.TOP_INT;
  }

  public boolean isDirty() {
    return Abstraction.TOP_BOOL;
  }

  public final void setBottom(int param0) {
  }

  public final int getLeft() {
    return Abstraction.TOP_INT;
  }

  public final void setLeft(int param0) {
  }

  public final int getRight() {
    return Abstraction.TOP_INT;
  }

  public final void setRight(int param0) {
  }

  public float getX() {
    return Abstraction.TOP_FLOAT;
  }

  public void setX(float param0) {
  }

  public float getY() {
    return Abstraction.TOP_FLOAT;
  }

  public void setY(float param0) {
  }

  public float getZ() {
    return Abstraction.TOP_FLOAT;
  }

  public void setZ(float param0) {
  }

  public float getElevation() {
    return Abstraction.TOP_FLOAT;
  }

  public void setElevation(float param0) {
  }

  public float getTranslationX() {
    return Abstraction.TOP_FLOAT;
  }

  public void setTranslationX(float param0) {
  }

  public float getTranslationY() {
    return Abstraction.TOP_FLOAT;
  }

  public void setTranslationY(float param0) {
  }

  public float getTranslationZ() {
    return Abstraction.TOP_FLOAT;
  }

  public void setTranslationZ(float param0) {
  }

  // public android.animation.StateListAnimator getStateListAnimator(){
  // return android.animation.StateListAnimator.TOP;
  // }
  //
  // public void setStateListAnimator(android.animation.StateListAnimator param0){
  // }

  public final boolean getClipToOutline() {
    return Abstraction.TOP_BOOL;
  }

  public void setClipToOutline(boolean param0) {
  }

  // public void setOutlineProvider(android.view.ViewOutlineProvider param0){
  // }
  //
  // public android.view.ViewOutlineProvider getOutlineProvider(){
  // return android.view.ViewOutlineProvider.TOP;
  // }

  public void invalidateOutline() {
  }

  public void getHitRect(android.graphics.Rect param0) {
  }

  public void getFocusedRect(android.graphics.Rect param0) {
  }

  public boolean getGlobalVisibleRect(android.graphics.Rect param0, android.graphics.Point param1) {
    return Abstraction.TOP_BOOL;
  }

  public final boolean getGlobalVisibleRect(android.graphics.Rect param0) {
    return Abstraction.TOP_BOOL;
  }

  public final boolean getLocalVisibleRect(android.graphics.Rect param0) {
    return Abstraction.TOP_BOOL;
  }

  public void offsetTopAndBottom(int param0) {
  }

  public void offsetLeftAndRight(int param0) {
  }

  // public android.view.ViewGroup.LayoutParams getLayoutParams(){
  // return
  // ((android.view.ViewGroup.LayoutParams)Abstraction.randomObject("android.view.ViewGroup.LayoutParams"));
  // }

  // public void setLayoutParams(android.view.ViewGroup.LayoutParams param0){
  // }

  public void scrollTo(int param0, int param1) {
  }

  public void scrollBy(int param0, int param1) {
  }

  protected boolean awakenScrollBars() {
    return Abstraction.TOP_BOOL;
  }

  protected boolean awakenScrollBars(int param0) {
    return Abstraction.TOP_BOOL;
  }

  protected boolean awakenScrollBars(int param0, boolean param1) {
    return Abstraction.TOP_BOOL;
  }

  // public void invalidate(android.graphics.Rect param0){
  // }
  //
  // public void invalidate(int param0, int param1, int param2, int param3){
  // }
  //
  // public void invalidate(){
  // }

  public boolean isOpaque() {
    return Abstraction.TOP_BOOL;
  }

  // public android.os.Handler getHandler(){
  // return android.os.Handler.TOP;
  // }

  public boolean post(java.lang.Runnable param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean postDelayed(java.lang.Runnable param0, long param1) {
    return Abstraction.TOP_BOOL;
  }

  public void postOnAnimation(java.lang.Runnable param0) {
  }

  public void postOnAnimationDelayed(java.lang.Runnable param0, long param1) {
  }

  public boolean removeCallbacks(java.lang.Runnable param0) {
    return Abstraction.TOP_BOOL;
  }

  public void postInvalidate() {
  }

  public void postInvalidate(int param0, int param1, int param2, int param3) {
  }

  public void postInvalidateDelayed(long param0) {
  }

  public void postInvalidateDelayed(long param0, int param1, int param2, int param3, int param4) {
  }

  public void postInvalidateOnAnimation() {
  }

  public void postInvalidateOnAnimation(int param0, int param1, int param2, int param3) {
  }

  public void computeScroll() {
  }

  public boolean isHorizontalFadingEdgeEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setHorizontalFadingEdgeEnabled(boolean param0) {
  }

  public boolean isVerticalFadingEdgeEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setVerticalFadingEdgeEnabled(boolean param0) {
  }

  protected float getTopFadingEdgeStrength() {
    return Abstraction.TOP_FLOAT;
  }

  protected float getBottomFadingEdgeStrength() {
    return Abstraction.TOP_FLOAT;
  }

  protected float getLeftFadingEdgeStrength() {
    return Abstraction.TOP_FLOAT;
  }

  protected float getRightFadingEdgeStrength() {
    return Abstraction.TOP_FLOAT;
  }

  public boolean isHorizontalScrollBarEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setHorizontalScrollBarEnabled(boolean param0) {
  }

  public boolean isVerticalScrollBarEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setVerticalScrollBarEnabled(boolean param0) {
  }

  public void setScrollbarFadingEnabled(boolean param0) {
  }

  public boolean isScrollbarFadingEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public int getScrollBarDefaultDelayBeforeFade() {
    return Abstraction.TOP_INT;
  }

  public void setScrollBarDefaultDelayBeforeFade(int param0) {
  }

  public int getScrollBarFadeDuration() {
    return Abstraction.TOP_INT;
  }

  public void setScrollBarFadeDuration(int param0) {
  }

  public int getScrollBarSize() {
    return Abstraction.TOP_INT;
  }

  public void setScrollBarSize(int param0) {
  }

  // public void setScrollBarStyle(int param0){
  // }

  public int getScrollBarStyle() {
    return Abstraction.TOP_INT;
  }

  protected int computeHorizontalScrollRange() {
    return Abstraction.TOP_INT;
  }

  protected int computeHorizontalScrollOffset() {
    return Abstraction.TOP_INT;
  }

  protected int computeHorizontalScrollExtent() {
    return Abstraction.TOP_INT;
  }

  protected int computeVerticalScrollRange() {
    return Abstraction.TOP_INT;
  }

  protected int computeVerticalScrollOffset() {
    return Abstraction.TOP_INT;
  }

  protected int computeVerticalScrollExtent() {
    return Abstraction.TOP_INT;
  }

  public boolean canScrollHorizontally(int param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean canScrollVertically(int param0) {
    return Abstraction.TOP_BOOL;
  }

  protected final void onDrawScrollBars(android.graphics.Canvas param0) {
  }

  // protected void onDraw(android.graphics.Canvas param0){
  // }

  protected void onAttachedToWindow() {
  }

  public void onScreenStateChanged(int param0) {
  }

  public void onRtlPropertiesChanged(int param0) {
  }

  public boolean canResolveLayoutDirection() {
    return Abstraction.TOP_BOOL;
  }

  public boolean isLayoutDirectionResolved() {
    return Abstraction.TOP_BOOL;
  }

  protected void onDetachedFromWindow() {
  }

  protected int getWindowAttachCount() {
    return Abstraction.TOP_INT;
  }

  // public android.os.IBinder getWindowToken(){
  // return android.os.Binder.TOP;
  // }

  // public android.view.WindowId getWindowId(){
  // return android.view.WindowId.TOP;
  // }
  //
  // public android.os.IBinder getApplicationWindowToken(){
  // return android.os.Binder.TOP;
  // }

  public android.view.Display getDisplay() {
    return android.view.Display.TOP;
  }

  public final void cancelPendingInputEvents() {
  }

  public void onCancelPendingInputEvents() {
  }

  // public void saveHierarchyState(android.util.SparseArray param0){
  // }
  //
  // protected void dispatchSaveInstanceState(android.util.SparseArray param0){
  // }
  //
  // protected android.os.Parcelable onSaveInstanceState(){
  // return android.bluetooth.le.AdvertiseData.TOP;
  // }

  // public void restoreHierarchyState(android.util.SparseArray param0){
  // }
  //
  // protected void dispatchRestoreInstanceState(android.util.SparseArray param0){
  // }
  //
  // protected void onRestoreInstanceState(android.os.Parcelable param0){
  // }

  public long getDrawingTime() {
    return Abstraction.TOP_LONG;
  }

  public void setDuplicateParentStateEnabled(boolean param0) {
  }

  public boolean isDuplicateParentStateEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setLayerType(int param0, android.graphics.Paint param1) {
  }

  public void setLayerPaint(android.graphics.Paint param0) {
  }

  public int getLayerType() {
    return Abstraction.TOP_INT;
  }

  public void buildLayer() {
  }

  public void setDrawingCacheEnabled(boolean param0) {
  }

  public boolean isDrawingCacheEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public android.graphics.Bitmap getDrawingCache() {
    return android.graphics.Bitmap.TOP;
  }

  public android.graphics.Bitmap getDrawingCache(boolean param0) {
    return android.graphics.Bitmap.TOP;
  }

  public void destroyDrawingCache() {
  }

  public void setDrawingCacheBackgroundColor(int param0) {
  }

  public int getDrawingCacheBackgroundColor() {
    return Abstraction.TOP_INT;
  }

  public void buildDrawingCache() {
  }

  public void buildDrawingCache(boolean param0) {
  }

  public boolean isInEditMode() {
    return Abstraction.TOP_BOOL;
  }

  protected boolean isPaddingOffsetRequired() {
    return Abstraction.TOP_BOOL;
  }

  protected int getLeftPaddingOffset() {
    return Abstraction.TOP_INT;
  }

  protected int getRightPaddingOffset() {
    return Abstraction.TOP_INT;
  }

  protected int getTopPaddingOffset() {
    return Abstraction.TOP_INT;
  }

  protected int getBottomPaddingOffset() {
    return Abstraction.TOP_INT;
  }

  public boolean isHardwareAccelerated() {
    return Abstraction.TOP_BOOL;
  }

  public void setClipBounds(android.graphics.Rect param0) {
  }

  public android.graphics.Rect getClipBounds() {
    return android.graphics.Rect.TOP;
  }

  // public void draw(android.graphics.Canvas param0){
  // }

  // public android.view.ViewOverlay getOverlay(){
  // return android.view.ViewOverlay.TOP;
  // }

  public int getSolidColor() {
    return Abstraction.TOP_INT;
  }

  public boolean isLayoutRequested() {
    return Abstraction.TOP_BOOL;
  }

  public void layout(int param0, int param1, int param2, int param3) {
  }

  protected void onLayout(boolean param0, int param1, int param2, int param3, int param4) {
  }

  protected void onFinishInflate() {
  }

  // public android.content.res.Resources getResources(){
  // return ((android.content.res.Resources)Abstraction.randomObject("android.content.res.Resources"));
  // }

  public void invalidateDrawable(android.graphics.drawable.Drawable param0) {
  }

  public void scheduleDrawable(android.graphics.drawable.Drawable param0, java.lang.Runnable param1,
                               long param2) {
  }

  public void unscheduleDrawable(android.graphics.drawable.Drawable param0, java.lang.Runnable param1) {
  }

  public void unscheduleDrawable(android.graphics.drawable.Drawable param0) {
  }

  protected boolean verifyDrawable(android.graphics.drawable.Drawable param0) {
    return Abstraction.TOP_BOOL;
  }

  protected void drawableStateChanged() {
  }

  public void drawableHotspotChanged(float param0, float param1) {
  }

  public void refreshDrawableState() {
  }

  public final int[] getDrawableState() {
    return ((int[]) Abstraction.randomObject("int[]"));
  }

  protected int[] onCreateDrawableState(int param0) {
    return ((int[]) Abstraction.randomObject("int[]"));
  }

  protected static int[] mergeDrawableStates(int[] param0, int[] param1) {
    return ((int[]) Abstraction.randomObject("int[]"));
  }

  public void jumpDrawablesToCurrentState() {
  }

  public void setBackgroundColor(int param0) {
  }

  public void setBackgroundResource(int param0) {
  }

  public void setBackground(android.graphics.drawable.Drawable param0) {
  }

  public void setBackgroundDrawable(android.graphics.drawable.Drawable param0) {
  }

  public android.graphics.drawable.Drawable getBackground() {
    return android.graphics.drawable.Drawable.TOP;
  }

  public void setBackgroundTintList(android.content.res.ColorStateList param0) {
  }

  public android.content.res.ColorStateList getBackgroundTintList() {
    return ((android.content.res.ColorStateList) Abstraction
        .randomObject("android.content.res.ColorStateList"));
  }

  public void setBackgroundTintMode(android.graphics.PorterDuff.Mode param0) {
  }

  public android.graphics.PorterDuff.Mode getBackgroundTintMode() {
    return ((android.graphics.PorterDuff.Mode) Abstraction.randomObject("android.graphics.PorterDuff.Mode"));
  }

  public void setPadding(int param0, int param1, int param2, int param3) {
  }

  public void setPaddingRelative(int param0, int param1, int param2, int param3) {
  }

  public int getPaddingTop() {
    return Abstraction.TOP_INT;
  }

  public int getPaddingBottom() {
    return Abstraction.TOP_INT;
  }

  public int getPaddingLeft() {
    return Abstraction.TOP_INT;
  }

  public int getPaddingStart() {
    return Abstraction.TOP_INT;
  }

  public int getPaddingRight() {
    return Abstraction.TOP_INT;
  }

  public int getPaddingEnd() {
    return Abstraction.TOP_INT;
  }

  public boolean isPaddingRelative() {
    return Abstraction.TOP_BOOL;
  }

  public void setSelected(boolean param0) {
  }

  protected void dispatchSetSelected(boolean param0) {
  }

  public boolean isSelected() {
    return Abstraction.TOP_BOOL;
  }

  public void setActivated(boolean param0) {
  }

  protected void dispatchSetActivated(boolean param0) {
  }

  public boolean isActivated() {
    return Abstraction.TOP_BOOL;
  }

  // public android.view.ViewTreeObserver getViewTreeObserver(){
  // return android.view.ViewTreeObserver.TOP;
  // }

  // public android.view.View getRootView(){
  // return android.view.View.TOP;
  // }

  public void getLocationOnScreen(int[] param0) {
  }

  public void getLocationInWindow(int[] param0) {
  }

  // public final android.view.View findViewById(int param0){
  // return android.view.View.TOP;
  // }

  // public final android.view.View findViewWithTag(java.lang.Object param0){
  // return android.view.View.TOP;
  // }

  // public void setId(int param0){
  // }
  //
  // public int getId(){
  // return Abstraction.TOP_INT;
  // }
  //
  // public java.lang.Object getTag(){
  // return Abstraction.TOP_OBJ;
  // }

  // public void setTag(java.lang.Object param0){
  // }

  public java.lang.Object getTag(int param0) {
    return Abstraction.TOP_OBJ;
  }

  public void setTag(int param0, java.lang.Object param1) {
  }

  public int getBaseline() {
    return Abstraction.TOP_INT;
  }

  public boolean isInLayout() {
    return Abstraction.TOP_BOOL;
  }

  public void requestLayout() {
  }

  public void forceLayout() {
  }

  public final void measure(int param0, int param1) {
  }

  protected void onMeasure(int param0, int param1) {
  }

  protected final void setMeasuredDimension(int param0, int param1) {
  }

  public static int combineMeasuredStates(int param0, int param1) {
    return Abstraction.TOP_INT;
  }

  public static int resolveSize(int param0, int param1) {
    return Abstraction.TOP_INT;
  }

  public static int resolveSizeAndState(int param0, int param1, int param2) {
    return Abstraction.TOP_INT;
  }

  public static int getDefaultSize(int param0, int param1) {
    return Abstraction.TOP_INT;
  }

  protected int getSuggestedMinimumHeight() {
    return Abstraction.TOP_INT;
  }

  protected int getSuggestedMinimumWidth() {
    return Abstraction.TOP_INT;
  }

  public int getMinimumHeight() {
    return Abstraction.TOP_INT;
  }

  public void setMinimumHeight(int param0) {
  }

  public int getMinimumWidth() {
    return Abstraction.TOP_INT;
  }

  public void setMinimumWidth(int param0) {
  }

  // public android.view.animation.Animation getAnimation(){
  // return android.view.animation.Animation.TOP;
  // }

  public void startAnimation(android.view.animation.Animation param0) {
  }

  public void clearAnimation() {
  }

  public void setAnimation(android.view.animation.Animation param0) {
  }

  protected void onAnimationStart() {
  }

  protected void onAnimationEnd() {
  }

  protected boolean onSetAlpha(int param0) {
    return Abstraction.TOP_BOOL;
  }

  public void playSoundEffect(int param0) {
  }

  public boolean performHapticFeedback(int param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean performHapticFeedback(int param0, int param1) {
    return Abstraction.TOP_BOOL;
  }

  public void setSystemUiVisibility(int param0) {
  }

  public int getSystemUiVisibility() {
    return Abstraction.TOP_INT;
  }

  public int getWindowSystemUiVisibility() {
    return Abstraction.TOP_INT;
  }

  public void onWindowSystemUiVisibilityChanged(int param0) {
  }

  public void dispatchWindowSystemUiVisiblityChanged(int param0) {
  }

  public void setOnSystemUiVisibilityChangeListener(android.view.View.OnSystemUiVisibilityChangeListener param0) {
  }

  public void dispatchSystemUiVisibilityChanged(int param0) {
  }

  // public final boolean startDrag(android.content.ClipData param0, android.view.View.DragShadowBuilder
  // param1, java.lang.Object param2, int param3){
  // return Abstraction.TOP_BOOL;
  // }

  public boolean onDragEvent(android.view.DragEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean dispatchDragEvent(android.view.DragEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  // public static android.view.View inflate(android.content.Context param0, int param1,
  // android.view.ViewGroup param2){
  // return android.view.View.TOP;
  // }

  protected boolean overScrollBy(int param0, int param1, int param2, int param3, int param4, int param5,
                                 int param6, int param7, boolean param8) {
    return Abstraction.TOP_BOOL;
  }

  protected void onOverScrolled(int param0, int param1, boolean param2, boolean param3) {
  }

  public int getOverScrollMode() {
    return Abstraction.TOP_INT;
  }

  public void setOverScrollMode(int param0) {
  }

  public void setNestedScrollingEnabled(boolean param0) {
  }

  public boolean isNestedScrollingEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public boolean startNestedScroll(int param0) {
    return Abstraction.TOP_BOOL;
  }

  public void stopNestedScroll() {
  }

  public boolean hasNestedScrollingParent() {
    return Abstraction.TOP_BOOL;
  }

  public boolean dispatchNestedScroll(int param0, int param1, int param2, int param3, int[] param4) {
    return Abstraction.TOP_BOOL;
  }

  public boolean dispatchNestedPreScroll(int param0, int param1, int[] param2, int[] param3) {
    return Abstraction.TOP_BOOL;
  }

  public boolean dispatchNestedFling(float param0, float param1, boolean param2) {
    return Abstraction.TOP_BOOL;
  }

  public boolean dispatchNestedPreFling(float param0, float param1) {
    return Abstraction.TOP_BOOL;
  }

  public void setTextDirection(int param0) {
  }

  public int getTextDirection() {
    return Abstraction.TOP_INT;
  }

  public boolean canResolveTextDirection() {
    return Abstraction.TOP_BOOL;
  }

  public boolean isTextDirectionResolved() {
    return Abstraction.TOP_BOOL;
  }

  public void setTextAlignment(int param0) {
  }

  public int getTextAlignment() {
    return Abstraction.TOP_INT;
  }

  public boolean canResolveTextAlignment() {
    return Abstraction.TOP_BOOL;
  }

  public boolean isTextAlignmentResolved() {
    return Abstraction.TOP_BOOL;
  }

  public static int generateViewId() {
    return Abstraction.TOP_INT;
  }

  public android.view.ViewPropertyAnimator animate() {
    return null;
  }

  public final void setTransitionName(java.lang.String param0) {
  }

  public java.lang.String getTransitionName() {
    return Abstraction.TOP_STRING;
  }

}