package android.view;

import android.view.ContextMenu.ContextMenuInfo;

public class MenuItemImpl implements android.view.MenuItem {

  int itemId;
  int groupId;
  String title;
  boolean enabled = true;
  boolean visible = true;
  android.view.ContextMenu.ContextMenuInfo info;
  View actionView;

  public MenuItemImpl(int groupId, int itemId, String string) {
    System.out.println("Creating menuitem " + string);
    this.itemId = itemId;
    this.groupId = groupId;
    this.title = string;
  }

  @Override
  public android.view.ContextMenu.ContextMenuInfo getMenuInfo() {
    return info;
  }

  @Override
  public android.view.MenuItem setIcon(int param0) {
    return this;
  }

  @Override
  public android.view.MenuItem setTitle(int param0) {
    // title = param0;
    return this;
  }

  @Override
  public int getItemId() {
    return itemId;
  }

  @Override
  public android.view.MenuItem setEnabled(boolean param0) {
    this.enabled = param0;
    return this;
  }

  @Override
  public android.view.MenuItem setVisible(boolean param0) {
    this.visible = param0;
    return this;
  }

  @Override
  public CharSequence getTitle() {
    return title;
  }

  @Override
  public boolean isEnabled() {
    return enabled;
  }

  @Override
  public boolean isVisible() {
    return visible;
  }

  @Override
  public void setMenuInfo(ContextMenuInfo info) {
    this.info = info;

  }

  @Override
  public MenuItem setActionView(View actionView) {
    this.actionView = actionView;
    return this;
  }

  @Override
  public View getActionView() {
    return actionView;
  }
}