package android.graphics;

import java.io.OutputStream;

import android.os.Parcel;
import android.os.Parcelable;

public final class Bitmap implements Parcelable {

  public enum CompressFormat {
    JPEG(0), PNG(1), WEBP(2);

    CompressFormat(int nativeInt) {
      this.nativeInt = nativeInt;
    }

    final int nativeInt;
  }

  /**
   * Possible bitmap configurations. A bitmap configuration describes how pixels are stored. This affects the
   * quality (color depth) as well as the ability to display transparent/translucent colors.
   */
  public enum Config {
    // these native values must match up with the enum in SkBitmap.h

    /**
     * Each pixel is stored as a single translucency (alpha) channel. This is very useful to efficiently store
     * masks for instance. No color information is stored. With this configuration, each pixel requires 1 byte
     * of memory.
     */
    ALPHA_8(2),

    /**
     * Each pixel is stored on 2 bytes and only the RGB channels are encoded: red is stored with 5 bits of
     * precision (32 possible values), green is stored with 6 bits of precision (64 possible values) and blue
     * is stored with 5 bits of precision.
     * 
     * This configuration can produce slight visual artifacts depending on the configuration of the source.
     * For instance, without dithering, the result might show a greenish tint. To get better results dithering
     * should be applied.
     * 
     * This configuration may be useful when using opaque bitmaps that do not require high color fidelity.
     */
    RGB_565(4),

    /**
     * Each pixel is stored on 2 bytes. The three RGB color channels and the alpha channel (translucency) are
     * stored with a 4 bits precision (16 possible values.)
     * 
     * This configuration is mostly useful if the application needs to store translucency information but also
     * needs to save memory.
     * 
     * It is recommended to use {@link #ARGB_8888} instead of this configuration.
     *
     * Note: as of {@link android.os.Build.VERSION_CODES#KITKAT}, any bitmap created with this configuration
     * will be created using {@link #ARGB_8888} instead.
     * 
     * @deprecated Because of the poor quality of this configuration, it is advised to use {@link #ARGB_8888}
     *             instead.
     */
    @Deprecated
    ARGB_4444(5),

    /**
     * Each pixel is stored on 4 bytes. Each channel (RGB and alpha for translucency) is stored with 8 bits of
     * precision (256 possible values.)
     * 
     * This configuration is very flexible and offers the best quality. It should be used whenever possible.
     */
    ARGB_8888(6);

    int i;

    Config(int i) {
      this.i = i;
    }
  }

  public static android.graphics.Bitmap TOP = new android.graphics.Bitmap();

  public Bitmap() {
    // no must se;
    // no may se;
  }

  public int getHeight() {
    return 100;
  }

  public int getWidth() {
    return 100;
  }

  public static Bitmap createBitmap(Bitmap b, int i1, int i2, int i3, int i4, Matrix m, boolean bool) {
    return TOP;
  }

  public static Bitmap createBitmap(int i1, int i2, Bitmap.Config conf) {
    return TOP;
  }

  public boolean compress(Bitmap.CompressFormat format, int i1, OutputStream out) {
    return true;
  }

  public void recycle() {

  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
  }
}