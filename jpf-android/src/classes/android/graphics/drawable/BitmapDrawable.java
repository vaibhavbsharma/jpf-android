package android.graphics.drawable;

import android.content.res.Resources;
import android.graphics.Bitmap;

public class BitmapDrawable extends android.graphics.drawable.Drawable {
  public static android.graphics.drawable.BitmapDrawable TOP = new android.graphics.drawable.BitmapDrawable();

  public BitmapDrawable() {
  }

  public BitmapDrawable(android.graphics.Bitmap param0) {
  }

  public BitmapDrawable(Resources r, android.graphics.Bitmap param0) {
  }

  public Bitmap getBitmap() {
    return Bitmap.TOP;
  }
}