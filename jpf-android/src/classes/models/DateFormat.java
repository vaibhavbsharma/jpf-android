package models;

import gov.nasa.jpf.vm.Abstraction;

import java.util.Date;

import android.content.Context;

public class DateFormat {
  public static models.DateFormat TOP = new models.DateFormat();

  public DateFormat() {
  }

  public void setTimeZone(java.util.TimeZone param0) {
  }

  public java.util.Date parse(java.lang.String param0) throws java.text.ParseException {
    return new Date();
  }

  public final java.lang.String format(java.util.Date param0) {
    return Abstraction.TOP_STRING;
  }

  public static DateFormat getDateFormat(Context c) {

    return TOP;
  }
}