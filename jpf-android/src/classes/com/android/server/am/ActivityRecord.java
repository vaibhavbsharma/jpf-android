package com.android.server.am;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;

import java.util.ArrayList;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Binder;

class ActivityRecord extends Binder {

  // Activity activity;
  Intent intent; // the original intent that generated us
  ComponentName realActivity; // the intent component, or target of an alias.
  ActivityRecord resultTo; // who started this entry, so will get our reply
  String resultWho; // additional identifier for use by resultTo.
  int requestCode; // code given by requester (resultTo)
  ArrayList results; // pending ActivityResult objs we have received
  ActivityInfo info; // all about me
  String stringName; // for caching of toString().

  @FilterField
  @NeverBreak
  int id = 0;

  @FilterField
  @NeverBreak
  static private int ID = 0;

  public ActivityRecord(Intent intent, ComponentName realActivity, ActivityRecord resultTo, String resultWho, int requestCode, ActivityInfo info,
      String stringName) {
    super();
    this.intent = intent;
    this.realActivity = realActivity;
    this.resultTo = resultTo;
    this.resultWho = resultWho;
    this.requestCode = requestCode;
    this.info = info;
    this.stringName = stringName;
    this.id = ID++;

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ActivityRecord [id="
        + id
        + ", intent="
        + intent
        + ", realActivity="
        + realActivity
        + ((resultTo != null) ? ", resultTo=" + resultTo.stringName + ", resultWho=" + resultWho + ", requestCode=" + requestCode + ", results="
            + results : "") + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    // result = prime * result + ((intent == null) ? 0 : intent.hashCode());
    // result = prime * result + ((realActivity == null) ? 0 : realActivity.hashCode());
    // result = prime * result + requestCode;
    result = prime * result + id;
    // result = prime * result + ((resultWho == null) ? 0 : resultWho.hashCode());
    // result = prime * result + ((stringName == null) ? 0 : stringName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ActivityRecord other = (ActivityRecord) obj;
    //
    // if (intent == null) {
    // if (other.intent != null)
    // return false;
    // } else if (!intent.equals(other.intent))
    // return false;
    // if (realActivity == null) {
    // if (other.realActivity != null)
    // return false;
    // } else if (!realActivity.equals(other.realActivity))
    // return false;
    // if (requestCode != other.requestCode)
    // return false;
    //
    // if (resultWho == null) {
    // if (other.resultWho != null)
    // return false;
    // } else if (!resultWho.equals(other.resultWho))
    // return false;
    // if (stringName == null) {
    // if (other.stringName != null)
    // return false;
    // } else if (!stringName.equals(other.stringName))
    // return false;
    if (id != other.id) {
      return false;
    }
    return true;
  }

}