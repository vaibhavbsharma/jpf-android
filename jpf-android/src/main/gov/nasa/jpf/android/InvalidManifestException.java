package gov.nasa.jpf.android;

import org.xml.sax.SAXException;

public  class InvalidManifestException extends SAXException {

  public InvalidManifestException(String message) {
    super(message);
  }

}