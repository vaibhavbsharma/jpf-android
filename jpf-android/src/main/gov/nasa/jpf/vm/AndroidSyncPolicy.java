package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;

public class AndroidSyncPolicy extends AllRunnablesSyncPolicy {

  public AndroidSyncPolicy(Config config) {
    super(config);
  }

  @Override
  protected ThreadInfo[] getTimeoutRunnables(ApplicationContext appCtx) {
    ThreadInfo[] allRunnables = super.getTimeoutRunnables(appCtx);

    int maxPrio = Integer.MIN_VALUE;
    int n = 0;
    for (int i = 0; i < allRunnables.length; i++) {
      ThreadInfo ti = allRunnables[i];
      if (ti != null) {
        int prio = ti.getPriority();

        if (prio >= maxPrio) {
          maxPrio = prio;
          n = i;
        }
      }
    }
    ThreadInfo[] prioRunnables = { allRunnables[n] };
    return prioRunnables;
  }

  // @Override
  // protected ChoiceGenerator<ThreadInfo> getRunnableCG(String id, ThreadInfo tiCurrent) {
  // ApplicationContext appCtx = tiCurrent.getApplicationContext();
  // ThreadInfo[] choices = getTimeoutRunnables(appCtx);
  //
  // if (choices.length == 0) {
  // return null;
  // }
  //
  // if ((choices.length == 1) && ((choices[0] == tiCurrent)) && !tiCurrent.isTimeoutWaiting()) { // no
  // context
  // // switch
  // if (!breakSingleChoice) {
  // return null;
  // }
  // }
  //
  // ChoiceGenerator<ThreadInfo> cg = new ThreadChoiceFromSet(id, choices, true);
  //
  // if (!vm.getThreadList().hasProcessTimeoutRunnables(appCtx)) {
  // GlobalSchedulingPoint.setGlobal(cg);
  // }
  //
  // return cg;
  // }
}
