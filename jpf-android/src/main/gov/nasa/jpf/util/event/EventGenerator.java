package gov.nasa.jpf.util.event;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.vm.MJIEnv;

/**
 * This provides an interface between the EventScheduler and different EventGenerators. EventGenerators
 * generates events on the fly when no more events are available.
 * 
 * @author Heila van der Merwe <heilamvdm@gmail.com>
 *
 */
public interface EventGenerator {
  /**
   * Returns a list of events to process non-deterministically;
   * 
   * @param env
   * @param objRef
   *          - used to retrieve values stored in JPF environment used for backtracking and state matching
   *          (should not be null)
   * @param producer
   *          - used to retrieve required data such as currently available events (should not be null)
   * @return
   */
  public Event[] getEvents(MJIEnv env, int objRef, EventsProducer producer);

  /**
   * Returns if the EventGenerator requires events to be collected in the JPF environment
   * 
   * @return
   */
  public boolean collectEvents();

  public void destroy(JPF jpf);

}
