package gov.nasa.jpf.util.event;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.util.JPFLogger;
import gov.nasa.jpf.util.StateExtensionClient;
import gov.nasa.jpf.util.StateExtensionListener;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.generator.DefaultEventGenerator;
import gov.nasa.jpf.util.event.generator.DynamicEventGenerator;
import gov.nasa.jpf.util.event.generator.Huristic2EventGenerator;
import gov.nasa.jpf.util.event.generator.HuristicEventGenerator;
import gov.nasa.jpf.util.event.generator.ScriptEventGenerator;
import gov.nasa.jpf.util.event.generator.TreeEventGenerator;
import gov.nasa.jpf.vm.MJIEnv;

public class AndroidEventGenerator implements EventGenerator, StateExtensionClient<InputStrategy> {
  protected static JPFLogger log = JPF.getLogger("event");

  // Instances of the EventGenerators//
  private ScriptEventGenerator scriptEventGenerator;
  private DefaultEventGenerator defaultEventGenerator;
  private DynamicEventGenerator dynamicEventGenerator;
  private TreeEventGenerator treeEventGenerator;
  private Huristic2EventGenerator heu2EventGenerator;
  private HuristicEventGenerator heuEventGenerator;

  // ///////////////////////////////////

  public EventGenerator eventGenerator;
  public InputStrategy inputStrategy;

  EventsProducer eventProducer;

  public InputStrategy getInputStrategy() {
    return inputStrategy;
  }

  public AndroidEventGenerator(MJIEnv env, EventsProducer eventProducer) {

    Config conf = env.getConfig();
    this.eventProducer = eventProducer;
    try {
      // decide which input strategy to use and set it up
      String configProperty = conf.getProperty("event.strategy", InputStrategy.DEFAULT.name());
      inputStrategy = InputStrategy.getInputStrategy(configProperty);
      log("Inputstrategy= " + inputStrategy);

      switch (inputStrategy) {
      case SCRIPT: {
        String scriptPath = conf.getProperty("android.script");
        if (scriptEventGenerator == null) {
          scriptEventGenerator = new ScriptEventGenerator(scriptPath);
          scriptEventGenerator.registerListener(env.getJPF());
        }
        eventGenerator = scriptEventGenerator;

        break;
      }
      case SCRIPT_GENERATE: {
        String scriptPath = conf.getProperty("android.script");
        if (scriptEventGenerator == null) {
          scriptEventGenerator = new ScriptEventGenerator(scriptPath);
          defaultEventGenerator = new DefaultEventGenerator(conf);
        }
        eventGenerator = scriptEventGenerator;
        break;
      }
      case DYNAMIC: {
        if (dynamicEventGenerator == null) {
          dynamicEventGenerator = new DynamicEventGenerator(conf);
        }
        eventGenerator = dynamicEventGenerator;
        break;
      }
      case EVENTTREE:
        if (treeEventGenerator == null) {
          treeEventGenerator = new TreeEventGenerator(conf);
          eventGenerator = treeEventGenerator;
          treeEventGenerator.registerListener(env.getJPF());
        }
        break;
      case HEURISTIC:
        if (heuEventGenerator == null) {
          heuEventGenerator = new HuristicEventGenerator(conf, eventProducer);
          eventGenerator = heuEventGenerator;
          heuEventGenerator.registerListener(env.getJPF());
        }
        break;
      case HEURISTIC2:
        if (heu2EventGenerator == null) {
          heu2EventGenerator = new Huristic2EventGenerator(conf, eventProducer);
          eventGenerator = heu2EventGenerator;
          heu2EventGenerator.registerListener(env.getJPF());
        }
        break;
      default: {
        if (defaultEventGenerator == null)
          defaultEventGenerator = new DefaultEventGenerator(conf);
        eventGenerator = defaultEventGenerator;
      }
      }
      // include = env.getConfig().getStringArray("event.include");
      // exclude = env.getConfig().getStringArray("event.exclude");
    } catch (Exception e) {
      log.severe("AndroidEventGenerator: " + e.getMessage());
      env.throwException("gov.nasa.jpf.util.event.InvalidEventException",
          "Could not construct AndroidEventGenerator - " + e.getMessage());
    }
  }

  @Override
  public Event[] getEvents(MJIEnv env, int objRef, EventsProducer producer) {
    return eventGenerator.getEvents(env, objRef, producer);
  }

  StateExtensionListener<InputStrategy> sel;

  @Override
  public void registerListener(JPF jpf) {
    if (sel == null) {
      sel = new StateExtensionListener<InputStrategy>(this);
      jpf.addSearchListener(sel);
    }

  }

  @Override
  public void restore(InputStrategy stateExtension) {
    if (stateExtension == null) {
      return;
    }
    this.inputStrategy = stateExtension;

    switch (this.inputStrategy) {
    case SCRIPT: {
      eventGenerator = scriptEventGenerator;
      break;
    }
    case SCRIPT_GENERATE: {
      eventGenerator = scriptEventGenerator;
      break;
    }
    case DYNAMIC: {
      eventGenerator = dynamicEventGenerator;
      break;
    }
    case EVENTTREE: {
      eventGenerator = treeEventGenerator;
      break;
    }
    case HEURISTIC: {
      eventGenerator = heuEventGenerator;
      break;
    }
    case HEURISTIC2: {
      eventGenerator = heu2EventGenerator;
      break;
    }
    default: {
      eventGenerator = defaultEventGenerator;
    }
    }
  }

  @Override
  public InputStrategy getStateExtension() {
    return inputStrategy;
  }

  @Override
  public void destroy(JPF jpf) {
    inputStrategy = null;
    if (eventGenerator != null) {
      eventGenerator.destroy(jpf);
      eventGenerator = null;
    }
    if (defaultEventGenerator != null) {
      defaultEventGenerator.destroy(jpf);
      defaultEventGenerator = null;
    }
    if (dynamicEventGenerator != null) {
      dynamicEventGenerator.destroy(jpf);
      dynamicEventGenerator = null;
    }
    if (scriptEventGenerator != null) {
      scriptEventGenerator.destroy(jpf);
      scriptEventGenerator = null;
    }
    if (treeEventGenerator != null) {
      treeEventGenerator.destroy(jpf);
      treeEventGenerator = null;
    }
    if (heuEventGenerator != null) {
      heuEventGenerator.destroy(jpf);
      heuEventGenerator = null;
    }
    if (heu2EventGenerator != null) {
      heu2EventGenerator.destroy(jpf);
      heu2EventGenerator = null;
    }

    jpf.removeListener(sel);
  }

  private static void log(String message) {
    log.info("AndroidEventGenerator: " + message);
  }

  @Override
  public boolean collectEvents() {
    return eventGenerator.collectEvents();
  }
}
