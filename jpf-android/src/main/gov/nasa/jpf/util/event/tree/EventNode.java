//
// Copyright (C) 2014 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.util.event.tree;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.EventImpl;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 *
 */
public class EventNode implements Cloneable {

  private transient EventNode parent; // set on creation of node
  private EventNode children; // can be added as discovered using addChildren
  // addChild
  private EventNode sibling; // set when an event is added as a child

  private boolean visited = false;
  List<Integer> endStates = new ArrayList<Integer>();

  // private transient EventNode match = null;

  private Event event; // event that we are storing

  public int stateID = -1;
  public List<Integer> nextStates = new ArrayList<Integer>();

  public EventNode() {
    // used for root node only
  }

  /**
   *
   * @param parent
   *          should not be null
   */
  public EventNode(EventNode parent) {
    this.parent = parent;
  }

  /**
   *
   * @param parent
   *          should not be null
   */
  public EventNode(Event event) {
    this.event = event;
  }

  /**
   * The EventNode should only be null in the case of the root node.
   * 
   * @return
   */
  public EventNode getParent() {
    return this.parent;
  }

  /**
   * Event must not be null;
   * 
   * @param event
   * @return
   */
  public EventNode addChild(Event event, int stateID) {
    // ? check for dub?
    EventNode duplicate = checkDuplicate(this.getChildren(), event, stateID);
    if (duplicate != null) {
      return duplicate;
    }

    EventNode newNode = new EventNode(this); // set parent
    newNode.event = event; // set event
    if (this.children == null) {
      this.children = newNode;
    } else {

      for (Iterator<EventNode> it = getChildren(); it.hasNext();) {
        EventNode e = it.next();
        if (it.hasNext() == false) { // move to last item in list
          e.setSibling(newNode); // put in list

        }
      }
    }
    return newNode;
  }

  public EventNode checkDuplicate(Iterator<EventNode> it, Event event, int stateID) {
    while (it.hasNext()) {
      EventNode child = it.next();
      if (child.event.equals(event))
        if (stateID != 0 && this.stateID == stateID) {
          return child;
        } else if (stateID == 0) {
          return child;
        }

    }
    return null;
  }

  /**
   * for (Iterator<EventNode> childIter = getChildren(); childIter.hasNext();) { EventNode node =
   * childIter.next();
   * 
   * }
   * 
   * @return
   */
  public Iterator<EventNode> getChildren() {
    return new EventNodeIterator(children);
  }

  public boolean isInternal() {
    return children != null;
  }

  public boolean isExternal() {
    return children == null;
  }

  public void setSibling(EventNode e) {
    this.sibling = e;
  }

  public EventNode getSibling() {
    return this.sibling;
  }

  /**
   * Event must not be null;
   * 
   * @param event
   * @return
   */
  public EventNode addSibling(Event event, int stateID) {
    // ? check for dub?
    EventNode duplicate = checkDuplicate(new EventNodeIterator(this), event, stateID);
    if (duplicate != null) {
      return duplicate;
    }

    EventNode newNode = new EventNode(this.parent); // set parent
    newNode.event = event; // set event

    EventNodeIterator it = new EventNodeIterator(this);

    while (it.hasNext()) {
      EventNode e = it.next();
      if (it.hasNext() == false) { // move to last item in list
        e.setSibling(newNode); // put in list
      }
    }
    return newNode;
  }

  public boolean visited() {
    return visited;
  }

  public void setEvent(Event event) {
    this.event = event;
  }

  public Event getEvent() {
    return this.event;
  }

  public int printTree(PrintWriter writer, int level) {
    int count = 0;
    if (level != 0)
      writer.println();
    for (int i = 0; i < level; i++) {
      writer.print(". ");
    }
    writer.print(event.print() + " (" + this.stateID + "," + ((nextStates != null && nextStates.size() > 0) ? this.nextStates : "") + ")"
        + ((endStates != null && endStates.size() > 0) ? " endstates:" + endStates.toString() : "") + " (" + ((EventImpl) this.event).getWindowName()
        + "," + ((EventImpl) this.event).getNextWindowName() + ") ");
    if (children == null) {
    } else {
      for (Iterator<EventNode> it = getChildren(); it.hasNext();) {
        EventNode e = it.next();
        count += e.printTree(writer, level + 1);
      }
    }
    return count + 1;
  }

  public int getHash(Event e) {
    final int prime = 37;
    int result = 1;
    String wName = ((EventImpl) e).getWindowName();
    result = prime * result + ((wName == null) ? 0 : wName.hashCode());
    result = prime * result + e.hashCode();
    return result;
  }

  public static final class EventNodeIterator implements Iterator<EventNode> {

    private final EventNode children;
    private EventNode currentNode;

    public EventNodeIterator(EventNode children) {
      this.children = children;
      this.currentNode = this.children;
    }

    @Override
    public boolean hasNext() {
      return this.currentNode != null;
    }

    @Override
    public EventNode next() {
      if (this.hasNext()) {
        EventNode ret = this.currentNode;
        this.currentNode = this.currentNode.getSibling();
        return ret;
      } else
        throw new NoSuchElementException();
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }

  public int size() {
    int num = 0;
    for (Iterator<EventNode> it = getChildren(); it.hasNext();) {
      EventNode c = it.next();
      num += c.size();
    }

    return num + 1;
  }

  public void printPath(PrintWriter w) {
    EventNode pointer = parent;
    StringBuilder sb = new StringBuilder();

    sb.insert(0, event.print() + " (" + this.stateID + "," + ((nextStates != null && nextStates.size() > 0) ? this.nextStates : "") + ")"
        + ((endStates != null && endStates.size() > 0) ? " endstates:" + endStates.toString() : "") + " (" + ((EventImpl) this.event).getWindowName()
        + "," + ((EventImpl) this.event).getNextWindowName() + ")" + "\n");

    while (pointer != null) {
      sb.insert(0, pointer.event.print() + " (" + pointer.stateID + ","
          + ((pointer.nextStates != null && pointer.nextStates.size() > 0) ? pointer.nextStates : "") + ")"
          + ((pointer.endStates != null && pointer.endStates.size() > 0) ? " endstates:" + pointer.endStates.toString() : "") + " ("
          + ((EventImpl) pointer.event).getWindowName() + "," + ((EventImpl) pointer.event).getNextWindowName() + ")" + "\n");
      pointer = pointer.getParent();
    }
    w.print(sb.toString());
  }

  public int countEnd() {
    int count = 0;
    if (children == null) {
      count++;
    } else {
      for (Iterator<EventNode> it = getChildren(); it.hasNext();) {
        EventNode e = it.next();
        count += e.countEnd();
      }
    }
    return count;
  }

  public int countEnds() {
    int count = 0;
    count += endStates.size();
    if (children != null) {
      for (Iterator<EventNode> it = getChildren(); it.hasNext();) {
        EventNode e = it.next();
        count += e.countEnds();
      }
    }
    return count;
  }

  public int count() {
    int count = 0;
    if (children == null) {
    } else {
      for (Iterator<EventNode> it = getChildren(); it.hasNext();) {
        EventNode e = it.next();
        count += e.count();
      }
    }
    return count + 1;
  }

  public void removeChild(EventNode child) {
    EventNode e2 = null;

    for (Iterator<EventNode> it = getChildren(); it.hasNext();) {
      EventNode e = it.next();
      if (e.equals(child)) {
        // remove node
        if (children.equals(e)) {
          // first node in list
          EventNode sibl = e.getSibling();
          if (sibl != null) {
            children = e.getSibling();
          } else {
            children = null;
          }
        } else {
          // assume there are more items in list
          if (e2 != null) {
            e2.setSibling(e);
          }
        }
      }
      e2 = e;
    }
  }

  /**
   * @return the match
   */
  // public EventNode getMatch() {
  // return match;
  // }

  /**
   * @param match
   *          the match to set
   */
  // public void setMatch(EventNode match) {
  // this.match = match;
  // }

  public void hist(int[] hist, int depth) {
    if (endStates.size() > 0) {
      hist[depth] += endStates.size();
    }
    if (children != null) {
      for (Iterator<EventNode> it = getChildren(); it.hasNext();) {
        EventNode e = it.next();
        e.hist(hist, depth + 1);
      }
    }
  }

  public void printSequences(PrintWriter[] writers, int depth) {
    if (endStates.size() > 0) {
      printPath(writers[depth]);
      writers[depth].println();
    }
    if (children != null) {
      for (Iterator<EventNode> it = getChildren(); it.hasNext();) {
        EventNode e = it.next();
        e.printSequences(writers, depth + 1);
      }
    }
  }

  /**
   * 
   * This is used to traverse the Abstract Syntax Tree (AST) of the script and visit each node of the tree.
   * 
   * @param v
   * @throws Exception
   */
  public void accept(EventVisitor v) {
    v.visit(this);
  }

  public int getNumberOfChildren() {
    int count = 0;
    Iterator<EventNode> it = getChildren();
    while (it.hasNext()) {
      count++;
      it.next();
    }
    return count;
  }

  public void addEndState(int stateId2) {
    endStates.add(stateId2);
  }

}
