package gov.nasa.jpf.util.event.generator;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.util.event.EventGenerator;
import gov.nasa.jpf.util.event.EventsProducer;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.SystemEvent;
import gov.nasa.jpf.vm.MJIEnv;
import android.content.ComponentName;
import android.content.Intent;

public class DefaultEventGenerator implements EventGenerator {
  String target;

  public DefaultEventGenerator(Config config) {
    target = config.getTarget();

  }

  @Override
  public Event[] getEvents(MJIEnv env, int objRef, EventsProducer producer) {
    Event[] inputs = producer.getEvents();

    if (producer.isFirstEvent(env)) {
      inputs = generateFirstEvent();
    } else {
      // processEvents
    }

    return inputs;
  }

  private Event[] generateFirstEvent() {
    // get package and activity of first Activity
    String packageName = target.substring(0, target.lastIndexOf('.'));
    String componentName = target.substring(target.lastIndexOf('.') + 1, target.length());
    Intent startIntent = new Intent();
    startIntent.setComponent(new ComponentName(packageName, componentName));
    startIntent.setAction(Intent.ACTION_MAIN);
    startIntent.addCategory(Intent.CATEGORY_LAUNCHER);
    SystemEvent[] events = { new SystemEvent(startIntent) };
    events[0].setAction("startActivity");
    return events;
  }

  @Override
  public void destroy(JPF jpf) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean collectEvents() {
    return true;
  }

}
