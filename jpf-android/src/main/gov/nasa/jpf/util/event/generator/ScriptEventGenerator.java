package gov.nasa.jpf.util.event.generator;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.util.JPFLogger;
import gov.nasa.jpf.util.StateExtensionClient;
import gov.nasa.jpf.util.StateExtensionListener;
import gov.nasa.jpf.util.event.EventGenerator;
import gov.nasa.jpf.util.event.EventsProducer;
import gov.nasa.jpf.util.event.events.AndroidEvent;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.SystemEvent;
import gov.nasa.jpf.util.event.events.UIEvent;
import gov.nasa.jpf.util.script.impl.AlternativeChoiceGenerator;
import gov.nasa.jpf.util.script.impl.Any;
import gov.nasa.jpf.util.script.impl.Any.Choice;
import gov.nasa.jpf.util.script.impl.Repeat;
import gov.nasa.jpf.util.script.impl.RepeatInfinate;
import gov.nasa.jpf.util.script.impl.ResetRepeat;
import gov.nasa.jpf.util.script.impl.Script;
import gov.nasa.jpf.util.script.impl.ScriptElement;
import gov.nasa.jpf.util.script.impl.ScriptElementContainer;
import gov.nasa.jpf.util.script.impl.Section;
import gov.nasa.jpf.util.script.parser.ScriptParser;
import gov.nasa.jpf.util.script.visitor.AddVisitor;
import gov.nasa.jpf.util.script.visitor.PrintVisitor;
import gov.nasa.jpf.vm.MJIEnv;

import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.content.ComponentName;
import android.content.Intent;

/**
 * The ScriptEventGenerator generates events given an input script. The script is parsed into a AST which it
 * traversed
 * 
 * @author heila
 *
 */
public class ScriptEventGenerator implements EventGenerator {

  private static boolean DEBUG_SCRIPT = false;
  protected static JPFLogger log = JPF.getLogger("event");
  protected static final String TAG = "ScriptEventGenerator: ";

  public static class IntentEntry implements Entry<String, Intent> {
    String key;
    Intent value;

    public IntentEntry(String key, Intent value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public String getKey() {
      return this.key;
    }

    @Override
    public Intent getValue() {
      return this.value;
    }

    @Override
    public Intent setValue(Intent value) {
      this.value = value;
      return this.value;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
      IntentEntry entry = new IntentEntry(this.key, new Intent(this.value));
      return entry;
    }

  }

  public static class IntentMap implements StateExtensionClient<IntentEntry[]> {

    Map<String, IntentEntry> intentMap = new HashMap<String, IntentEntry>();

    @Override
    public IntentEntry[] getStateExtension() {
      Collection<IntentEntry> intents = intentMap.values();

      IntentEntry[] ret = new IntentEntry[intents.size()];

      int index = 0;
      for (IntentEntry i : intents) {
        try {
          ret[index] = (IntentEntry) i.clone();
        } catch (CloneNotSupportedException e) {
          e.printStackTrace();
        }
        index++;
      }
      return ret;
    }

    @Override
    public void restore(IntentEntry[] stateExtension) {

      intentMap.clear();
      for (IntentEntry i : stateExtension) {
        intentMap.put(i.key, i);
      }
    }

    StateExtensionListener<IntentEntry[]> sel;

    @Override
    public void registerListener(JPF jpf) {
      if (sel == null) {
        sel = new StateExtensionListener<IntentEntry[]>(this);
        jpf.addSearchListener(sel);
      }
    }

    public Intent getIntent(String id) {
      IntentEntry i = intentMap.get(id);
      if (i != null) {
        return i.getValue();
      } else
        return null;

    }

    public void put(IntentEntry entry) {
      intentMap.put(entry.key, entry);
    }

    public int getLengthOfArgs(Object[] args) {
      int j = 0;
      if (args != null && args.length > 0) {
        do {
          if (args[j] == null) {
            break;
          }
          j++;
        } while (args != null && j < args.length);
      }
      return j;
    }

    public void addProperty(SystemEvent event) {
      IntentEntry i = intentMap.get(event.getTarget());
      Intent intent = null;

      if (event.getIntent() != null) {
        intent = event.getIntent();
      } else {
        if (i != null)
          intent = i.getValue();
      }
      if (intent == null) {
        intent = new Intent();
      }

      if (i == null) {
        i = new IntentEntry(event.getTarget(), intent);
        intentMap.put(i.getKey(), i);
      }

      String action = event.getAction();
      if (action.startsWith("put")) {
        if (event.getArguments() != null && getLengthOfArgs(event.getArguments()) == 2) {
        } else {
          throw new RuntimeException("Method " + action + "() takes two parameters - (key, value)");
        }
      }

      if (action.equals("putExtraString")) {
        intent.putExtra((String) event.getArguments()[0], (String) event.getArguments()[1]);
      } else if (action.equals("putExtraInt")) {
        intent.putExtra((String) event.getArguments()[0], ((Double) event.getArguments()[1]).intValue());
      } else if (action.equals("putExtraBool")) {
        intent.putExtra((String) event.getArguments()[0],
            Boolean.parseBoolean((String) event.getArguments()[1]));
      } else if (action.equals("putExtraFloat")) {
        float f = (float) (double) event.getArguments()[1];
        intent.putExtra((String) event.getArguments()[0], f);
      } else if (action.equals("putExtraDouble")) {
        intent.putExtra((String) event.getArguments()[0], (double) event.getArguments()[1]);
      } else if (action.equals("putExtraByte")) {
        byte f = (byte) (double) event.getArguments()[1];
        intent.putExtra((String) event.getArguments()[0], f);
      } else if (action.equals("putExtraChar")) {
        intent.putExtra((String) event.getArguments()[0], ((String) event.getArguments()[1]).charAt(0));
      } else if (action.equals("putExtraShort")) {
        short f = (short) (double) event.getArguments()[1];
        intent.putExtra((String) event.getArguments()[0], f);
      } else if (action.equals("setComponent")) {
        if (getLengthOfArgs(event.getArguments()) == 1) {
          String componentName = (String) event.getArguments()[0];
          String[] name = componentName.split("\\.");
          if (name != null && name.length >= 2) {
            String mPackage = "";
            String mClass = "";
            mPackage = componentName.substring(0, componentName.lastIndexOf("."));
            mClass = name[name.length - 1]; // get the string after the last ".", this will be the name
            if (mPackage.length() > 0 && mClass.length() > 0) {
              intent.setComponent(new ComponentName(mPackage, mClass));
            } else {
              throw new RuntimeException("setComponent() takes parameter \"com.package.Classname\" given:"
                  + mPackage + "." + mClass);
            }
          } else {
            throw new RuntimeException("setComponent() takes parameter \"com.package.Classname\"");
          }
        } else {
          throw new RuntimeException("setComponent() only takes one argument");
        }
      } else {
        // try to find method using reflection

        // Use reflection to call the setter method on the Intent object
        Class<Intent> intentClass = (Class<Intent>) intent.getClass();

        int j = 0;
        try {
          Method method = null;
          Method[] methods = intentClass.getMethods();

          for (int index = 0; index < methods.length; index++) {
            Method m = methods[index];
            if (m.getName().equals(event.getAction())) {
              method = m;
              break;
            }
          }

          if (method == null) {
            throw new Exception("Could not find method \"" + event.getAction()
                + "\" on Intent.java for parameters " + event.getArguments());
          }
          j = getLengthOfArgs(event.getArguments());
          Object[] a = {};
          if (j > 0) {
            a = Arrays.copyOf(event.getArguments(), j);
          }

          method.invoke(intent, a);

        } catch (Exception e) {
          throw new RuntimeException(e.getMessage());
        }
      }
    }
  }

  /** maps id to scriptelement */
  Map<Integer, ScriptElement> idToScriptElementMap = new HashMap<Integer, ScriptElement>();

  /** maps name of section to its current pointer */
  Map<String, ScriptElement> currentSectionStateByName = new HashMap<String, ScriptElement>();

  ArrayList<String> sectionNameToIndexMap = new ArrayList<String>();
  ArrayList<Repeat> indexToRepeatIDMap = new ArrayList<Repeat>();

  IntentMap intentMap;

  public ScriptEventGenerator(Config c) throws Exception {
    this(c.getProperty("android.script"));
  }

  public ScriptEventGenerator(String filename) throws Exception {
    ScriptParser parser = new ScriptParser(new FileReader(filename));
    Script s = parser.parse();
    PrintVisitor p = new PrintVisitor();
    p.visit(s);

    AddVisitor v = new AddVisitor(idToScriptElementMap, currentSectionStateByName, sectionNameToIndexMap,
        indexToRepeatIDMap);
    v.visit(s);
    intentMap = new IntentMap();
    createPreDefinedIntents();
  }

  public ScriptEventGenerator(Script s) {
    PrintVisitor p = new PrintVisitor();
    p.visit(s);

    AddVisitor v = new AddVisitor(idToScriptElementMap, currentSectionStateByName, sectionNameToIndexMap,
        indexToRepeatIDMap);
    v.visit(s);
    intentMap = new IntentMap();
    createPreDefinedIntents();
  }

  public int[] getRepeatState() {
    int[] state = new int[indexToRepeatIDMap.size()];
    for (int i = 0; i < state.length; i++) {
      Repeat repeat = indexToRepeatIDMap.get(i);
      state[i] = repeat.getCount();
    }
    return state;
  }

  public int[] getScriptState() {
    int[] state = new int[sectionNameToIndexMap.size()];
    for (int i = 0; i < state.length; i++) {
      String sectionName = sectionNameToIndexMap.get(i);
      ScriptElement currentElement = currentSectionStateByName.get(sectionName);
      state[i] = (currentElement != null) ? currentElement.getId() : -1;
    }
    return state;
  }

  public void restoreScriptState(int[] state) {

    for (int i = 0; i < state.length; i++) {
      int stateID = state[i];
      ScriptElement se = idToScriptElementMap.get(stateID);
      String sectionName = sectionNameToIndexMap.get(i);
      currentSectionStateByName.put(sectionName, se);
    }

  }

  public void restoreRepeatState(int[] state) {
    for (int i = 0; i < state.length; i++) {
      int repeatCount = state[i];
      indexToRepeatIDMap.get(i).setCount(repeatCount);
    }
  }

  public gov.nasa.jpf.util.event.events.Event getNextEvent(String sectionName, MJIEnv env) {
    ScriptElement nextScriptElement = currentSectionStateByName.get(sectionName);
    gov.nasa.jpf.util.event.events.Event event = getEventFromScriptElement(nextScriptElement, sectionName,
        env);
    return event;

  }

  /**
   * Convert current ScriptElement to an Event object. It also adds the next script element in map
   * currentSectionStateByName.
   * 
   * ANY: If the current ScriptElement is a ANY, we need to push a cg for each choice. When returning we need
   * to schedule the first element in that choice. This CG could be a cascaded CG in the case that an ANY
   * scriptElement contains another ANY.
   * 
   * The next scriptelement in this case a cg is pushed stays the ANY, else it becomes the first script
   * element in the choice. When no more choices are available, we add the next sibling of the any as the next
   * element
   * 
   * REPEAT: when a repeat is reached we increment the counter and return the first script element in the
   * repeat. For each script element we need to check if its parent is a repeat and its sibling null. In this
   * case we need to check if the repeat is done. If not we restart the sequence in the repeat and return the
   * its first element.
   * 
   * 
   * EVENT: uievent starts with a $
   * 
   * system event with @ or no target.
   * 
   * next event is sibling of event
   * 
   * 
   * CHOICE: never be processed on its own SECTION: never be processed on its own SCRIPT: never be processed
   * on its own
   * 
   * @param currentScriptElement
   * @param env
   * @return
   */

  private ScriptElement getNextScriptElement(ScriptElement currentScriptElement, String sectionName,
                                             MJIEnv env) {
    ScriptElement next = null;
    if (currentScriptElement instanceof Section) {
      next = ((ScriptElementContainer) currentScriptElement).getChildren();
    } else if (currentScriptElement instanceof Any) {
      next = currentScriptElement;
    } else {
      // get next event of currentScriptElement
      next = ((currentScriptElement != null) ? currentScriptElement.getNextSibling() : null);
    }
    // make sure to repeat a repeat if sequence in a repeat is finished
    if (currentScriptElement != null && next == null) {

      // last element in a choice or repeat
      ScriptElement parent = currentScriptElement.getParent();

      while (parent != null && !(parent instanceof Section)) {
        if (parent instanceof Repeat) {
          if (((Repeat) parent).getCount() < ((Repeat) parent).getNumber()) {
            // if repeat not done, set next to start of repeat
            next = parent;
            break;
          } else {
            // if repeat done, set next to next of repeat or parent of repeat
            next = parent.getNextSibling();
            if (next != null) {
              break;
            }
          }
        } else if (parent instanceof Choice) {
          // choice done, next is Any's next
          next = parent.getParent().getNextSibling();
          if (next != null)
            break;
        }
        parent = parent.getParent();
      }
    }

    return next;
  }

  private gov.nasa.jpf.util.event.events.Event getEventFromScriptElement(ScriptElement next,
                                                                         String sectionName, MJIEnv env) {
    // if parent is repeat and no next sibling, first check if repeat must be repeated else add repeat's
    // sibling as next.
    gov.nasa.jpf.util.event.events.Event ret = null;
    if (next == null) {
      ret = null; // no more events
    } else if (next instanceof Any) {
      ret = getEventFromAny(env, (Any) next, sectionName);
    } else if (next instanceof gov.nasa.jpf.util.script.impl.Event) {
      ret = getEventFromEvent(env, (gov.nasa.jpf.util.script.impl.Event) next, sectionName);
    } else if (next instanceof ResetRepeat) {
      ret = getEventFromResetRepeat(env, (ResetRepeat) next, sectionName);
    } else if (next instanceof RepeatInfinate) {
      ret = getEventFromRepeatInfinite(env, (RepeatInfinate) next, sectionName);
    } else if (next instanceof Repeat) {
      ret = getEventFromRepeat(env, (Repeat) next, sectionName);

    } else {
      env.throwException("RuntimeException", "Unknown Event scheduled" + next.toString());
    }

    return ret;
  }

  private gov.nasa.jpf.util.event.events.Event getEventFromRepeatInfinite(MJIEnv env, Repeat e,
                                                                          String sectionName) {
    gov.nasa.jpf.util.event.events.Event ret = null;
    // get first element in repeat
    ScriptElement current = e.getChildren();
    // recursive call to process the first element
    ret = getEventFromScriptElement(current, sectionName, env);

    return ret;
  }

  /**
   * Reset Counter of next Repeat ScriptElement and return first EVent of next Repeat.
   * 
   * @param env
   * @param currentScriptElement
   * @param sectionName
   * @return
   */
  public gov.nasa.jpf.util.event.events.Event getEventFromResetRepeat(MJIEnv env, ResetRepeat rr,
                                                                      String sectionName) {
    Repeat r = (Repeat) rr.getNextSibling();
    r.reset();
    return getEventFromRepeat(env, r, sectionName);
  }

  public gov.nasa.jpf.util.event.events.Event getEventFromEvent(MJIEnv env,
                                                                gov.nasa.jpf.util.script.impl.Event e,
                                                                String sectionName) {
    try {
      gov.nasa.jpf.util.event.events.Event ret = null;
      if (e.getTarget() != null && !e.getTarget().equals("device")) {

        if (e.getTarget().startsWith("$")) {
          // uievent
          ret = new UIEvent(e.getTarget(), e.getAction(), e.getParams());
        } else {
          // intent event
          ret = new SystemEvent(e.getTarget(), e.getAction(), e.getParams());
          intentMap.addProperty((SystemEvent) ret);
          ret = AndroidEvent.DEFAULT;
        }
      } else {
        // target == null
        ret = new SystemEvent(e.getTarget(), e.getAction(), e.getParams());
        for (Object o : e.getParams()) {
          if (o instanceof String) {
            if (((String) o).startsWith("@")) {
              Intent i = intentMap.getIntent(((String) o));
              if (i != null) {
                ((SystemEvent) ret).setIntent(i);
              } else {
                throw new RuntimeException("No Intent found in map for " + ((String) o));
              }
            }
          }
        }
      }
      currentSectionStateByName.put(sectionName, e);
      return ret;
    } catch (Exception exception) {
      env.throwException("gov.nasa.jpf.util.event.InvalidEventException", "ScriptEventGenerator: "
          + exception.getMessage());
    }
    return null;
  }

  public gov.nasa.jpf.util.event.events.Event getEventFromRepeat(MJIEnv env, Repeat e, String sectionName) {
    gov.nasa.jpf.util.event.events.Event ret = null;

    // next repetition
    e.inc();

    // if there are still repetitions remaining
    if (e.getCount() <= e.getNumber()) {
      // get first element in repeat
      ScriptElement current = e.getChildren();
      // recursive call to process the first element
      ret = getEventFromScriptElement(current, sectionName, env);
    } else {
      // no more repetitions - next is repeat's next
      ScriptElement current = e.getNextSibling();

      // if repeat has no next
      if (current == null) {
        ScriptElement parent = e.getParent(); // get parent
        if (parent instanceof Repeat) {
          current = parent; // we need to process parent
        } else if (parent instanceof Choice) {
          current = parent.getParent().getNextSibling(); // move to any's next
        } else {
          current = null; // no more events
        }
      }
      ret = getEventFromScriptElement(current, sectionName, env);
    }
    return ret;
  }

  public gov.nasa.jpf.util.event.events.Event getEventFromAny(MJIEnv env, Any any, String sectionName) {
    gov.nasa.jpf.util.event.events.Event ret = null;

    String id = "0000";
    ScriptElement e = any.getParent();
    while (!(e instanceof Section)) {
      if (e instanceof Repeat) {
        id += String.valueOf(((Repeat) e).getCount());
      }
      e = e.getParent();
    }

    AlternativeChoiceGenerator acg = env.getSystemState().getCurrentChoiceGenerator(
        String.valueOf(id) + String.valueOf(any.getId()), AlternativeChoiceGenerator.class);
    if (acg == null) {

      // push cg for each first element in choice
      ScriptElement children = (any).getChildren();
      int count = 0;
      while (children != null) {
        count++;
        children = children.getNextSibling();
      }

      acg = new AlternativeChoiceGenerator(String.valueOf(id) + String.valueOf(any.getId()), count);
      env.setNextChoiceGenerator(acg);
      System.out.println("New AlternativeCG" + acg);
      env.repeatInvocation(); // re-executes the current instruction to make
      // sure that we set the first choice of the cg now.

      ret = AndroidEvent.DEFAULT;
      currentSectionStateByName.put(sectionName, any);

      // reschedule this ScriptElement so that we try this again

      // do not change next event as it will be rescheduled
    } else {
      System.out.println("Loading AlternativeCG" + acg);

      // move to the next choice event
      int choice = acg.getNextChoice();
      int count = 1;
      ScriptElement children = (any).getChildren();
      while (children != null && count < choice) {
        count++;
        children = children.getNextSibling();
      }

      ScriptElement current = ((Any.Choice) children).getChildren();
      ret = getEventFromScriptElement(current, sectionName, env);
      // env.repeatInvocation(); // re-executes the current instruction to make
      // sure that we set the first choice
      // of the cg now.
      // env.repeatInvocation(); // re-executes the current instruction to make
      // return ret;
    }
    return ret;
  }

  /**
   * Algorithm: shift to point to next event, return next event, save state
   * 
   * @param env
   * @param objRef
   * @param currentWindow
   * @return
   */
  @Override
  public Event[] getEvents(MJIEnv env, int objRef, EventsProducer producer) {
    if (env.getReferenceField(objRef, "scriptState") == MJIEnv.NULL) {
      env.setReferenceField(objRef, "scriptState", env.newIntArray(getScriptState()));
      env.setReferenceField(objRef, "repeatState", env.newIntArray(getRepeatState()));

      if (DEBUG_SCRIPT) {
        log.info("ScriptState (initialize JPF ss): " + Arrays.toString(getScriptState()));
        log.info("RepeatState (initialize JPF rs): " + Arrays.toString(getRepeatState()));
      }
    }

    int[] temp_ss = getScriptState();
    int[] temp_rs = getRepeatState();

    restoreScriptState(env.getIntArrayObject(env.getReferenceField(objRef, "scriptState")));
    restoreRepeatState(env.getIntArrayObject(env.getReferenceField(objRef, "repeatState")));

    if (DEBUG_SCRIPT) {
      log.info("ScriptState (native restoring ss): " + Arrays.toString(temp_ss) + "=>"
          + Arrays.toString(getScriptState()));
      log.info("RepeatState (native restoring rs): " + Arrays.toString(temp_rs) + "=>"
          + Arrays.toString(getRepeatState()));
    }
    String currentWindow = producer.getCurrentWindow(env, objRef);
    String sectionName = currentWindow;
    if (!currentSectionStateByName.containsKey(sectionName)) {
      // strip of package and try again
      sectionName = sectionName.substring(sectionName.lastIndexOf(".") + 1, sectionName.length());
    }
    log.info("Getting next script action for window: " + currentWindow);

    log.info("Getting next script action for window: " + sectionName);
    ScriptElement next = this.getNextScriptElement(currentSectionStateByName.get(sectionName), sectionName,
        env);

    gov.nasa.jpf.util.event.events.Event event = null;
    if (next != null)
      event = this.getEventFromScriptElement(next, sectionName, env);

    temp_ss = env.getIntArrayObject(env.getReferenceField(objRef, "scriptState"));
    temp_rs = env.getIntArrayObject(env.getReferenceField(objRef, "repeatState"));

    env.setReferenceField(objRef, "scriptState", env.newIntArray(getScriptState()));
    env.setReferenceField(objRef, "repeatState", env.newIntArray(getRepeatState()));

    if (DEBUG_SCRIPT) {
      log.info("ScriptState (JPF ss update): " + Arrays.toString(temp_ss) + "=>"
          + Arrays.toString(getScriptState()));
      log.info("RepeatState (JPF rs update): " + Arrays.toString(temp_rs) + "=>"
          + Arrays.toString(getRepeatState()));
    }

    Event[] events = null;

    if (event != null) {
      events = new Event[1];
      events[0] = event;
    } else {
      events = new Event[0];
    }
    return events;
  }

  @Override
  public void destroy(JPF jpf) {
    // TODO close readers (parser)

  }

  @Override
  public boolean collectEvents() {
    // we generate events on the fly - not interested in currently available events (for future we can maybe
    // use to verify that script event exists)
    return false;
  }

  public void createPreDefinedIntents() {
    Intent i = new Intent();
    IntentEntry intent = new IntentEntry("@WifiOffIntent", i);
    i.setAction("android.net.conn.CONNECTION_CHANGE");
    i.putExtra("type", "WIFI");
    i.putExtra("state", "DISCONNECTED");
    i.putExtra("reason", "");
    i.putExtra("extra", "");
    intentMap.put(intent);

    i = new Intent();
    intent = new IntentEntry("@WifiOnIntent", i);
    i.setAction("android.net.conn.CONNECTION_CHANGE");
    i.putExtra("type", "WIFI");
    i.putExtra("state", "CONNECTED");
    i.putExtra("reason", "");
    i.putExtra("extra", "");
    intentMap.put(intent);

    i = new Intent();
    intent = new IntentEntry("@WifiSuspendedIntent", i);
    i.setAction("android.net.conn.CONNECTION_CHANGE");
    i.putExtra("type", "WIFI");
    i.putExtra("state", "SUSPENDED");
    i.putExtra("reason", "");
    i.putExtra("extra", "");
    intentMap.put(intent);

    i = new Intent();
    intent = new IntentEntry("@MobileDisconnectedIntent", i);
    i.setAction("android.net.conn.CONNECTION_CHANGE");
    i.putExtra("type", "MOBILE");
    i.putExtra("state", "DISCONNECTED");
    i.putExtra("reason", "");
    i.putExtra("extra", "");
    intentMap.put(intent);

    i = new Intent();
    intent = new IntentEntry("@MobileConnectedIntent", i);
    i.setAction("android.net.conn.CONNECTION_CHANGE");
    i.putExtra("type", "MOBILE");
    i.putExtra("state", "CONNECTED");
    i.putExtra("reason", "");
    i.putExtra("extra", "");
    intentMap.put(intent);

    i = new Intent();
    intent = new IntentEntry("@WifiConnectedIntent", i);
    i.setAction("android.net.conn.CONNECTION_CHANGE");
    i.putExtra("type", "MOBILE");
    i.putExtra("state", "SUSPENDED");
    i.putExtra("reason", "");
    i.putExtra("extra", "");
    intentMap.put(intent);

    i = new Intent();
    intent = new IntentEntry("@urlInputStreamIntent", i);
    i.setAction("android.net.conn.URL_INPUT_STREAM");
    intentMap.put(intent);

  }

  public void registerListener(JPF jpf) {
    intentMap.registerListener(jpf);
  }

}
