//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.util.event.events;

import java.util.Arrays;

/**
 * This models a single user interaction, which maps to a (reflection) call of a method in a android.view.View
 * instance (or a static method if there is no target spec).
 * 
 * <h2>Supported UI Events</h2>
 * <table>
 * <th>
 * <td>Component</td>
 * <td>Script Actions</td></th>
 * <tr>
 * 
 * </tr>
 * </table>
 * Button button1.click(), button2.longClick() EditText editName.setText("Heila") ListView
 * listNames.selectItem("Heila") CheckBox checkAccept.check(true) RadioGroup options.selectOption("Option 1")
 * Device action device.rotate() device.pressHome() device.pressBack() TODO IntegerPicker, DatePicker,
 * ImageButton, Scroller, VideoView
 * 
 * UI Events are sent to the native WindowManager class by the AndroidEventProducer. The WindownManager class
 * then calls a method in the currentky visible Window class to process the event.
 *
 * @author Heila van der Merwe <heilamvdm@gmail.com>
 * @date Edited 27 March 2015
 */
public class UIEvent extends EventImpl {

  /** The component name that can be resolved to a widget in the view hierarchy */
  private String target;

  /** The name of the action mapped to a method that needs to be executed on the widget */
  private String action;

  private Object[] arguments;

  public UIEvent(String target, String action) {
    this(target, action, null);
  }

  public UIEvent(String target, String action, Object[] arguments) {

    this.target = target;
    this.action = action;
    this.arguments = arguments;
  }

  /**
   * 'spec' has the form $target.action target is either a number or a hierarchical string of ids, e.g.
   * $MyFrame.Ok
   */
  public UIEvent(String spec, Object[] args) {

    int i = spec.lastIndexOf('.');
    if (i > 0) {
      target = spec.substring(0, i);
      spec = spec.substring(i + 1);
    }

    action = spec;
    this.arguments = args;
  }

  public String getTarget() {
    return target;
  }

  protected void setTarget(String target) {
    this.target = target;
  }

  public String getAction() {
    return action;
  }

  protected void setAction(String s) {
    action = s;
  }

  public Object[] getArguments() {
    return arguments;
  }

  public void addArgument(Object o) {
    if (arguments == null) {
      arguments = new Object[1];
      arguments[0] = o;
    } else {
      Object[] newArgs = new Object[arguments.length + 1];
      System.arraycopy(arguments, 0, newArgs, 0, arguments.length);
      newArgs[arguments.length] = o;
      arguments = newArgs;
    }

  }

  @Override
  public String toString() {
    StringBuilder b = new StringBuilder();
    if (target != null) {
      b.append(target);
      b.append('.');
    }
    if (action != null) {
      b.append(action);
      b.append('(');
      if (arguments != null)
        b.append(Arrays.toString(arguments));
      b.append(')');

    }
    return b.toString();
  }

  // @Override
  /**
   * Compares action & target field strings as well as the list of arguments of type Object.
   *
   * The argument lists considered equal if both arrays contain the same number of elements, and all
   * corresponding pairs of elements in the two arrays are equal. Two objects e1 and e2 are considered equal
   * if (e1==null ? e2==null : e1.equals(e2)). In other words, the two arrays are equal if they contain the
   * same elements in the same order. Also, two array references are considered equal if both are null.
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof UIEvent)) {
      return false;
    }
    UIEvent other = (UIEvent) obj;
    if (action == null) {
      if (other.action != null) {
        return false;
      }
    } else if (!action.equals(other.action)) {
      return false;
    }
    if (!Arrays.equals(arguments, other.arguments)) {
      return false;
    }
    if (target == null) {
      if (other.target != null) {
        return false;
      }
    } else if (!target.equals(other.target)) {
      return false;
    }
    return true;
  }

  @Override
  public String print() {
    return toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((action == null) ? 0 : action.hashCode());
    result = prime * result + ((arguments == null || arguments.length == 0) ? 0 : Arrays.hashCode(arguments));
    result = prime * result + ((target == null) ? 0 : target.hashCode());
    return result;
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    UIEvent e = null;
    if (this.getArguments() != null) {
      e = new UIEvent(this.getTarget(), this.getAction(), Arrays.copyOf(this.getArguments(), this.getArguments().length));
    } else {
      e = new UIEvent(this.getTarget(), this.getAction());
    }
    return e;
  }

}
