package gov.nasa.jpf.util.event.generator;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.util.StateExtensionClient;
import gov.nasa.jpf.util.StateExtensionListener;
import gov.nasa.jpf.util.event.EventGenerator;
import gov.nasa.jpf.util.event.EventsProducer;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.EventImpl;
import gov.nasa.jpf.util.event.events.SystemEvent;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.VM;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import android.content.ComponentName;
import android.content.Intent;
import android.util.Log;

/**
 * Fires events only once in a branch
 * 
 * @author heila
 *
 */
public class HuristicEventGenerator implements EventGenerator {

  HashMap<Integer, EventWrapper> events = new HashMap<Integer, EventWrapper>();

  public static class EventWrapper {

    int hash;
    private int count = 0;

    public EventWrapper(int hash) {
      this.hash = hash;
    }

    public EventWrapper(EventWrapper e) {
      this.hash = e.getHash();
      this.setCount(e.getCount());
    }

    public void increaseCount() {
      count++;
    }

    public void setCount(int count) {
      this.count = count;
    }

    public int getCount() {
      return count;
    }

    public int getHash() {
      return hash;
    }

    @Override
    public int hashCode() {
      return hash;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
      EventWrapper e = new EventWrapper(this.getHash());
      e.setCount(this.getCount());
      return e;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      EventWrapper other = (EventWrapper) obj;
      if (hash != other.hash)
        return false;
      return true;
    }
  }

  String target;
  VM vm;
  StateListener listener;
  EventsProducer eventProducer;
  DynamicEventGenerator generator;
  boolean dynamicEvents = false;
  Config c;
  public final int MAX;

  public HuristicEventGenerator(Config config, EventsProducer eventProducer) {
    c = config;
    target = config.getTarget();
    this.eventProducer = eventProducer;
    dynamicEvents = config.getBoolean("event.heuristic.dynamic", false);
    MAX = config.getInt("event.heuristic.count", 1);
    if (dynamicEvents)
      generator = new DynamicEventGenerator(config);

  }

  public static class StateListener implements StateExtensionClient<EventWrapper[]> {

    HuristicEventGenerator mGenerator;

    public StateListener(HuristicEventGenerator generator) {
      mGenerator = generator;
    }

    @Override
    public EventWrapper[] getStateExtension() {
      EventWrapper[] cloneArray = new EventWrapper[mGenerator.events.size()];
      int i = 0;
      for (EventWrapper e : mGenerator.events.values()) {
        cloneArray[i] = new EventWrapper(e);
        i++;
      }
      return cloneArray;

    }

    @Override
    public void restore(EventWrapper[] stateExtension) {
      for (EventWrapper e : stateExtension) {
        EventWrapper wrapper = mGenerator.events.get(e.hashCode());
        if (wrapper == null)
          mGenerator.events.put(e.getHash(), new EventWrapper(e));
        else
          wrapper.setCount(e.getCount());
      }
      Iterator<Entry<Integer, EventWrapper>> iter = mGenerator.events.entrySet().iterator();
      while (iter.hasNext()) {
        Entry<Integer, EventWrapper> entry = iter.next();
        boolean found = false;
        for (EventWrapper w : stateExtension) {
          if (w.hashCode() == entry.getKey()) {
            found = true;
            break;
          }
        }
        if (!found) {
          iter.remove();
        }
      }

    }

    StateExtensionListener<EventWrapper[]> sel;

    @Override
    public void registerListener(JPF jpf) {
      sel = new StateExtensionListener<EventWrapper[]>(this);
      jpf.addSearchListener(sel);
    }

    public void removeListener(JPF jpf) {
      jpf.removeListener(sel);
    }

  }

  @Override
  public Event[] getEvents(MJIEnv env, int objRef, EventsProducer producer) {
    Event[] eventList = producer.getEvents();
    if (producer.isFirstEvent(env)) {
      return generateFirstEvent();
    } else {
      // processEvents

      if (dynamicEvents)
        eventList = generator.getEvents(env, objRef, producer, eventList);
      eventList = checkEvents(eventList);
      return eventList;
    }
  }

  public Event[] checkEvents(Event[] events) {
    int len = events.length;
    int newLen = len;

    for (int i = 0; i < len; i++) {
      Event e = events[i];
      String wName = ((EventImpl) e).getWindowName();

      if (wName.contains("$") && e instanceof SystemEvent && ((SystemEvent) e).getAction().equals("sendBroadcast")) {
        events[i] = null;
        newLen--;
        continue;
      }
      int hash = getHash(e);

      EventWrapper ew = this.events.get(hash);
      if (ew == null) {
        ew = new EventWrapper(hash);
        this.events.put(hash, ew);
      }

      if (ew.getCount() < MAX) {
        if (c.getBoolean("event.heuristic.tree", false))
          ew.increaseCount();
      } else {
        events[i] = null;
        newLen--;
      }

    }

    // if events have been filtered, construct new array
    if (newLen < len) {
      Event[] ret = new Event[newLen];

      int j = 0;
      for (int i = 0; i < len; i++) {
        if (events[i] != null) {
          ret[j] = events[i];
          j++;
        }
      }
      return ret;
    }
    return events;
  }

  private Event[] generateFirstEvent() {
    // get package and activity of first Activity
    String packageName = target.substring(0, target.lastIndexOf('.'));
    String componentName = target.substring(target.lastIndexOf('.') + 1, target.length());
    Intent startIntent = new Intent();
    startIntent.setComponent(new ComponentName(packageName, componentName));
    startIntent.setAction(Intent.ACTION_MAIN);
    startIntent.addCategory(Intent.CATEGORY_LAUNCHER);
    SystemEvent[] events = { new SystemEvent(startIntent) };
    events[0].setAction("startActivity");
    events[0].setWindowName("default");
    return events;
  }

  @Override
  public void destroy(JPF jpf) {
    if (listener != null) {
      listener.removeListener(jpf);
    }
    if (generator != null) {
      generator.destroy(jpf);
    }
  }

  @Override
  public boolean collectEvents() {
    return true;
  }

  public void registerListener(JPF jpf) {
    this.vm = jpf.getVM();
    listener = new StateListener(this);
    listener.registerListener(jpf);
  }

  public void addEvent(Event retEvent) {
    if (c.getBoolean("event.heuristic.tree", false))
      return;
    int h = getHash(retEvent);
    EventWrapper ew = this.events.get(h);
    if (ew != null) {
      ew.increaseCount();
    } else {
      Log.i("HeuristicEventGenerator", "Could not locate event in heurtistic map:" + retEvent);
    }
  }

  public int getHash(Event e) {
    final int prime = 37;
    int result = 1;
    String wName = ((EventImpl) e).getWindowName();
    result = prime * result + wName.hashCode();
    result = prime * result + e.hashCode();
    return result;
  }

}
