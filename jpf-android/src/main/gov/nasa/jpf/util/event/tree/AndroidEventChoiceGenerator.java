//
// Copyright (C) 2014 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.util.event.tree;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.vm.ChoiceGeneratorBase;

import java.util.Arrays;

/**
 * ChoiceGenerator to schedule a set of alternative (concurrent) Events. This is basically just a pointer into
 * the event tree to the current Event. Each event in the eventTree stores a list of alternatives in the case
 * of a set of alternative event should occur at the same time.
 */
public class AndroidEventChoiceGenerator extends ChoiceGeneratorBase<Event> {

  Event[] events;
  int choice;

  public AndroidEventChoiceGenerator(Event[] events) {
    super(String.valueOf(Arrays.hashCode(events)));
    this.events = events;
    choice = -1;
  }

  /** Returns the next alternative event that is being processed or has to be processed */
  @Override
  public Event getNextChoice() {
    Event ret = events[choice];
    return ret;
  }

  @Override
  public Class<Event> getChoiceType() {
    return Event.class;
  }

  /** Have we finished processing all alternatives (choices)? */
  @Override
  public boolean hasMoreChoices() {
    return choice < events.length - 1;
  }

  /** Move on to next alternative event */
  @Override
  public void advance() {
    choice++;
  }

  @Override
  public void reset() {
    isDone = false;
    choice = -1;
  }

  @Override
  public int getTotalNumberOfChoices() {
    return events.length;
  }

  @Override
  public int getProcessedNumberOfChoices() {
    return choice + 1;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder(getClass().getName());
    sb.append("[ " + this.hashCode() + " id=\"");
    sb.append(id);
    sb.append('"');

    sb.append(",isCascaded:");
    sb.append(Boolean.toString(isCascaded));

    sb.append(",{");
    for (int i = 0; i < events.length; i++) {
      if (i != choice) {
        sb.append(',');
      }
      if (i == choice) {
        sb.append(MARKER);
      }
      sb.append(events[i].toString());
    }
    sb.append("}]");

    return sb.toString();
  }
}
