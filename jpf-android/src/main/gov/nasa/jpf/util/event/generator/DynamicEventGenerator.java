package gov.nasa.jpf.util.event.generator;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.util.event.EventsProducer;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.SystemEvent;
import gov.nasa.jpf.util.event.generator.dynamic.MongoDBWrapper;
import gov.nasa.jpf.util.event.generator.dynamic.XStreamConverter;
import gov.nasa.jpf.vm.MJIEnv;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import android.content.Intent;

/**
 * Returns events for a set of entrypoints
 * 
 * @author heila
 *
 */
public class DynamicEventGenerator extends DefaultEventGenerator {
  MongoDBWrapper tester;
  XStreamConverter converter;

  public DynamicEventGenerator(Config config) {
    super(config);
    try {
      tester = new MongoDBWrapper("event_db", "events");
      converter = new XStreamConverter();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public Event[] getEvents(MJIEnv env, int androidEventProducerRef, EventsProducer producer) {
    Event[] events = super.getEvents(env, androidEventProducerRef, producer);
    if (events != null) {
      List<Event> inputs = new LinkedList<Event>();
      for (Event e : events) {
        List<Event> inputEvents = expandEvent(e);
        inputs.addAll(inputEvents);
      }
      return inputs.toArray(new Event[inputs.size()]);
    }
    return null;
  }

  /**
   * Lookup extra information about event and expand it into a list of events
   * 
   * @param e
   * @return
   */
  private List<Event> expandEvent(Event event) {

    List<Event> inputs = new LinkedList<Event>();

    if (event != null && event instanceof SystemEvent && ((SystemEvent) event).getIntent() != null
        && ((SystemEvent) event).getAction().equals("sendBroadcast")) {

      Event[] events = lookupBundle((SystemEvent) event);
      if (events == null || events.length == 0 || events[0] == null) {
        if (event != null) {
          events = new Event[1];
          events[0] = event;
        }
      }
      inputs = Arrays.asList(events);

    } else {
      if (event != null) {
        inputs.add(event);
      }
    }
    return inputs;
  }

  private Event[] lookupBundle(SystemEvent e) {
    List<Event> inputs = new LinkedList<Event>();

    SystemEvent event = e;

    List<String> params = null;
    String className = e.getIntent().getComponent().getClassName();
    System.out.println("$$$$$$$ " + className);
    try {
      // assert statements
      params = tester.getInputParameters(className,
          "onReceive(Landroid/content/Context;Landroid/content/Intent;)V");
      // params = tester.getInputParameters("com.example.android.musicplayer.MusicIntentReceiver",
      // "onReceive(Landroid/content/Context;Landroid/content/Intent;)V");
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    if (params == null || params.size() < 1) {
      inputs.add(e);
      return inputs.toArray(new Event[inputs.size()]);
    }
    Object[] paramsObjects = new Object[params.size()];
    int index = 0;
    for (String p : params) {
      String[] pp = p.split(",");
      List<Object> objs = new LinkedList<Object>();

      for (String ppp : pp) {
        Object o = null;
        try {
          // System.out.println("XStream getting object for: " + ppp);

          o = converter.getObject(ppp);
        } catch (Exception e1) {
          System.out.println("Catching all exceptions");
          // HIDE FOR NOW
          continue;
        }
        objs.add(o);
      }

      paramsObjects[index++] = objs;
      // System.out.println(Arrays.toString(paramsObjects));

      for (Object o : (List) paramsObjects[index - 1]) {
        Object listObjects = o;
        if (((Intent) o).getAction().equals(event.getIntent().getAction())) {
          SystemEvent clone = null;
          try {
            clone = (SystemEvent) event.clone();
          } catch (CloneNotSupportedException e1) {
            e1.printStackTrace();
          }
          clone.setIntent((Intent) listObjects);
          inputs.add(clone);
        }
      }
    }

    return inputs.toArray(new Event[inputs.size()]);
  }

  public Event[] getEvents(MJIEnv env, int objRef, EventsProducer producer, Event[] events) {
    if (events != null) {
      List<Event> inputs = new LinkedList<Event>();
      for (Event e : events) {
        List<Event> inputEvents = expandEvent(e);
        inputs.addAll(inputEvents);
      }
      return inputs.toArray(new Event[inputs.size()]);
    }
    return null;
  }

}
