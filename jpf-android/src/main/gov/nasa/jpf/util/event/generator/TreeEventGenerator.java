package gov.nasa.jpf.util.event.generator;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.util.StateExtensionClient;
import gov.nasa.jpf.util.StateExtensionListener;
import gov.nasa.jpf.util.event.EventGenerator;
import gov.nasa.jpf.util.event.EventsProducer;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.generator.dynamic.XStreamConverter;
import gov.nasa.jpf.util.event.generator.dynamic.XStreamConverter.XStreamConverterException;
import gov.nasa.jpf.util.event.tree.AndroidEventTree;
import gov.nasa.jpf.util.event.tree.EventNode;
import gov.nasa.jpf.util.event.tree.EventNode.EventNodeIterator;
import gov.nasa.jpf.vm.MJIEnv;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedList;

public class TreeEventGenerator implements EventGenerator {

  private class EventTreeInputWrapper implements StateExtensionClient<EventNode> {
    AndroidEventTree tree;
    EventNode current;

    public EventTreeInputWrapper(AndroidEventTree p) {
      this.tree = p;
    }

    @Override
    public EventNode getStateExtension() {
      return current;
    }

    @Override
    public void restore(EventNode stateExtension) {
      current = stateExtension;
    }

    StateExtensionListener<EventNode> sel;

    @Override
    public void registerListener(JPF jpf) {
      if (sel == null) {
        sel = new StateExtensionListener<EventNode>(this);
        jpf.addSearchListener(sel);
      }
    }

    public Event[] getNextEvents(EventNode n) {
      if (current == null) {
        // root has not been processed
        current = tree.root();
        return new Event[] { tree.root().getEvent() };
      } else {
        if (current.equals(tree.root())) {
          // root has been processed, return its children
          Event[] events = new Event[tree.root().getNumberOfChildren()];

          Iterator<EventNode> it = tree.root().getChildren();
          int child = 0;
          EventNode sibling = null;
          while (it.hasNext()) {
            EventNode node = it.next();
            if (sibling == null)
              sibling = node;
            events[child] = node.getEvent();
            child++;
          }
          current = sibling;
          return events;
        } else {
          // get the current child matching the processed event
          LinkedList<Event> children = new LinkedList<Event>();
          current = current.checkDuplicate(new EventNodeIterator(current), n.getEvent(), 0);
          if (current == null) {
            return null;
          }
          // get its children
          Event[] events = new Event[current.getNumberOfChildren()];
          Iterator<EventNode> it = current.getChildren();
          int count = 0;
          while (it.hasNext()) {
            EventNode node = it.next();
            events[count] = node.getEvent();
            count++;
          }
          return events;
        }
      }
    }
  }

  EventTreeInputWrapper treeWrapper;

  public TreeEventGenerator(Config config) {
    try {
      XStreamConverter converter = new XStreamConverter();
      converter.xstream.setClassLoader(config.getClassLoader());
      String s = new String(Files.readAllBytes(Paths.get("test2.xml")));
      AndroidEventTree p = (AndroidEventTree) converter.getObject(s);
      treeWrapper = new EventTreeInputWrapper(p);
      System.out.println("Success" + (p != null));
    } catch (UnsupportedEncodingException | FileNotFoundException e) {
      e.printStackTrace();
    } catch (XStreamConverterException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  @Override
  public Event[] getEvents(MJIEnv env, int objRef, EventsProducer producer) {
    EventNode n = producer.getCurrentEvent();
    return treeWrapper.getNextEvents(n);
  }

  @Override
  public void destroy(JPF jpf) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean collectEvents() {
    return false;
  }

  public void registerListener(JPF jpf) {
    treeWrapper.registerListener(jpf);
  }

}
