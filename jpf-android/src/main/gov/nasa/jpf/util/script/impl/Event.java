package gov.nasa.jpf.util.script.impl;


public class Event extends ScriptElement {

  private String target;
  private String action;
  private Object[] params;
  private int size = 0;

  public Event(int linenumber, ScriptElementContainer parent) {
    super(linenumber, parent);
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public Object[] getParams() {
    return params;
  }

  public void addParam(Object param) {
    if (this.params == null) {
      params = new Object[10];
      size = 0;
    } else if (size == 9) {
      // resize if to small
      Object[] n = new Object[20];
      System.arraycopy(params, 0, n, 0, 10);
      size = 10;
    }
    params[size] = param;
    size++;
  }

  @Override
  public String toString() {
    String eventString =getTarget() + "." + getAction() + "(";
    if (getParams() != null) {
      for (Object s : getParams()) {
        if (s != null)
          if (s instanceof String) {
            eventString += "\"" + s.toString() + "\" , ";
          } else {
            eventString += s.toString() + ", ";
          }
      }
      eventString = eventString.substring(0, eventString.length() - 3);
    }

    eventString += ")";
    return  eventString;
  }

}
