package gov.nasa.jpf.util.script.impl;


public class ResetRepeat extends ScriptElement {

  public ResetRepeat(int linenumber, ScriptElementContainer parent) {
    super(linenumber, parent);
  }

  public void resetCounter() {
    ((Repeat) this.sibling).count = 0;
  }

}
