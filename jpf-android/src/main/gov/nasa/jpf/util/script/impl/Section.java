package gov.nasa.jpf.util.script.impl;


public class Section extends ScriptElementContainer {

  private String name;

  public Section(int linenumber, ScriptElementContainer parent) {
    super(linenumber, parent);
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
