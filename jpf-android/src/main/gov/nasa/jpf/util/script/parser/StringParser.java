package gov.nasa.jpf.util.script.parser;

public class StringParser {

  public static Object getObjectFromString(String s) {
    if (s == null || s.length() == 0) {
      return null;
    } else if (isInt(s))
      return Integer.parseInt(s);
    else if (isFloat(s))
      return Float.parseFloat(s);
    else if (isBool(s))
      return Boolean.parseBoolean(s);
    return s;
  }

  private static boolean isInt(String s) {
    boolean test = true;
    for (byte c : s.getBytes()) {
      if (!Character.isDigit(c)) {
        test = false;
        break;
      }
    }
    return test;

  }

  private static boolean isFloat(String s) {
    boolean test = true;
    for (byte c : s.getBytes()) {
      if (!Character.isDigit(c) && ((char) c) != '.' && ((char) c) != ',') {
        test = false;
        break;
      }
    }

    if (s.split(".").length > 2 || s.split(",").length > 2)
      test = false;

    return test;

  }

  private static boolean isBool(String s) {
    String s2 = s.toLowerCase().trim();
    if (s2.equals("true") || s2.equals("false"))
      return true;
    else
      return false;
  }

}
