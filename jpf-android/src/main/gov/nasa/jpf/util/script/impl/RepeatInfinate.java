package gov.nasa.jpf.util.script.impl;


public class RepeatInfinate extends Repeat {
  

  public RepeatInfinate(int linenumber, ScriptElementContainer parent) {
    super(linenumber, parent);
    this.setCount(0);
    this.setNumber(1);
  }

  @Override
  public boolean done() {
    return false;
  }

}
