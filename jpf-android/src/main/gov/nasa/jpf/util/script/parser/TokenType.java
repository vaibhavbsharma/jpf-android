package gov.nasa.jpf.util.script.parser;

public enum TokenType {
  ANY("any"), REPEAT("repeat"), SCRIPT("script"), SECTION("section"), ID("identifier"), CHOICE("choice"), RBRACE(
      "}"), LBRACE("{"), COMMA(","), SEMI_COLON(";"),LPAREN("("),RPAREN(")"), EOF("eof"), NUMBER(""), STAR("*");

  private final String string;

  /**
   * Sole constructor.
   * 
   * @param string
   *          representation of the token.
   */
  private TokenType(final String string) {
    this.string = string;
  }

  public String toString() {
    return "Token [" + string + "]";
  }

  public static Token getToken(String id) {
    Token t = null;

    switch (id.toLowerCase()) {
    case "any":
      t = new Token(TokenType.ANY, id);
      break;

    case "repeat":
      t = new Token(TokenType.REPEAT, id);
      break;

    case "section":
      t = new Token(TokenType.SECTION, id);
      break;

    case "script":
      t = new Token(TokenType.SCRIPT, id);
      break;

    case "choice":
      t = new Token(TokenType.CHOICE, id);
      break;

    case "}":
      t = new Token(TokenType.RBRACE, id);
      break;

    case "{":
      t = new Token(TokenType.LBRACE, id);
      break;

    case ";":
      t = new Token(TokenType.SEMI_COLON, id);
      break;

    case ",":
      t = new Token(TokenType.COMMA, id);
      break;
      
    case "(":
      t = new Token(TokenType.LPAREN, id);
      break;

    case ")":
      t = new Token(TokenType.RPAREN, id);
      break;
    case "*":
      t = new Token(TokenType.STAR, id);
      break;

    default:
      t = new Token(TokenType.ID, id);
      break;
    }
    return t;

  }

}
