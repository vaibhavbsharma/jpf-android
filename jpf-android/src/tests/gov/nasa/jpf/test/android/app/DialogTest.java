package gov.nasa.jpf.test.android.app;

import gov.nasa.jpf.util.test.TestJPF;
import gov.nasa.jpf.vm.Verify;

import org.junit.Test;

import android.app.Activity;
import android.os.Bundle;
import android.os.MockMainLooper;

public class DialogTest extends TestJPF {
  // static final TypeRef PROPERTY = new TypeRef("gov.nasa.jpf.listener.PreciseRaceDetector");
  static final String LISTENER = "+listener=gov.nasa.jpf.jvm.SearchStateListener;gov.nasa.jpf.listener.SearchStats";

  @Test
  public void testDialog() {

    if (verifyNoPropertyViolation(LISTENER)) {
      MockMainLooper.prepareMainLooper();
      Verify.breakTransition("Test");
      startActivity();
      MockMainLooper.testloop();

    }

  }

  public void startActivity() {
    // Activity a = new TestDialogActivity();
    // ContextImpl appContext = new ContextImpl();
    // // appContext.init(r.packageInfo, r.token, this);
    // appContext.setOuterContext(a);
    // a.attach(appContext, this, new Instrumentation(), null, 1, null, null, null, null, null,
    // 0, null, new Configuration());
    // a.onCreate(new Bundle());
    // a.onStart();
  }

  public class TestDialogActivity extends Activity {

    @Override
    public void onCreate(Bundle state) {

    }

  }

}
