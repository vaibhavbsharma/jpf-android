package gov.nasa.jpf.test.android.os;

import gov.nasa.jpf.util.test.TestJPF;
import gov.nasa.jpf.vm.Verify;

import org.junit.Test;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.MockMainLooper;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class AsyncTaskTest extends TestJPF {
  // static final TypeRef PROPERTY = new TypeRef("gov.nasa.jpf.listener.PreciseRaceDetector");
  static final String LISTENER = "+listener=gov.nasa.jpf.jvm.SearchStateListener;gov.nasa.jpf.listener.SearchStats";

  // Testing must only print once since the states will be matched

  // @Test
  // public void testAsyncTask() {
  //
  // if (verifyNoPropertyViolation(LISTENER)) {
  //
  // MockMainLooper.prepareMainLooper();
  // Verify.breakTransition("Test");
  // new TestAsyncTask().execute(5);
  // // new TestAsyncTask().execute(5);
  // // .breakTransition("Test");
  // System.out.println("Testing");
  //
  // }
  // }

  @Test
  public void testAsyncTaskStateMatch() {
    if (verifyNoPropertyViolation("+vm.storage.class = nil", LISTENER,
        "+search.class = gov.nasa.jpf.search.DFSearch", "")) {
      MockMainLooper.prepareMainLooper();
      if (Verify.getBoolean()) {
        new TestAsyncTask().execute(5);
      } else {
        new TestAsyncTask().execute(5);
      }

      MockMainLooper.testloop();

      // System.out.println("Testing");
      // System.out.println("Testing");
      // System.out.println("Testing");
      // System.out.println("Testing");
      // Verify.breakTransition("Testing2");
      // System.out.println("Testing2");
      // System.out.println("Testing2");
      // System.out.println("Testing2");

    }
  }

  // @Test
  // public void testAsyncTaskParamsNotStateMatch() {
  //
  // if (verifyNoPropertyViolation()) {
  // MockMainLooper.prepareMainLooper();
  // if (Verify.getBoolean()) {
  // new TestAsyncTask().execute(5);
  // } else {
  // new TestAsyncTask().execute(6);
  // }
  //
  // Verify.breakTransition("Test");
  // System.out.println("Testing");
  //
  // }
  // }

  @Test
  public void testAsyncTaskWithLooper() {

    if (verifyNoPropertyViolation(LISTENER, "+search.class = gov.nasa.jpf.search.DFSearch")) {
      MockMainLooper.prepareMainLooper();

      InternalHandler h = new InternalHandler();
      Message m = h.obtainMessage(1);
      h.dispatchMessage(m);
      MockMainLooper.testloop();
    }
  }

  //

  // Post messages to start AsyncThread in main thread
  private static class InternalHandler extends Handler {
    @Override
    public void handleMessage(Message msg) {
      testMethod();
    }

    private void testMethod() {
      if (Verify.getBoolean()) {
        new Test2AsyncTask().execute(5);
      } else {
        new Test2AsyncTask().execute(5);
      }
    }
  }

  public static class TestAsyncTask extends AsyncTask<Integer, Integer, Integer> {

    @Override
    protected void onPreExecute() {
      System.out.println("onPreExecute ");
      super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Integer result) {
      System.out.println("onPostExecute ");
      super.onPostExecute(result);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
      System.out.println("onProgressUpdate ");
      super.onProgressUpdate(values);
    }

    @Override
    protected Integer doInBackground(Integer... params) {
      System.out.println("doinbackground ");
      return null;
    }

  }

  public static class Test2AsyncTask extends AsyncTask<Integer, Integer, Integer> {

    @Override
    protected Integer doInBackground(Integer... params) {
      System.out.println("doinbackground " + params[0] + Thread.currentThread().getName());
      // publishProgress(params);
      return params[0];
    }

    @Override
    protected void onPreExecute() {
      System.out.println("onPreExecute " + Thread.currentThread().getName());
      super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Integer result) {
      System.out.println("onPostExecute " + result + Thread.currentThread().getName());
      super.onPostExecute(result);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
      System.out.println("onProgressUpdate " + values[0] + Thread.currentThread().getName());
      super.onProgressUpdate(values);
    }

  }

  public static void main(String testMethods[]) throws Throwable {
    runTestsOfThisClass(testMethods);
  }

}