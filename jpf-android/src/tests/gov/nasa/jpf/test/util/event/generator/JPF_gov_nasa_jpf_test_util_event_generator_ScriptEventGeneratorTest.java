package gov.nasa.jpf.test.util.event.generator;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.util.event.EventsProducer;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.generator.ScriptEventGenerator;
import gov.nasa.jpf.util.event.tree.AndroidEventChoiceGenerator;
import gov.nasa.jpf.util.event.tree.EventNode;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

public class JPF_gov_nasa_jpf_test_util_event_generator_ScriptEventGeneratorTest extends NativePeer {
  ScriptEventGenerator scriptEventGenerator;

  public static class AndroidTestEventProducer implements EventsProducer {

    @Override
    public String getCurrentWindow(MJIEnv env, int objRef) {
      return "default";
    }

    @Override
    public Event[] getEvents() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public boolean isFirstEvent(MJIEnv env) {
      return false;
    }

    @Override
    public EventNode getCurrentEvent() {
      // TODO Auto-generated method stub
      return null;
    }

  }

  AndroidTestEventProducer aep = new AndroidTestEventProducer();

  @MJI
  public void zeroSG(MJIEnv env, int objref) {
    this.scriptEventGenerator = null;
  }

  @MJI
  public int processNextScriptEvent(MJIEnv env, int objRef) throws Exception {
    if (this.scriptEventGenerator == null) {
      String scriptPath = env.getConfig().getProperty("android.script");
      this.scriptEventGenerator = new ScriptEventGenerator(scriptPath);
      this.scriptEventGenerator.registerListener(env.getJPF());
    }
    Event[] nextEvents = null;
    AndroidEventChoiceGenerator cg = null;
    // we don't care which next events are in tree we want to put our own events in there
    nextEvents = scriptEventGenerator.getEvents(env, objRef, aep);
    int ret = MJIEnv.NULL;
    if (nextEvents != null && nextEvents.length > 0)
      ret = ((nextEvents != null) ? env.newString(nextEvents[0].toString()) : MJIEnv.NULL);
    return ret;
  }

}
