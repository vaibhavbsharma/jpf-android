package gov.nasa.jpf.test.util.event.generator;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.util.event.EventsProducer;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.SystemEvent;
import gov.nasa.jpf.util.event.generator.DynamicEventGenerator;
import gov.nasa.jpf.util.event.tree.EventNode;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;
import nhandler.conversion.ConversionException;
import nhandler.conversion.jpf2jvm.JPF2JVMConverter;
import nhandler.conversion.jvm2jpf.JVM2JPFConverter;
import android.content.Intent;
import android.view.KeyEvent;

public class JPF_gov_nasa_jpf_test_util_event_generator_DynamicEventGeneratorTest extends NativePeer implements
    EventsProducer {

  DynamicEventGenerator d;

  @Override
  public String getCurrentWindow(MJIEnv env, int objRef) {
    // ask window manager
    return "default";
  }

  @MJI
  public void $init(MJIEnv env, int robj) {
    System.out.println("# entering native <init>(I)");
    d = new DynamicEventGenerator(env.getConfig());
  }

  @MJI
  public int getNextEvent(MJIEnv env, int robj, int eventsRef) throws ConversionException {
    Event[] events = (Event[]) JPF2JVMConverter.obtainJVMObj(eventsRef, env);
    this.events = events;
    Intent i = new Intent("action");
    i.putExtra("test", new KeyEvent(1, 1));
    Event[] eventsRet = { new SystemEvent(i) };
    gov.nasa.jpf.util.event.events.Event[] list = eventsRet;// d.getEvents(env, robj, this);
    System.out.println(list[0]);
    return JVM2JPFConverter.obtainJPFObj(list[0], env);
  }

  Event[] events = null;

  @Override
  public Event[] getEvents() {
    return events;
  }

  @Override
  public boolean isFirstEvent(MJIEnv env) {
    return false;
  }

  @Override
  public EventNode getCurrentEvent() {
    // TODO Auto-generated method stub
    return null;
  }

}
