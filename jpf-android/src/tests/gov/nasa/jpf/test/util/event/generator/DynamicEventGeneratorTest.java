package gov.nasa.jpf.test.util.event.generator;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.UIEvent;
import gov.nasa.jpf.util.test.TestJPF;

import org.junit.Test;

import android.util.Log;

public class DynamicEventGeneratorTest extends TestJPF {


  public DynamicEventGeneratorTest() {

  }

  public Event[] getEvents() {
    Event[] events = new UIEvent[2];
    events[0] = new UIEvent("$button1", "onClick()", null);
    events[1] = new UIEvent("$button2", "onClick()", null);
    return events;

  }

  public native Event getNextEvent(Event[] events);

  @Test
  public void testGetEvent() {
    if (verifyNoPropertyViolation("+event.strategy=dynamic")) {
      Event e = getNextEvent(getEvents());
      Log.i("TAG", e.toString());
      System.out.println(e.print());
    }
  }

}