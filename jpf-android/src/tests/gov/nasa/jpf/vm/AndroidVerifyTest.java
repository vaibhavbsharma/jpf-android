package gov.nasa.jpf.vm;

import gov.nasa.jpf.util.test.TestJPF;

import org.junit.Test;

public class AndroidVerifyTest extends TestJPF {

  @Test
  public void testVerify() {
    int i = -10;
    if (verifyNoPropertyViolation()) {
      i = AndroidVerify.getInt(0, 2, "AndroidVerify.testVerify");
      System.out.println(i);

    }
    System.out.println(i);
  }

  @Test
  public void testVerifyFirst() {
    if (verifyNoPropertyViolation("+verify.choice=first")) {
      int i = AndroidVerify.getInt(0, 2, "AndroidVerify.testVerify");
      System.out.println(i);
    }
  }

  @Test
  public void testVerifyAll() {
    if (verifyNoPropertyViolation("+verify.choice=all")) {
      int i = AndroidVerify.getInt(0, 2, "AndroidVerify.testVerify");
      System.out.println(i);
    }
  }

  @Test
  public void testVerifyLast() {
    if (verifyNoPropertyViolation("+verify.choice=last")) {
      int i = AndroidVerify.getInt(0, 2, "AndroidVerify.testVerify");
      System.out.println(i);
    }
  }

  @Test
  public void testVerifyRandom() {
    if (verifyNoPropertyViolation("+verify.choice=random")) {
      int i = AndroidVerify.getInt(0, 3, "AndroidVerify.testVerify");
      System.out.println(i);
      i = AndroidVerify.getInt(0, 3, "AndroidVerify.testVerify");
      System.out.println(i);
      i = AndroidVerify.getInt(0, 3, "AndroidVerify.testVerify");
      System.out.println(i);
    }
  }

  @Test
  public void testVerifyNext() {
    if (verifyNoPropertyViolation("+verify.choice=next")) {
      int i = AndroidVerify.getInt(0, 2, "AndroidVerify.testVerify");
      System.out.println(i);
      i = AndroidVerify.getInt(0, 2, "AndroidVerify.testVerify");
      System.out.println(i);
      i = AndroidVerify.getInt(0, 2, "AndroidVerify.testVerify");
      System.out.println(i);
    }
  }

  @Test
  public void testVerifyNextBoolean() {
    if (verifyNoPropertyViolation("+verify.choice=random")) {
      boolean b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
      b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
      b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
    }
  }

  @Test
  public void testVerifyNextBoolean2() {
    if (verifyNoPropertyViolation("+verify.choice=first")) {
      boolean b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
      b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
      b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
    }
  }

  @Test
  public void testVerifyNextBoolean3() {
    if (verifyNoPropertyViolation("+verify.choice=last")) {
      boolean b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
      b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
      b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
    }
  }

  @Test
  public void testVerifyNextBoolean4() {
    if (verifyNoPropertyViolation("+verify.choice=all")) {
      boolean b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
      b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
      b = AndroidVerify.getBoolean("AndroidVerify.testVerify");
      System.out.println(b);
    }
  }

  @Test
  public void testVerifyNextString() {
    if (verifyNoPropertyViolation("+verify.choice=random")) {
      String s = AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
      s += AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
      s += AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
    }
  }

  @Test
  public void testVerifyNextString2() {
    if (verifyNoPropertyViolation("+verify.choice=first")) {
      String s = AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
      s = AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
      s = AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
    }
  }

  @Test
  public void testVerifyNextString3() {
    if (verifyNoPropertyViolation("+verify.choice=last")) {
      String s = AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
      s = AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
      s = AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
    }
  }

  @Test
  public void testVerifyNextString4() {
    if (verifyNoPropertyViolation("+verify.choice=all")) {
      String s = AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      // System.out.println(s);
      s += AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      // System.out.println(s);
      s += AndroidVerify.getString(new String[] { "a", "b", "c" }, "AndroidVerify.testVerify");
      System.out.println(s);
    }
  }

}
